# synchronized原理

`synchronized` 的实现包含下面几种层级：

1. `synchronized` 的字节码实现

1. `synchronized` 的 `JVM` - `HotSpot` 实现

1. `synchronized` 的硬件实现



## 字节码实现

将方法加锁或代码块加锁的代码编译后执行以下命令查看字节码

> javap -v xxx.class


### 代码块的加锁

代码块的加锁，`monitorenter` 与 `monitorExit` 指令配合使用。

<img src="../../../java-notes/img/synchronized-代码块加锁.png">

`monitorenter` 与 `monitorExit` 指令是由 `objectMonitor.hpp` 类实现的
<https://hg.openjdk.java.net/jdk8u/jdk8u/hotspot/file/9ce27f0a4683/src/share/vm/runtime/objectMonitor.hpp>

### 方法加锁

为方法加锁, `ACC_SYNCHRONIZED`

<img src="../../../java-notes/img/synchronized-方法加锁.png">

## JVM实现 - todo


## 硬件实现 - todo


```
public class T {
    static volatile int i = 0;
    
    public static void n() { i++; }
    
    public static synchronized void m() {}
    
    publics static void main(String[] args) {
        for(int j=0; j<1000_000; j++) {
            m();
            n();
        }
    }
}
```

java -XX:+UnlockDiagonositicVMOptions -XX:+PrintAssembly T

C1 Compile Level 1 (一级优化)

C2 Compile Level 2 (二级优化)

找到m() n()方法的汇编码，会看到 lock cmpxchg .....指令


## ObjectMonitor机制与synchronized锁升级

1. 什么是`ObjectMonitor`机制？  
    当`synchronized`同步块锁机制升级为**重量级锁**后使用的工作机制，就是`Object Monitor`机制。

1. 实现`synchronized`/`Object Monitor`的基础：
    - Java对象头中的`MarkWord`
    - Monitor对象

1. `synchronized` 锁升级  
    通常情况下，我们在代码中使用 `synchronized` 协调多个线程的工作过程，实际上就是使用 `Object Monitor` 对多个线程的工作进行协调。`synchronized` 的执行过程就是传统意义上的**悲观锁**思想的一种实现，但实际上 `synchronized` 执行过程中还涉及到锁升级过程，升级顺序为  
    **无锁** > **偏向锁** > **轻量级锁(CAS)** > **重量级锁(Object Monitor机制)**




### 锁相关的MarkWord区域结构

根据对象所处的锁状态的不同，`MarkWord` 区域的存储结构会发生变动。例如当对象处于轻量级锁或偏向锁状态的情况下，`MarkWord` 区域的存储结构的定义不同。

` MarkWord` 区域在64位JVM版本的情况和在32位JVM版本的情况下，其结构长度不一样。我们这里讨论64位的版本。

<img src="../../../java-notes/img/javaObjectMarkWord.jpg">




### 锁升级过程

锁一共有四种状态，级别从低到高依次是：

**无锁** > **偏向锁** > **轻量级锁(CAS)** > **重量级锁(Object Monitor机制)**

这几种状态随着竞争情况而升级。锁还可以降级，但是条件非常苛刻，所以一般认为锁不能降级。

#### 偏向锁

偏向锁假定将来**只有第一个申请锁的线程会使用锁**，这时在 `Mark Word` 中使用 `CAS` 记录 `owner` ，如果纪录成功，则说明偏向锁获取成功，记录锁状态为偏向锁，以后当前线程等于 `owner` 就可以无成本的直接获得锁。如果有其他线程竞争，将膨胀为轻量级锁。

> 如果需要，使用参数-XX:-UseBiasedLocking禁止偏向锁优化（默认打开）。

#### 轻量级锁

**轻量级锁的目标是：**

在实际竞争较少的情况下，减少重量级锁产生的性能消耗，包括系统调用引起的内核态与用户态的切换，线程阻塞造成的线程切换等。

使用轻量级锁时，不需要申请互斥量，只需要将 `Mark Word` 中的部分字节使用 `CAS` 更新为指向锁记录的指针，如果更新成功在，择轻量级锁获取成功，记录锁状态为轻量级所，否则说明已有线程获得了轻量级锁，此时说明发生了锁竞争，接下来膨胀为重量级锁。

#### 重量级锁

重量级锁是通过对象内部的监视器(`monitor`)实现，其中 `monitor` 的本质是依赖于底层操作系统的 `Mutex Lock` 实现，操作系统实现线程之间的切换需要从用户态到内核态的切换，切换成本非常高。线程竞争不使用自旋，不会消耗 `CPU`。但是线程会进入阻塞,等待被其他线程被唤醒，响应时间缓慢。


### Monitor

**管程**，英文是 `Monitor`，也常被翻译 *监视器*。

在[《操作系统同步原语》](https://beanlam.me/syncprimitive/) 这篇文章中，介绍了操作系统在面对 进程/线程 间同步的时候，所支持的一些同步原语，其中 `semaphore` 信号量 和 `mutex` 互斥量是最重要的同步原语。

在使用基本的 `mutex` 进行并发控制时，需要程序员非常小心地控制 `mutex` 的 `down` 和 `up` 操作，否则很容易引起死锁等问题。为了更容易地编写出正确的并发程序，所以在 `mutex` 和 `semaphore` 的基础上，提出了更高层次的同步原语` monitor`，不过需要注意的是，操作系统本身并不支持 `monitor` 机制，实际上，`monitor` 是属于编程语言的范畴，当你想要使用 `monitor` 时，先了解一下语言本身是否支持 `monitor` 原语，例如 `C` 语言它就不支持 `monitor`，`Java` 语言支持 `monitor。`

一般的 `monitor` 实现模式是编程语言在语法上提供语法糖，而如何实现 `monitor` 机制，则属于编译器的工作，`Java` 就是这么干的。


`monitor` 的重要特点是，同一个时刻，只有一个 进程/线程 能进入 `monitor` 中定义的临界区，这使得 `monitor` 能够达到互斥的效果。但仅仅有互斥的作用是不够的，无法进入 `monitor` 临界区的 进程/线程，它们应该被阻塞，并且在必要的时候会被唤醒。显然，`monitor` 作为一个同步工具，也应该提供这样的管理 进程/线程 状态的机制。想想我们为什么觉得 `semaphore` 和 `mutex` 在编程上容易出错，因为我们需要去亲自操作变量以及对 进程/线程 进行阻塞和唤醒。`monitor` 这个机制之所以被称为“更高级的原语”，那么它就不可避免地需要对外屏蔽掉这些机制，并且在内部实现这些机制，使得使用 `monitor` 的人看到的是一个简洁易用的接口。

#### monitor的基本元素

1. 临界区
1. `monitor`对象以及锁对象
1. 定义在`monitor`对象/锁对象上的`wait`(阻塞)，`signal`(信号/通知/唤醒)操作 - api


使用 `monitor` 机制的主要目的是为了同一时刻只能有互斥进入临界区，为了能够做到阻塞无法进入临界区的线程/进程，还需要一个`Object Monitor`协助，这个`Object Monitor`内部会有相应的数据结构，来保存被阻塞的线程，由于这种`mobitor`是基于`mutex`实现的，所以`Object Monitor`必须维护一个基于 `mutex`的锁。

此外为了在适当的时候能够阻塞和唤醒进程/线程，还需要引入一个**条件变量**，使用这个条件变量来决定*程序的逻辑*。这个条件变量一般保存在`monitor object`的内部。

另外 `monitor object` 内部采用了数据结构来保存被阻塞的线程以及条件变量，因此它也必须对外提供两个 `API` 来让线程进入`阻塞(wait)`状态，以及之后被`唤醒(notify)`


#### Java语言对monitor的支持

- 临界区：使用`synchronized` 关键字来修饰**类方法**、**实例方法**、**代码块**

- **`monitor`对象以及锁对象**：使用**Class对象**，**实例对象**作为锁对象

- `wait`(阻塞)，`signal`(信号/通知/唤醒)操作：使用锁对象的继承自`Object`的 `wait`、`notify`等方法来实现阻塞、唤醒操作

#### Java对象锁与Monitor

`JVM` 为每个对象都提供了一个 `monitor` (C++实现)实例，在 `Java` 中 `Monitor` 被描述为对象监视器，可以类比为一个特殊房间,这个房间中有一些需要被保护的数据，进入房间即为持有 `monitor` ，退出房间为释放 `monitor`.

使用 `synchronized` 修饰的同步代码在执行时，主要就是通过锁对象的 `monitor` 的取用和释放来实现的。

### MonitorObject机制

`MonitorObject` 机制就是 `synchronized` 升级为重量级锁后使用的机制

MonitorObject的结构如下：

<img src="../../../java-notes/img/ObjectMonitor.png">

源码如下：
<https://hg.openjdk.java.net/jdk8u/jdk8u/hotspot/file/5bd0e0bcb152/src/share/vm/runtime/objectMonitor.hpp>

- **_owner**: 指向持有`ObjectMonitor`的线程

- **_WaitSet**: 已经持有过锁，但又调用过wait等方法释放操作权，并且没有离开同步块，等待其他线程调用`notify/notifyAll`的线程。就会进入该队列

- **_EntryList**：就绪队列，等待持有锁对象

下图为 `Object Monitor` 的工作过程：

<img src="../../../java-notes/img/objectMonitor-工作过程.png">

正如 `Object Monitor` 机制的名称一样，它是以 `Java` 对象为基础的，这是一种在多线程情况下对特定对象的操作权限的一种控制方式，在这种控制方式下流程如下：

1. **线程进入监控区 - `_EntryList`**：在线程争取锁对象失败时，进入该区域，在代码中表现为停留在 `synchronized` 同步块外，处于 `_EntryList` 区域的线程，其线程被标识为 `BLOCKED`

1. **线程进入对象操作权持有区 - `_Owner`**：
    1. 当线程争取到锁对象后：`_Owner` 将持有该线程对象,而当前持有对象操作权限的线程**互斥量(`Mutex`)**将被记录在这个对象的对象头中,此时线程为运行状态 `RUNNABLE`，并且会将**锁进入次数(`_count`)**加1，此种现象称之为**锁重入**。

    1. 当线程释放对象的操作权限后：`_Owner` 被置为 `NULL`，**锁进入次数(`_count`)**减一，当没有线程持有该对象时， `_count` 值为0。

1. **线程进入待授权区**：当线程争取到锁对象后，通过 `wait` 方法释放了对象的操作权，但是只要这个线程还没有退出 `synchronized` 同步代码块，就不会释放对于锁对象的**抢占权**，这些线程被存放在 `_WaitSet` 区域，但并不是所有存在该区域的线程都能重新参与对象操作权的抢占，而是只能通过 `notify`/`notifyAll` 等方法通知的线程能狗参与


**注意**：每个对象的 `ObejctMonitor` 控制过程相对独立，但是一个线程可以同时拥有一个或者多个对象的操作权。


## 面试

1. synchroized与Lock区别

    1. 在高争用 高耗时的环境下synchronized效率更高
    1. 在低争用 低耗时的环境下CAS效率更高
    1. synchronized到重量级之后是等待队列不消耗CPU而CAS等待期间消耗CPU
