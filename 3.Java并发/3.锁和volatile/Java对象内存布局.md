# Java对象的内存布局

<https://blog.csdn.net/zhou920786312/article/details/84204399>

在HotSpot 虚拟机中，将对象结构分为三块区域：
- **对象头（Object Header）**：我们讨论32位JDK版本和32位操作系统下它的内部结构（64位JDK版本和64位操作系统的情况类似，只不过各主要结构都变成了32位的长度）。视情况它又可能分为2-3个子结构：

    - **MarkWord** 区域是该对象关键的运行时数据，默认储存着对象的 **HashCode**、**分代年龄**、以及当前**锁的信息**,**它是实现轻量级锁和偏向锁的关键**。另外如果当前对象作为锁对象时，当锁产生变化时当前的数据结构也会产生变化。

    - **Class pointer** 这是一个指针区域，这个指针区域指向元数据区中（JDK1.8）该对象所代表的类描述，这样`JVM`才知道这个对象是哪一个类的实例

    - 数组长度（只有数组形式的对象会有这个区域）：数组对象的这个区域表达了数组长度。


- **实例数据(Instance Data)**：  
这个区域当然就是描述真实的对象数据。这个区域包括了对象中的所有字段属性信息，它们可能是某个其它对象的地址引用，也可能是基础数据的数据值。


- **对齐填充区(可能存在)**：  
对齐填充区域并不是必须存在的，它只是起到占位作用 - 这是因为`HotSpot JVM` 虚拟机要求被管理的对象的大小都是**8字节**的整数倍。那么在某些情况下，就需要填充区域对不足的对象区域进行填充。这个概念与CPU的缓存对齐类似，是JVM为了硬件读取速度而做出的设计。

下面这张图展示了普通类型和数组类型对象在内存中的布局。

<img src="../../../java-notes/img/对象在内存中的布局.png">




## 使用JOL查看对象的内存布局
`JOL` 是`openjdk`提供的一个查看内存布局的工具，全称为 `Java Object Layout`。

maven
```
<dependency>
    <groupId>org.openjdk.jol</groupId>
    <artifactId>jol-core</artifactId>
    <version>0.9</version>
</dependency>
```

code

```
public class Hello_JOL {
    public static void main(String[] args) {
        Object o = new Object();
        String s = ClassLayout.parseInstance(o).toPrintable();
        System.out.println(s);
    }
}
```
测试1,输出
```
java.lang.Object object internals:
OFFSET SIZE  TYPE DESCRIPTION    VALUE
0       4    (object header)    01 00 00 00 (00000001 00000000 00000000 00000000) (1)
4       4    (object header)    00 00 00 00 (00000000 00000000 00000000 00000000) (0)
8       4    (object header)    e5 01 00 f8 (11100101 00000001 00000000 11111000) (-134217243)
12      4    (loss due to the next object alignment)
Instance size: 16 bytes
Space losses: 0 bytes internal + 4 bytes external = 4 bytes total
```
该对象大小为16字节，细分区域如下：

1. 前两项`object header` 为`mark word`大小为8字节。

1. 第三项`object header`为`Class pointer`即对象指针，指向该实例的元类。

1. 第四项为对齐区，它保证了对象的大小为8的倍数，这是`hotSpot`的要求。

## 关于内存布局的HotSpot参数

使用以下命令查看`jvm`的参数

```
java -XX:+PrintCommandLineFlags -version
```
输出(已添加注释)
```
# 最大堆大小(字节)
-XX:InitialHeapSize=198883520 

# 最大堆大小(字节)
-XX:MaxHeapSize=3182136320 

# 打印命令行参数 - 我们刚才就是用的这个命令
-XX:+PrintCommandLineFlags 

# 使用压缩类指针 - 默认开启(开启后指针大小为4，否则为8字节)
# 这里的类指针是指对象实例指向元类的指针，可以使用JOL工具查看
-XX:+UseCompressedClassPointers 

# 压缩普通对象指针
# 普通对象指针是指在代码中定义的对象引用
# 如 Obejct o = new Object(); 中的o
-XX:+UseCompressedOops 

-XX:-UseLargePagesIndividualAllocation 

-XX:+UseParallelGC

java version "1.8.0_131"
Java(TM) SE Runtime Environment (build 1.8.0_131-b11)
Java HotSpot(TM) 64-Bit Server VM (build 25.131-b11, mixed mode)
```


## 面试题

1. Object o = new Object();中的对象o占用多大内存。

    答：占用16个字节的内存，其中 `markword` 占用8个字节，类指针占用4个字节，对齐区占用4个字节。


1. 以下对象在 `new` 后占用多少内存?
    ```
    class A{
        boolean a;
    }
    ```
    答：在开启指针压缩后，共占用16个字节，其中 `markword` 占用8个字节，类指针占用4个字节，a占用1个字节，对齐区占用3个字节。