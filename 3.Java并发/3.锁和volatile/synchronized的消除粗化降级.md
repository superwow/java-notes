# 锁的消除粗化降级

## 逃逸分析

逃逸分析(`Escape Analysis`)是目前`Java`虚拟机中比较前沿的优化技术。这是一种可以有效减少 `Java` 程序中同步负载和内存堆分配压力的跨函数全局数据流分析算法。通过逃逸分析，`Java Hotspot` 编译器能够分析出一个新的对象的引用的使用范围从而决定是否要将这个对象分配到堆或者栈上。

逃逸分析主要有以下两种：

- **方法逃逸**：当一个**对象**在方法中被定义后，可能作为调用**参数**被**外部方法**说引用。  
    ```
    public static StringBuffer craeteStringBuffer(String s1, String s2) {
        StringBuffer sb = new StringBuffer();
        sb.append(s1);
        sb.append(s2);
        return sb;
    }
    ```
    如返回值 `sb` 可能被外部所引用
- **线程逃逸**：通过复制给**类变量**或者作为**实例变量**在其他**线程**中可以被访问到。



## 锁消除

锁消除是基于**逃逸分析**的，如果一个对象随着方法对应栈帧的压栈而创建，弹栈而销毁，即该对象无法逃脱出这个方法，则这个方法里对该对象的**加锁操作可以消除**

```
public String concat(String s1, String s2){
    StringBuffer sb = new StringBuffer();
    sb.append(s1);
    sb.append(s2);
    return sb.toString();
}
```

经过逃逸分析，发现StringBuffer对象无法逃出这个方法，且对方法来说是线程私有的（虚拟机栈是线程私有的），那么对append方法的加锁就可以消除，免去加锁的开销。

## 锁粗化

`JVM` 探测到对一系列动作都要加锁，就会将**加锁范围扩大**
```
public String concat(String s1, String s2){
    StringBuffer sb = new StringBuffer();
    sb.append(s1);
    sb.append(s2);
    return sb.toString();
}
```

将加锁加在第一个append方法前，释放锁加在最后一个append后，这样只要加锁一次，而不用两次了。（例子不大准确。因为这里其实已经锁消除了，但是差不多描述的就是这么个意思）


## 锁升级 - ReentrantReadWriteLock

针对 `ReentrantReadWriteLock` 来说，锁升级就是**读锁升级为写锁的过程**，在持有读锁的情况下，中间加入写锁，随后释放读锁的整个过程中，读锁升级为写锁。
```
//读锁
readLock.lock();
    ...
    //写锁 - 此时锁已经升级为写锁
    writeLock.lock();
    ...
//读锁 - 此时锁已经升级为写锁
readLock.lock();
```
