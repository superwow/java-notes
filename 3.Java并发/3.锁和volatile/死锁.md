# 死锁

死锁是指两个或两个以上的线程在执行过程中，由于竞争资源或者由于彼此通信而造成的一种阻塞的现象，若无外力作用，它们都将无法推进下去。此时称系统处于死锁状态或系统产生了死锁，这些永远在互相等待的进程称为死锁进程

## 死锁产生的条件

1. 互斥条件  
    线程在一段时间内对资源的排他性使用

1. 请求和保持条件  
    线程已经保持了一个资源，但是又提出了新的资源请求，但是该资源已经被其他线程占有

1. 不剥夺条件  
    线程已获得的资源，在未使用完之前，不能被剥夺，只能在自己使用完时自己释放

1. 环路等待条件  
    一种资源的循环等待链

## 实例

```
public class DealLock1 {

    private static final Object lock1 = new Object();
    private static final Object lock2 = new Object();

    public static void main(String[] args) {
        new Thread(() -> {

            try {
                Thread.sleep(500);
                synchronized (lock1) {
                    System.out.println(Thread.currentThread().getName() + " = lock1");
                    Thread.sleep(5000);
                    synchronized (lock2) {
                        System.out.println(Thread.currentThread().getName() + " = lock2");
                    }

                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        new Thread(() -> {

            try {
                Thread.sleep(500);

                synchronized (lock2) {
                    System.out.println(Thread.currentThread().getName() + " = lock2");
                    Thread.sleep(5000);
                    synchronized (lock1) {
                        System.out.println(Thread.currentThread().getName() + " = lock1");
                    }

                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
```

此时线程1和线程2都在等待对方释放锁，而自己却持有对方请求的锁，所以造成死锁。

输出：  

<img src="../../../java-notes/img/dealLock.png">