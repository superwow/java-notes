# JUC概述

首先我们知道，`JUC` 就是 `java.util.concurrent` 包，俗称`java`并发包。它的的作者是 - [Doug Lea](https://baike.baidu.com/item/Doug%20Lea/6319404?fr=aladdin)。

<img src="../../../java-notes/img/JUC.png">

可以从上图看出 `JUC` 可以分为以下类：

1. 原子类操作 - atomic

1. 锁 - locks

1. 线程池 - executor

1. 并发工具 - tools

1. 并发集合 - collections

1. 队列

1. 异步组合编程



附一张JUC主要的类思维导图:

<img src="../../../java-notes/img/JUC-Class.jpg">