# AQS

所谓AQS，指的是AbstractQueuedSynchronizer，它提供了一种实现阻塞锁和一系列依赖FIFO(First in first out - 先进先出)等待队列的同步器的框架。

<img src="../../../java-notes/img/AQS底层实现.png">

<img src="../../../java-notes/img/aqs.png">


AQS底层使用了双向链表,这是队列的一种实现. 

Sync queue就是同步队列，包含head(头)节点和tail(尾)节点，其中head节点主要用于后续的调度。

Condition queue就是条件队列，它使用单向链表进行实现，它并不是必须的，只有当程序中需要使用Condition时才会产生这个链表，并且可能会有多个Condition queue。

## AQS的功能

- 使用Node实现FIFO(First in First out)队列,可以用于构建锁或者其他的同步装置的基础框架

- 使用一个名为State的int类型的变量作为锁，为0时表示自由，上锁为1，大于1为重入

- AQS的使用方式，是通过继承AQS实现其模板方法，然后将子类继承并实现它的方法管理其状态{acquire(获取)和release(释放)}的方法操纵状态。
    具体实现有`ReentrantLock`、`Semaphore`、`CountDownLatch`、`CyclicBarrier`等类。

- 可以同时实现排它锁，和共享锁模式(独占、共享)

AQS内部维护了一个CLH队列来管理锁，线程会首先尝试获取锁(此时队列中的线程还没有获得锁的机会，但后来线程却在尝试获得锁，所以此时是非公平锁)

如果竞争锁失败就会将当前线程以及等待信息包装成为一个node节点加入到同步队列中(具体是采用CAS算法，但线程并发对尾部节点可能会导致其他线程失败，解决办法是循环CAS至成功)，同时再阻塞该线程。当获取锁的线程释放锁以后，会从队列中唤醒后继阻塞的节点(线程)。




## CountDownLatch - 闭锁

<img src="../../../java-notes/img/CountDownLacch.png">

TA - 主线程
T1、T2、T3 - 子线程

在主线程中设置CountDownLactch的实例的cnt属性为3，此时启动T1、T2、T3线程并在内部调用CountDownLactch的countDown()方法将cnt-1,此时在主线程内执行CountDownLactch的await()方法，阻塞主线程。当子线程内执行完毕cnt等于0时，继续执行主线程。

示例
```
public class CountDownLatchDemo1 {

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(3);

        for (int i = 0; i < 3; i++) {
            new Thread(() -> {
                System.out.println(Thread.currentThread().getName());
                latch.countDown();
            }, String.valueOf(i)).start();
        }
        latch.await();
        System.out.println("end");
    }
}
```
输出: 
```
0
1
2
end
```

其中await(long timeout, TimeUnit unit)方法可以阻塞主线程一段时间后继续执行。
```
latch.await(10, TimeUnit.SECONDS);
```


## Semaphore - 信号量
```
public class SemaphoreDemo1 {

    private static final Semaphore semaphore = new Semaphore(20);

    public static void main(String[] args) {
        ExecutorService exec = Executors.newCachedThreadPool();
        for (int i = 0; i < 200; i++) {
            final int a = i;
            exec.execute(() -> {

                try {
                    //获取许可,此时也可以获取多个acquire(3)
                    semaphore.acquire();

                    System.out.println(a);

                    Thread.sleep(2000);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //释放许可,也可以释放多个许可release(2)，获取与释放不必一一对应
                semaphore.release();
            });
        }
        exec.shutdown();

    }
}
```
输出时是非连续的，所以可以看到同时执行的线程只有20个。

如果在并发较高时，可以使用tryAcquire()方法，尝试获取许可。如果获取不到许可，依然能够继续执行代码，如果获取到了许可，在执行完业务逻辑后依然需要释放许可。
| 函数 | 说明 |
| ---- | ------ |
| boolean tryAcquire() | 尝试获取1个许可，获取成功返回true，失败返回false |
| boolean tryAcquire(int permits) | 尝试获取多个许可，获取成功返回true，失败返回false |
| boolean tryAcquire(long timeout, TimeUnit unit) | 在一定时间内尝试获取1个许可，获取成功返回true，失败返回false |
| boolean tryAcquire(int permits ,long timeout, TimeUnit unit) | 在一定时间内尝试获取多个许可，获取成功返回true，失败返回false |






具体使用场景：如高并发下丢弃需求。


## CyclicBarrier - 循环屏障

栅栏类似于闭锁，它能阻塞一组线程直到某个事件的发生。栅栏与闭锁的关键区别在于，所有的线程必须同时到达栅栏位置，才能继续执行。闭锁用于等待事件，而栅栏用于等待其他线程。

CyclicBarrier可以使一定数量的线程反复地在栅栏位置处汇集。当线程到达栅栏位置时将调用await方法，这个方法将阻塞直到所有线程都到达栅栏位置。如果所有线程都到达栅栏位置，那么栅栏将打开，此时所有的线程都将被释放，而栅栏将被重置以便下次使用。


示例
```
public class CycLicBarrierDemo1 {

    private static final CyclicBarrier barrier = new CyclicBarrier(5);

    //线程到达屏障后优先执行，回调Runnable
    //private static final CyclicBarrier barrier = new CyclicBarrier(5, new Runable(){});
    
    public static void main(String[] args) {

        ExecutorService service = Executors.newCachedThreadPool();

        for (int i = 0; i < 20; i++) {

            final int a = i;

            service.execute(() -> {
                System.out.println(a + " - start");
                try {
                    barrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
                System.out.println(a + " - end");
            });
        }

        service.shutdown();
    }
}
```
输出
```
0 - start
1 - start
2 - start
3 - start
5 - start
5 - end
0 - end
1 - end
2 - end
3 - end
。。。。。。
```
API
| 函数名 | 说明 |
| - | - |
| int await()  | 线程到达栅栏 |
| int await(long timeout, TimeUnit unit)  | 线程到达栅栏，但会等待一段时间，到达时间后会继续执行 |
| int getNumberWaiting() | 返回当前在障碍处等待的参与者数量 |
| int getParties() | 返回突破屏障所需数量 |
| boolean isBroken() | 查询此屏障是否处于断开状态。 |
| void reset() | 将屏障重置为其初始状态 |


**CountDownLatch 与 CyclicBarrier对比**

CountDownLatch 是一次性的，CyclicBarrier 是可循环利用的
CountDownLatch 参与的线程的职责是不一样的，有的在倒计时，有的在等待倒计时结束。CyclicBarrier 参与的线程职责是一样的。




## ReentrantLock - 重入锁

### ReentrantLock 与 Sychronized

- 可重入性 
     都可以重入都是使用计数器来实现的

- 锁实现
    - ReentrantLock 使用 AQS 实现
    - Sychronized 使用 JVM 实现

- 性能
    在Sychronized引入偏向锁，轻量级锁之后两者性能没有太大差别
    在简单场景下官方推荐使用Sychronized，因为它的写法更容易
    (Sychronized的优化，就是借鉴了AQS的CAS技术，都是试图在用户态解决加锁问题，避免进入内核态的线程阻塞)

- 功能区别
    - ReentrantLock ：需要手动释放锁(在finally代码块中)，锁的粒度根据需要手动控制即可
    - Sychronized ：不需要手动释放锁，锁粒度较大

- ReentrantLock可以指定公平锁还是非公平锁，Sychronized只能是非公平锁    

### ReentrantLock独有功能

- ReentrantLock可以指定公平锁还是非公平锁(但是公平锁会带来性能损耗)

- 提供了一个Condition类，可以分组唤醒需要唤醒的线程

- 提供了一种能够中断等待锁的线程的机制，lock.lockInterruptibly()

ReentrantLock使用的是自旋锁，通过循环调用CAS操作，来实现加锁，性能较好，避免了线程进入内核态并阻塞


当需要使用以上三个功能时，可以弃用Sychronized，转而使用ReentrantLock。

### API


<table class="table-left">
    <tr>
        <th>函数名</th>
        <th>说明</th>
    </tr>
    <tr>
        <th>new ReentrantLock()</th>
        <th>非公平锁</th>
    </tr>
    <tr  class="red">
        <th>new ReentrantLock(boolean isFair)</th>
        <th>公平/非公平锁</th>
    </tr>
    <tr  class="red">
        <th>void lock()</th>
        <th>锁定</th>
    </tr>
    <tr  class="red">
        <th>boolean tryLock()</th>
        <th>尝试获取锁定</th>
    </tr>
    <tr>
        <th>boolean tryLock(long timeout, TimeUnit unit)</th>
        <th>在一定时间内尝试获取锁定</th>
    </tr>
    <tr  class="red">
        <th>void unlock()</th>
        <th>取消锁定</th>
    </tr>
    <tr  class="red">
        <th>boolean isFair()</th>
        <th>是否公平锁</th>
    </tr>
    <tr  class="red">
        <th>boolean isLocked()</th>
        <th>是否锁定</th>
    </tr>
    <tr  class="red">
        <th>int getHoldCount()</th>
        <th>当前持有锁的线程数，也就是调用lock的次数</th>
    </tr>
    <tr class="red">
        <th>void lockInterruptibly()</th>
        <th>如果当前线程没有被中断的话就获取锁定，如果已经中断就抛出异常</th>
    </tr>
    <tr >
        <th>boolean isHeldByCurrentThread()</th>
        <th>当前线程是否锁定状态</th>
    </tr>
    <tr>
        <th>int getQueueLength()</th>
        <th>返回等待获取锁的线程数</th>
    </tr>
    <tr>
        <th>int getWaitQueueLength(Condition condition)</th>
        <th>按照条件返回等待锁的线程数</th>
    </tr>
    <tr>
        <th>boolean hasQueuedThread(Thread thread)</th>
        <th>指定线程是否等待锁</th>
    </tr>
    <tr>
        <th>boolean hasQueuedThreads()</th>
        <th>是否有线程在等待锁</th>
    </tr>
    <tr>
        <th>boolean hasWaiters(Condition condition)</th>
        <th>按照按给定条件，等待锁的线程</th>
    </tr>
</table>

### 使用lock实现生产者消费者
```
public class UseReentrantLock {

    private Lock lock = new ReentrantLock();
    //使用Condition配合Lock完成等待通知模型
    private Condition condition = lock.newCondition();

    public void method1() {

        try {
            lock.lock();
            System.out.println("当前线程：" + Thread.currentThread().getName() + "进入method1..");
            condition.await();
            Thread.sleep(1000);
            System.out.println("当前线程：" + Thread.currentThread().getName() + "退出method1..");
            Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void method2() {

        try {
            lock.lock();
            System.out.println("当前线程：" + Thread.currentThread().getName() + "进入method2..");
            Thread.sleep(2000);
            System.out.println("当前线程：" + Thread.currentThread().getName() + "退出method2..");
            Thread.sleep(1000);
            condition.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) throws InterruptedException {

        final UseReentrantLock use = new UseReentrantLock();

        new Thread(() -> {
            use.method1();
        }, "t1").start();

        Thread.sleep(200);
        new Thread(() -> {
            use.method2();

        }, "t2").start();
    }

}
```
输出
```
当前线程：t1进入method1..
当前线程：t2进入method2..
当前线程：t2退出method2..
当前线程：t1退出method1..
```
## ReentrantReadWriteLock - 读写锁

ReentrantReadWriteLock其核心就是实现读写分离的锁，在高并发下，尤其是读多写少的情况下，性能远高于重入锁。

读写锁，在读锁下多个线程可以进行并发的访问，但是在写锁的时候，只能顺序访问。但是读写锁在读多写少的情况下，写锁有可能竞争不到锁，所以会造成饥饿现象。

读锁与写锁的关系为：

读读共享，写写互斥，读写互斥





