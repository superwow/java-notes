# 线程相关的API

## Thread类的构造方法

<table>
    <tr>
        <th>构造方法</th>
        <th>说明</th>
    </tr>
    <tr>
        <td>Thread()</td>
        <td>创建Thread对象</td>
    </tr>
    <tr class="red">
        <td>Thread(Runnable target)</td>
        <td>指定线程的运行逻辑</td>
    </tr>
    <tr  class="red">
        <td>Thread(Runnable target, String name)</td>
        <td>指定运行逻辑与线程名称</td>
    </tr>
    <tr>
        <td>Thread(String name)</td>
        <td>指定线程名称</td>
    </tr>
    <tr>
        <td>Thread(ThreadGroup group, Runnable target)</td>
        <td>指定线程的运行逻辑，并将该线程加入指定线程组</td>
    </tr>
    <tr>
        <td>Thread(ThreadGroup group, Runnable target, String name)</td>
        <td>指定线程的运行逻辑与名称，并将该线程加入指定线程组</td>
    </tr>
    <tr>
        <td>Thread(ThreadGroup group, Runnable target, String name, long stackSize) 
        </td>
        <td>指定线程的运行逻辑与名称与线程堆栈大小,并将该线程加入指定线程组，</td>
    </tr>
    <tr>
        <td>Thread(ThreadGroup group, String name)</td>
        <td>指定线程的名称,并将该线程加入指定线程组，</td>
    </tr>
</table>

## Thread实例方法

### void statr()

start用于启动线程。

- 线程的调用是**随机**的

- start方法是**通知** 线程规划器 此线程已经准备就绪等待调用线程的run方法。

- 多次调用start方法会抛出Exception in thread "main" java.lang.IllegalThreadStateException异常


### void interrupt()
用于中断线程


- 调用该方法仅仅是在当前线程中打了一个停止的标记，并不是真的停止线程。可以配合isInterrupted方法来终止线程。

- 当线程处于阻塞(调用`sleep`方法)状态时，抛出`InterruptedException`异常，但是如果使用锁进行阻塞，则不会抛出异常


### boolean isInterrupted()

线程是否已经中断。

一般情况下使用 `Thread.currentThread().isInterrupted()` 来进行判断线程是否为中断状态.


### boolean isAlive()

线程是否处于活动状态。

### void join()

join() 方法，等待其他线程终止，在当前线程中调用一个线程的 join() 方法，则当前线程转为阻塞状态，回到另一个线程结束，当前线程再由阻塞状态变为就绪状态，等待 cpu 的调用。

内部是使用当前线程对象作为锁，阻塞当前线程，当子线程结束时会将线程对象释放，从而继续运行主线程。

使用场景：

当主线程，等待子线程返回结果时。但是可以使用`FutureTask`代替。




### void join(long millis)

等待该线程终止的时间最长为 millis 毫秒，原理同上。




### void join(long millis, int nanos)

等待该线程终止的时间最长为 millis 毫秒 + nanos 纳秒。



### boolean isDaemon()

该线程是否为守护线程。即后台线程


### void setDaemon(boolean on)

将该线程标记为守护线程或用户线程。又叫后台线程（是后台提供一种通用的服务线程）

### void setPriority(int newPriority)

更改线程的优先级,默认优先级为5。

在操作系统中，线程可以划分优先级。优先级较高的线程得到的cpu资源较多，也就是cpu有限执行优先级较高的线程对象中的任务。设置优先级有助于 线程规划器 确定下一次选择哪一个线程来优先执行。

在JAVA中线程的优先级 从低到高分为1~10个等级，如果小于1或大于10，则jdk抛出异常。
Thread类中有三个常量预置定义优先级的值：
```
public final static int MIN_PRIORITY = 1;
public final static int NORM_PRIORITY = 5;
public final static int MAX_PRIORITY = 10;
```

JAVA中线程的优先级特性：

1. 继承性  
在JAVA中优先级具有继承性，如A线程启动B线程则B线程的优先级与A是一样的（不手动设置线程优先级的前提下）。

2. 规则性  
CPU尽量将执行资源让给优先级较高的线程。

3. 随机性  
大多数时候都是优先级较高的线程先执行完run方法中的任务，但不能说的太肯定，优先级较高的线程不一定每一次都先执行完成。



### 其他实例方法-不重要

<table>
    <tr>
        <th class="w-60">实例方法</th>
        <th>说明</th>
    </tr>
    <tr>
        <td>long getId()</td>
        <td>返回该线程的唯一标识符</td>
    </tr>
    <tr>
        <td>String getName()</td>
        <td>返回该线程的名称</td>
    </tr>
    <tr>
        <td>void setName(String name)</td>
        <td>改变线程名称，使之与参数 name 相同</td>
    </tr>
    <tr>
        <td>int getPriority()</td>
        <td>返回线程的优先级</td>
    </tr>
    <tr>
        <td>Thread.State getState())</td>
        <td>返回该线程的状态</td>
    </tr>
    <tr>
        <td>ThreadGroup getThreadGroup()</td>
        <td>返回该线程所属的线程组</td>
    </tr>
    <tr>
        <td>ClassLoader getContextClassLoader()</td>
        <td>返回该线程的上下文 ClassLoader</td>
    </tr>
    <tr>
        <td>StackTraceElement[] getStackTrace()</td>
        <td>返回一个表示该线程堆栈转储的堆栈跟踪元素数组</td>
    </tr>
    <tr>
        <td>Thread.UncaughtExceptionHandler getUncaughtExceptionHandler()</td>
        <td>返回该线程由于未捕获到异常而突然终止时调用的处理程序。</td>
    </tr>
    <tr>
        <td>void setContextClassLoader(ClassLoader cl)</td>
        <td>置该线程的上下文 ClassLoader</td>
    </tr>
    <tr>
        <td>void setUncaughtExceptionHandler(Thread.UncaughtExceptionHandler eh)</td>
        <td>设置该线程由于未捕获到异常而突然终止时调用的处理程序。</td>
    </tr>
    <tr>
        <td>String toString()</td>
        <td>返回该线程的字符串表示形式，包括线程名称、优先级和线程组。</td>
    </tr>
</table>

## Thread静态方法

### Thread currentThread()

返回对当前正在执行的线程对象的引用。

### static boolean interrupted()

测试当前线程是否已经中断。

### void sleep(long millis)

`sleep` 导致当前线程休眠，与 `wait` 方法不同的是 sleep 不会释放当前占有的锁,`sleep(long)`会导致线程进入 **TIMED-WATING** 状态，而 `Object` 的 `wait()` 方法会导致当前线程进入 **WATING** 状态

### void yield()

**yield** 会使当前线程让出 `CPU` 执行时间片，与其他线程一起重新竞争 `CPU` 时间片。一般情况下，优先级高的线程有更大的可能性成功竞争得到 `CPU` 时间片， 但这又不是绝对的，有的操作系统对线程优先级并不敏感。

### 其他静态方法

- **boolean holdsLock(Object obj)**：当且仅当当前线程在指定的对象上保持监视器锁时，才返回 true。

- **void dumpStack()**：将当前线程的堆栈跟踪打印至标准错误流。

- **int enumerate(Thread[] tarray)**：将当前线程的线程组及其子组中的每一个活动线程复制到指定的数组中。

- **Map< Thread,StackTraceElement[]> getAllStackTraces()**：返回所有活动线程的堆栈跟踪的一个映射。

- Thread.UncaughtExceptionHandler getDefaultUncaughtExceptionHandler()：返回线程由于未捕获到异常而突然终止时调用的默认处理程序。

- void setDefaultUncaughtExceptionHandler(Thread.UncaughtExceptionHandler eh)
设置当线程由于未捕获到异常而突然终止，并且没有为该线程定义其他处理程序时所调用的默认处理程序。


## Object线程相关的API

### void wait()

调用该方法的线程进入 **WAITING** 状态，只有等待另外线程的通知或被中断才会返回，需要注意的是调用 `wait()`方法后， 会释放对象的锁。因此， `wait` 方法一般用在同步方法或同步代码块中。


### void notify()

Object 类中的 **notify()** 方法， 唤醒在此对象监视器上等待的单个线程，如果所有线程都在此对象上等待，则会选择唤醒其中一个线程，选择是任意的，并在对实现做出决定时发生，线程通过调用其中一个 **wait()** 方法，在对象的监视器上等待， 直到当前的线程放弃此对象上的锁定，才能继续执行被唤醒的线程，被唤醒的线程将以常规方式与在该对象上主动同步的其他所有线程进行竞争。类似的方法还有 **notifyAll()** ，唤醒再次监视器上等待的所有线程。

### void notifyAll()

唤醒再次监视器上等待的所有线程。