# MQ概述

MQ 全称 Message Queue - 消息队列，是在消息的传输过程中保存消息的容器，多用于分布式系统间的通信。


AMQP 即 Advanced Message Queue Protocol（高级消息队列协议），是一个网络协议，使用户层协议的一个开放标准，为面向消息的中间设计，基于此协议的客户端与消息中间件可传递消息。2006年 AMQP 规范发布 类比 HTTP。

![](https://gitee.com/superwow/pic/raw/master/img/20210326215159.png)

1. 发布者将信息发送到 MQ的交换价

1. 随后交换机根据不同的路由规则，将消息分发到不同的 队列中

1. 最后消费者监听 queue ，消费消息


优势：

- 应用解耦

    系统的耦合性越高，容错性和可维护性就越低

- 削峰填谷

    当请求瞬间增多，从mq中拉取定量请求执行

劣势

- 系统可用性降低 - MQ宕机

- 系统复杂度提高 - 同步调用变为异步




# MQ 对比

![](https://gitee.com/superwow/pic/raw/master/img/20210326214829.png)

# RabbitMQ

RabbitMQ 基于AMQP 协议开发，架构图如下：

![](https://gitee.com/superwow/pic/raw/master/img/20210327091921.png)


- 交换机 - Exchange：Message 叨叨 broker 的第一站，根据分发规则，匹配查询表中的 routing key ，分发消息到 queue 中。常见类型如下

    - direct（point to point） - 点对点

    - topic（publish - subscribe） - 发布订阅

    - fanout（multicast） - 组播

- Queue：存储消息，并等待 consumer 取走

- Banding：exchange 和 queue 之间的虚拟连接，binding 中可以包含 routing key，binding 信息保存到exchange 的查询表中，用于 message 的分发



## 工作模式

Rabbitmq 提供了六种工作模式

RabbitMQ工作模式：
1、简单模式 HelloWorld
    一个生产者、一个消费者，不需要设置交换机（使用默认的交换机）

2、工作队列模式 Work Queue
    一个生产者、多个消费者（竞争关系），不需要设置交换机（使用默认的交换机）

3、发布订阅模式 Publish/subscribe
    需要设置类型为fanout的交换机，并且交换机和队列进行绑定，当发送消息到交换机后，交换机会将消息发送到绑定的队列

4、路由模式 Routing
    需要设置类型为direct的交换机，交换机和队列进行绑定，并且指定routing key，当发送消息到交换机后，交换机会根据routing key将消息发送到对应的队列

5、通配符模式 Topic
    需要设置类型为topic的交换机，交换机和队列进行绑定，并且指定通配符方式的routing key，当发送消息到交换机后，交换机会根据routing key将消息发送到对应的队列

 

### 简单模式
简单模式：point to point 

![](https://gitee.com/superwow/pic/raw/master/img/20210328111646.png)

功能：一个生产者P发送消息到队列Q,一个消费者C接收

不指定交换机


### work queues
work queues：将消息分发给多个消费者

一个生产者，多个消费者，每个消费者获取到的消息唯一，多个消费者只有一个队列

不指定交换机


![](https://gitee.com/superwow/pic/raw/master/img/20210327094757.png)


### publish/subscribe
publish/subscribe：发布订阅模式

功能：一个生产者发送的消息会被多个消费者获取。一个生产者、一个交换机、多个队列、多个消费者

生产者：可以将消息发送到队列或者是交换机。

消费者：只能从队列中获取消息。

如果消息发送到没有队列绑定的交换机上，那么消息将丢失。

交换机不能存储消息，消息存储在队列中

![](https://gitee.com/superwow/pic/raw/master/img/20210327095119.png)

发布订阅模式因为消费者获得的都是同样的消息，所以特别适合数据提供商与应用商。



### rounting 路由模式

![](https://gitee.com/superwow/pic/raw/master/img/20210327100137.png)

交换机（direct） 根据 routing key 将消息发送到指定队列,消费者将队列绑定到交换机时需要指定路由key



### topics
topics 主题模式

生产者P发送消息到交换机X，type=topic，交换机根据绑定队列的routing key的值进行通配符匹配；符号#：匹配一个或者多个词lazy.# 可以匹配lazy.irs或者lazy.irs.cor

![](https://gitee.com/superwow/pic/raw/master/img/20210327100400.png)


交换机（topic）rotingkey 支持通配符

### rpc
rpc远程调用

## 安装

ubbuntu单机安装
<https://blog.csdn.net/qq_22638399/article/details/81704372>



docker 集群安装

<https://blog.csdn.net/belonghuang157405/article/details/83540148?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.control&dist_request_id=&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.control>


# RabbitMQ高级

## 过期时间TTL

 过期时间TTL表示可以对消息设置预期的时间，在这个时间内都可以被消费者接收获取；过了之后消息将自动被删除。RabbitMQ可以对消息和队列设置TTL。目前有两种方法可以设置。
  

- 第一种方法是通过队列属性设置，队列中所有消息都有相同的过期时间。
- 第二种方法是对消息进行单独设置，每条消息TTL可以不同。


如果上述两种方法同时使用，则消息的过期时间以两者之间TTL较小的那个数值为准。消息在队列的生存时间一旦超过设置的TTL值，就称为dead message被投递到死信队列， 消费者将无法再收到该消息。

### 设置队列TTL
```xml
<!--定义过期队列及其属性，不存在则自动创建-->
<rabbit:queue id="my_ttl_queue" name="my_ttl_queue" auto-declare="true">
    <rabbit:queue-arguments>
        <!--投递到该队列的消息如果没有消费都将在6秒之后被删除-->
        <entry key="x-message-ttl" value-type="long" value="6000"/>
    </rabbit:queue-arguments>
</rabbit:queue>
```
使用
```java
/**
    * 过期队列消息
    * 投递到该队列的消息如果没有消费都将在6秒之后被删除
    */
@Test
public void ttlQueueTest(){
    //路由键与队列同名
    rabbitTemplate.convertAndSend("my_ttl_queue", "发送到过期队列my_ttl_queue，6秒内不消费则不能再被消费。");
}
```

> 参数 x-message-ttl 的值 必须是非负 32 位整数 (0 <= n <= 2^32-1) ，以毫秒为单位表示 TTL 的值。这样，值 6000 表示存在于 队列 中的当前 消息 将最多只存活 6 秒钟。

如果不设置TTL,则表示此消息不会过期。如果将TTL设置为0，则表示除非此时可以直接将消息投递到消费者，否则该消息会被立即丢弃。

![](https://gitee.com/superwow/pic/raw/master/img/20210328164033.png)

## 设置消息TTL

消息的过期时间；只需要在发送消息（可以发送到任何队列，不管该队列是否属于某个交换机）的时候设置过期时间即可。在测试类中编写如下方法发送消息并设置过期时间到队列：
```java
/**
    * 过期消息
    * 该消息投递任何交换机或队列中的时候；如果到了过期时间则将从该队列中删除
    */
@Test
public void ttlMessageTest(){
    MessageProperties messageProperties = new MessageProperties();
    //设置消息的过期时间，5秒
    messageProperties.setExpiration("5000");
​
    Message message = new Message("测试过期消息，5秒钟过期".getBytes(), messageProperties);
    //路由键与队列同名
    rabbitTemplate.convertAndSend("my_ttl_queue", message);
}
```

expiration 字段以微秒为单位表示 TTL 值。且与 x-message-ttl 具有相同的约束条件。因为 expiration 字段必须为字符串类型，broker 将只会接受以字符串形式表达的数字。 
  

当同时指定了 queue 和 message 的 TTL 值，则两者中较小的那个才会起作用。

## 死信队列

DLX，全称为Dead-Letter-Exchange , 可以称之为死信交换机，也有人称之为死信邮箱。当消息在一个队列中变成死信(dead message)之后，它能被重新发送到另一个交换机中，这个交换机就是DLX ，绑定DLX的队列就称之为死信队列。


消息变成死信，可能是由于以下的原因：

- 消息被拒绝
- 消息过期
- 队列达到最大长度

DLX也是一个正常的交换机，和一般的交换机没有区别，它能在任何的队列上被指定，实际上就是设置某一个队列的属性。当这个队列中存在死信时，Rabbitmq就会自动地将这个消息重新发布到设置的DLX上去，进而被路由到另一个队列，即死信队列。

要想使用死信队列，只需要在定义队列的时候设置队列参数`x-dead-letter-exchange` 指定交换机即可。


## 延迟队列
在RabbitMQ中延迟队列可以通过 过期时间 + 死信队列 来实现；具体如下流程图所示：

![](https://gitee.com/superwow/pic/raw/master/img/20210328204904.png)

延迟队列的应用场景；如：

在电商项目中的支付场景；如果在用户下单之后的几十分钟内没有支付成功；那么这个支付的订单算是支付失败，要进行支付失败的异常处理（将库存加回去），这时候可以通过使用延迟队列来处理
在系统中如有需要在指定的某个时间之后执行的任务都可以通过延迟队列处理


## 消息确认机制

确认并且保证消息被送达，提供了两种方式：发布确认和事务。(两者不可同时使用)在channel为事务时，不可引入确认模式；同样channel为确认模式下，不可使用事务。

## 发布确认

当生产者发出消息时，可以配置消息成功确认和消息发送失败回调。


消息成功确认：

当生产者发出消息后，可以在通道上等待消息的确认，并发送下一条消息（异步）。

当生产者得到确认后，可以使用回调来处理确认消息，当因为rabbit mq内部错误导致消息发送失败后，可以回复一个 nack 的消息，生产者同样可以使用回调来处理 nack 消息。

## 事务消息

RabbitMQ中与事务机制有关的方法有三个：
- txSelect(), 用于将当前channel设置成transaction模式，
- txCommit()，用于提交事务，
- txRollback(), 用于回滚事务，


在通过txSelect开启事务之后，我们便可以发布消息给broker代理服务器了，如果txCommit提交成功了，则消息一定到达了broker了，如果在txCommit执行之前broker异常崩溃或者由于其他原因抛出异常，这个时候我们便可以捕获异常通过txRollback回滚事务了。






# 削峰填谷

某应用的处理能力是每秒10个请求，当突然接收到30个请求，系统可能会崩溃，也可能会无响应。所以我们需要将请求均摊到一段时间内，让系统平稳运行。

![](https://gitee.com/superwow/pic/raw/master/img/20210328105921.png)

在通常的开发中我们一般使用两种模式

- RPC模式：上游应用直接调用下游应用

- MQ模式：生产者将消息发送到MQ，消费者从MQ取出消息并消费

这两种方式 下游应用，无法控制到达自己的流量，如果调用方不限速很可能把被调用方冲垮。


## RPC模式解决

在调用方或者被调用方增加限流程序，拦截请求，从而使得应用平稳运行

## MQ模式

从MQ Server 推的模式改为 MQ client 拉取的模式

这样根据业务能力每次从 MQ server 拉取若干条，保证系统平稳运行

# 延迟队列

延迟队列，即消息进入队列后不会立即被消费，只有到达指定时间后才会被消费

需求
1. 下单后三十分钟未支付，取消订单，回滚库存

1. 新用户注册成功七天后，发送短信问候

![](https://gitee.com/superwow/pic/raw/master/img/20210328152150.png)


# 消息积压


情况：

- 消费者宕机

- 消费者消费能力不足


解决

1. 上线更多的消费者，进行正常消费

1. 启动消息持久化，将消息保存到磁盘中

1. 上线队列消息服务，将消息批量取出，存入数据库，在慢慢处理


# 消息丢失
消息丢失的场景主要分为：

- 消息在生产者丢失，

- 消息在RabbitMQ丢失，

- 消息在消费者丢失

## 消息在生产者丢失

采用rabbit mq的确认机制。

当消息被收到时，会发送给生产者确认消息，当消息发送失败时也会通知是生产者。

## 消息在RabbitMQ丢失

对于消息做持久化处理，但是这种并不完全保险还是需要在数据库做源数据记录。以确保数据不丢失。

## 消息在消费者丢失

1. 设置手动确认消息

1. 当消费者宕机时，MQ不会删除该消息。而是会把未确认的消息重新发送给队列的消费者。

## 重复消费

使用手动确认消息后，当因为网络抖动问题，mq并未收到消费者的回复，那么此时MQ就会将消息分发给其他消费者进行消费。

答：

使用消费者幂等性方案
