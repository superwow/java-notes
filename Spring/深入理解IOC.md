# IoC理论

IoC 全程称之为 `Inversion of Control` 翻译成`控制反转`。  

依赖注入即 DI - `Dependency Injection` 与IOC其实是同一概念的不同角度的描述。


如何理解`控制反转`呢？我们需要回答下面四个问题：

1. 谁控制谁？

1. 控制什么？

1. 为何是反转？

1. 哪些方面反转了？

在回答这四个问题之前，我们先看一下IOC的定义：

> IoC，就是由Spring 的IoC容器来负责对象的生命周期和对象之间的关系

如何理解这个定义呢？我们需要一个例子来阐述它，看完这个例子后也能很好的回答上面四个问题。

例如找女朋友，一般情况下我们根据需求来找到一个妹子，然后对其进行追求，随后确立关系，如下

```java
public class YongMan{
    //指向女朋友的引用
    private Girl girlFriend;

    //确立关系
     public void setGirlFriend(Girl girlFriend) {
        this.girlFriend = girlFriend;
    }

    public static void main(String[] args){
        //对这就是你
        YoungMan you = new YoungMan();

        //这就是那个姑娘
        Girl girl = new Girl("你的各种条件，追求方式等");

        //互相确立关系
        beautifulGirl.setBoyFriend(you);
        you.setBeautifulGirl(girl);
    }
}
```
在上面的例子中，我们需要某个对象，一般是采用直接创建的方式,即`new`。这种方式必须要我们面对每个环节，而且我们使用完成之后还要销毁它。这种情况下我们的对象与它所依赖的对象会耦合在一起。

所以我们需要思考一个问题，我们每次用到自己依赖的对象需要自己去创建吗？我们使用这个对象时，并不关心这个对象时从哪里来的如何创建的，只需要它在就可以了。

这个帮助我们创建对象的人， 并且将对象交付给我们的人，就是 `IoC`。`IoC`帮助我们创建对象并且将对象交付给我们，我们直接使用对象及其行为即可。

<img src="../../java-notes/img/IoC.jpg">

在没有引入 `IoC`时，我们想要的对象，需要我们手动创建并且赋值。有了`IoC` 以后，我们与需要对象的关系则由 `Ioc` 统一管理，我们需要什么告知`Ioc`,`Ioc` 便会送过来。

现在看上面四个问题，答案就比较明显了。

1. **谁控制谁**：原来是自己new一个对象，现在是`Ioc`，帮助我们创建对象并进行赋值。

1. **控制什么**：控制对象

1. **为何是反转**：没有`IoC`时，需要我们自己控制对象的创建等工作，这是正常的，有了Ioc以后，我们依赖的对象则需要容器创建并进行注入。这时 依赖的对象由主动获取变成了被动接受，这就是控制反转。

1. **哪些方面反转了**：所依赖对象的获取反转了，也就是交给Ioc去进行注入.

## 注入形式

Ioc提供了`构造注入`、`setter方法注入`、`接口注入`

### 构造器注入

构造器注入就是在构造方法中声明依赖对象的参数列表，让spring知道它需要哪些依赖对象。

```java
YoungMan(lGirl girl) {
    this.girl = girl;
}
```

### setter方法注入

对于`JavaBean`而言，我们一般通过`get`、`set`来访问和设置对象的属性。所以对象只需要提供所依赖对象的setter方法，就可以通过方法相应的依赖对象注入到对象中。

```java
public class YongMan{
    private Girl girlFriend;

     public void setGirlFriend(Girl girlFriend) {
        this.girlFriend = girlFriend;
    }
}
```

### 接口注入

这种方式就是我们常用的定义一个接口，在去定义它的实现类，然后将实现类注入IoC容器当中。

# 五大组件体系

先看下图

<img src="../../java-notes/img/AnnotationConfigApplicationContext.png">

该图为 `AnnotationConfigApplicationContext` 类的体系结构，它基本上包含了IoC中的绝大部分核心类和接口(过去是`ClassPathXmlApplicationContext` ).


## Resource体系

`Resource`接口位于 `org.springframework.core.io` 包下。它是对资源的抽象，它的每一个类都代表了一种资源的访问策略。

<img src="../../java-notes/img/Resource.jpg">

主要常用的有如下：

- ClassPathResource：类文件资源

- FileSystemResource：文件系统资源

- UrlResource：url资源

- ByteArrayResource：

- ServletContextResource：

- InputStreamResource。


### ResourceLoader体系
 
 有了统一的资源抽象，那就就应该有资源加载，Spring 利用`org.springframework.core.io`包下的 `ResourceLoader` 

 <img src="../../java-notes/img/ResourceLoader体系.jpg">

## BeanDefinition体系

`BeanDefinition` 位于 `org.springframework.beans.factory.config` 包下，用来描述 Spring 中的 bean的元信息。

<img src="../../java-notes/img/BeanDefinition体系.jpg">


## BeanDefinitionReader体系

`BeanDefinitionReader` 位于 `org.springframework.beans.factory.support` 包下，它是用来读取配置文件、注解，并将其转化为IoC内部的数据结构： `BeanDefinition`

<img src="../../java-notes/img/BeanDefinitionReader.jpg">

## BeanFactory体系

`BeanFactory` 位于 `org.springframework.beans.factory` 包下，是一个纯粹的`bean容器`,它是Ioc必备的数据结构。其中 `beanFactroy` 内部维护着一个 `BeanDefinition` 类型的Map，并根据 `BeanDefinition` 进行 `bean`的创建和管理.

<img src="../../java-notes/img/BeanFactory 体系.jpg">

- BeanFactory 有三个直接子类 

    - ListableBeanFactory、
    
    - HierarchicalBeanFactory 
    
    - AutowireCapableBeanFactory 。


- DefaultListableBeanFactory 为最终默认实现，它实现了所有接口。


## ApplicationContext 体系

ApplicationContext 位于 `org.springframework.context` 包中，它就是 `Spring` 容器，它叫做应用上下文。它继承了以下接口来提供服务。

1. 继承 `BeanFactory` 接口，提供Bean创建以及管理等功能。

1. 继承 `MessageSource` 接口，提供了国际化标准访问策略

1. 继承 `ApplicationEventPublisher` 接口，提供事件机制。

1. 扩展 `ResourLoader` 可以用来加载各种资源。

1. 对Web的支持

<img src="../../java-notes/img/ApplicationContext.jpg">




## 小结

上面五个体系是 Spring 最核心的部分。

我们可以这样理解：
1. `ResourceLoader` 与 `BeanDefinitionReader` 将资源加载为 `Resource` 与`BeanDefinition`

1. `BeanFactory` 解析 `beanDefinition` 并实例化并存储到一个Map中，然后将实例注入到需要这些实例的地方。

1. 而 `ApplicationContext` 就是实现以上功能的那个类。