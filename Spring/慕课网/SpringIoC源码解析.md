
# 核心接口和类

Spring 解决了关键的问题：将对象之间的关系使用配置进行管理

- 依赖注入：依赖关系在Spring的IoC容器管理

- 通过把对象包装在Bean中达到管理对象和进行额外操作的目的

## bean与BeanDefinition

- Bean的本质就是Java对象，只是这个对象的生命周期由容器进行管理

- Spring对Bean对象的控制体现在注解/配置文件中

根据注解或配置生成Bean的定义文件 - BeanDefinition

常用属性

- 作用范围 - @Scope
    - sigleton - 单例 - 默认

    - prototype - 多例

    - request

    - session

    - globalsession

容器初始化主要做的事情

配置文件 --读取--> resource --解析-->  BeanDefition --注册--> 容器

- 解析配置

- 定位与注册对象

Spring 要求所有容器 都要实现 BeanFactory (顶级)接口，即Bean的工厂接口。

BeanFactory是Spring容器的根接口，定义了bean工厂的最基础特性，
例如getBean(String beanName) - 根据Bean名称从Spring获取实例等等，它定是用来管理Bean的容器，Spring中生成的Bean都是由这个接口的实现类来管理的


## FactoryBean


FactoryBean也是顶级接口，它的本质也是一个Bean，但是这个Bean是用来生成其他Bean的，当有类实现了这个接口，Spring会把这些实现这个接口的Bean取出随后调用 getObject 来生成我们想要的Bean


- 组件扫描：自动发现应用容器中需要创建的Bean

- 自动装配：自动满足Bean之间的依赖 