package com.mybatisDemo;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * mybatis 测试
 *
 * @author 刘磊
 * @date 2021-02-25 ${time}
 */
public class MybatisTest {


    /**
     * 传统方式
     */
    public void test1() throws IOException {
        //1. 读取配置文件，读取成字节输入流，但是现在还没有解析
        InputStream resourceAsStream = Resources.getResourceAsStream("sqlMapConfig.xml");

        //2. 解析配置文件，封装成 configuration对象。
        //   创建DefaultSqlSessionFactory对象
        SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder()
                .build(resourceAsStream);

        //3. 生产了DefaultSqlSession 实例对象
        //   设置了事务不自动提交
        //   完成了Executor 对象创建
        SqlSession sqlSession = sessionFactory.openSession();

        //4. 根据statement id 从configuration 获取了指定的 MappedStatement 对象
        //   将任务委派给了，executor执行器，进行执行
        List<Object> objectList = sqlSession.selectList("namespace.id");

    }

    /**
     * mapper方式
     */
    public void test2() throws IOException {
        //1. 读取配置文件，读取成字节输入流，但是现在还没有解析
        InputStream resourceAsStream = Resources.getResourceAsStream("sqlMapConfig.xml");

        //2. 解析配置文件，封装成 configuration对象。
        //   创建DefaultSqlSessionFactory对象
        SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder()
                .build(resourceAsStream);

        //3. 生产了DefaultSqlSession 实例对象
        //   设置了事务不自动提交
        //   完成了Executor 对象创建
        SqlSession sqlSession = sessionFactory.openSession();

        UserMapper mapper = sqlSession.getMapper(UserMapper.class);

    }


}
