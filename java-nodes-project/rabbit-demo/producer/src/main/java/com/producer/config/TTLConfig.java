package com.producer.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * TTL 过期时间
 *
 * @author 刘磊
 * @date 2021-03-28 ${time}
 */
@Configuration
public class TTLConfig {


    //声明队列
    @Bean
    public Queue workQ1() {
        Map<String, Object> arguments = new HashMap<String, Object>();
        arguments.put("x-message-ttl", 6000);

        return new Queue("my_ttl_queue", false, false, false, arguments);
    }

}
