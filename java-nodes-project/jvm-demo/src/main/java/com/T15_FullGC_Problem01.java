package com;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author 刘磊
 * @date 2021-03-15 ${time}
 */
public class T15_FullGC_Problem01 {

    private static class CarInfo {
        BigDecimal price = new BigDecimal(0.0);
        String name = "张三";
        int age = 5;
        Date birthDate = new Date();

        public void m() {
        }

    }

    /**
     * 每隔一段时间运行一次的线程池
     */
    private static ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(50,
            new ThreadPoolExecutor.DiscardOldestPolicy());


    public static void main(String[] args) throws InterruptedException {
        executor.setMaximumPoolSize(50);
        for (; ; ) {
            modelFilter();
            Thread.sleep(100);
        }
    }

    private static void modelFilter() {
        List<CarInfo> taskList = getAllCarInfo();

        taskList.forEach(info -> {
            executor.scheduleWithFixedDelay(() -> {
                info.m();
            }, 2, 3, TimeUnit.SECONDS);

        });

    }

    private static List<CarInfo> getAllCarInfo() {
        List<CarInfo> taskList = new ArrayList<CarInfo>();

        for (int i = 0; i < 100; i++) {
            CarInfo ci = new CarInfo();
            taskList.add(ci);
        }
        return taskList;
    }
}
