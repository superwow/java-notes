package com.autoconfig.common;

/**
 * @author 刘磊
 * @date 2019-10-30 ${time}
 */
public class Red {

    private String a = "red";


    public Red() {
    }

    public Red(String a) {
        this.a = a;
    }

    @Override
    public String toString() {
        return "Color{" +
                "a='" + a + '\'' +
                '}';
    }
}
