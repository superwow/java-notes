package com.autoconfig.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 刘磊
 * @date 2021-02-23 ${time}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person {

    String name;
}
