package com.autoconfig.common;

/**
 * @author 刘磊
 * @date 2019-10-30 ${time}
 */
public class Blue {
    private String a = "blue";


    public Blue() {
    }

    public Blue(String a) {
        this.a = a;
    }

    @Override
    public String toString() {
        return "Color{" +
                "a='" + a + '\'' +
                '}';
    }
}
