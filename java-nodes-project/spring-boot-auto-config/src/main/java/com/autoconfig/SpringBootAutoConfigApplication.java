package com.autoconfig;

import com.autoconfig.common.Blue;
import com.autoconfig.common.Dog;
import com.autoconfig.common.Person;
import com.autoconfig.common.Red;
import com.autoconfig.configurationDemo.Config;
import com.springBootStart.Girl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class SpringBootAutoConfigApplication {

    public static void main(String[] args) {




        ConfigurableApplicationContext run = SpringApplication.run(SpringBootAutoConfigApplication.class, args);


        Girl bean = run.getBean(Girl.class);
        System.out.println("bean = " + bean);


//        boolean windows = run.containsBean("windows");
//        System.out.println("windows = " + windows);
//        boolean linux = run.containsBean("linux");
//        System.out.println("linux = " + linux);


        //1. 从容器中获取组件
//        Person person01 = run.getBean("person01", Person.class);
//        Person person02 = run.getBean("person01", Person.class);
//
//
//        System.out.println("getBean 获取：" + (person01 == person02));
//
//
//        //2. 调用配置文件实例的 @Bean 修饰的方法
//        Config bean = run.getBean(Config.class);
//        person01 = bean.personBean();
//        person02 = bean.personBean();
//
//        System.out.println("调用方法 获取：" + (person01 == person02));
//
//
//        Dog dog = run.getBean(Dog.class);
//        System.out.println("dog = " + dog);


//        Person person01 = run.getBean(Person.class);
//        System.out.println("person01 = " + person01);
//
//        Dog dog = run.getBean(Dog.class);
//        System.out.println("dog = " + dog);


//        Blue blue = run.getBean(Blue.class);
//        System.out.println("blue = " + blue);





    }

}
