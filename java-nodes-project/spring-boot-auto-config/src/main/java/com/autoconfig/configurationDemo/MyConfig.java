package com.autoconfig.configurationDemo;

import com.autoconfig.common.Person;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 刘磊
 * @date 2021-02-24 ${time}
 */
@Configuration
public class MyConfig {


    @Bean("p-01")
    public Person p01() {

        return new Person("01");
    }

    @Bean("p-02")
    public Person p02() {
        return new Person("02");
    }


}
