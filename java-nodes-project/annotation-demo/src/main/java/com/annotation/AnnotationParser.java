package com.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @author 刘磊
 * @date 2021-02-20 ${time}
 */
public class AnnotationParser {

    /**
     * 解析类的注解
     */
    public static void parseTypeAnnotation() throws ClassNotFoundException {
        Class clazz = Class.forName("com.annotation.ImoocCourse");

        //这里获取是类的注解
        Annotation[] annotations = clazz.getAnnotations();

        for (Annotation annotation : annotations) {
            if (annotation instanceof CourseInfoAnnotation) {
                CourseInfoAnnotation courseInfoAnnotation = (CourseInfoAnnotation) annotation;
                String courseName = courseInfoAnnotation.courseName();
                System.out.println(courseName);
                System.out.println(courseInfoAnnotation.courseTag());
                System.out.println(courseInfoAnnotation.courseProfile());
                System.out.println(courseInfoAnnotation.courseIndex());
            }


        }
    }

    /**
     * 解析成员变量的注解
     */
    public static void parseFieldeAnnotation() throws ClassNotFoundException {
        Class clazz = Class.forName("com.annotation.ImoocCourse");

        for (Field field : clazz.getDeclaredFields()) {
            boolean present = field.isAnnotationPresent(PersoninfoAnnotation.class);
            if (present) {
                PersoninfoAnnotation annotation = field.getAnnotation(PersoninfoAnnotation.class);
                System.out.println(annotation.name());
                System.out.println(annotation.age());
                System.out.println(annotation.gender());
                for (String s : annotation.language()) {
                    System.out.println(s);
                }

            }
        }
    }

    /**
     * 解析方法的注解
     */
    public static void parseMethodeAnnotation() throws ClassNotFoundException {
        Class clazz = Class.forName("com.annotation.ImoocCourse");

        for (Method method : clazz.getMethods()) {
            boolean annotationPresent = method.isAnnotationPresent(CourseInfoAnnotation.class);
            if (annotationPresent) {
                CourseInfoAnnotation annotation = method.getAnnotation(CourseInfoAnnotation.class);
                String courseName = annotation.courseName();
                System.out.println(courseName);
                System.out.println(annotation.courseTag());
                System.out.println(annotation.courseProfile());
                System.out.println(annotation.courseIndex());
            }
        }
    }

    public static void main(String[] args) throws ClassNotFoundException {
        parseTypeAnnotation();
        System.out.println("------");
        parseFieldeAnnotation();
        System.out.println("------");
        parseMethodeAnnotation();
    }
}
