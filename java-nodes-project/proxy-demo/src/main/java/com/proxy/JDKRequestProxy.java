package com.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 代理类 & 测试执行
 *
 * @author 刘磊
 * @date 2021-02-19 ${time}
 */
public class JDKRequestProxy implements InvocationHandler {
    /**
     * 被代理对象
     */
    private Object target;

    /**
     * 获取代理实例
     *
     * @param target
     * @return
     */
    public Object getInstance(Object target) {
        this.target = target;
        Class<?> clazz = target.getClass();
        return Proxy.newProxyInstance(
                clazz.getClassLoader(),
                clazz.getInterfaces(),
                this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args)
            throws Throwable {

        System.out.println("request执行开始");
        Object result = method.invoke(this.target, args);
        System.out.println("request执行结束");
        return result;
    }

    public static void main(String[] args) {
        Controller controller = (Controller) new JDKRequestProxy()
                .getInstance(new RequestController());
        System.out.println(controller.getClass());
        controller.request();
    }
}
