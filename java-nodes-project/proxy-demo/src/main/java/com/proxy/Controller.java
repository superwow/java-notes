package com.proxy;

/**
 * 代理接口
 *
 * @author 刘磊
 * @date 2021-02-19 ${time}
 */
public interface Controller {
    void request();
}
