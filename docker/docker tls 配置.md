# docker tls远程连接

## 生成证书

官网步骤

<https://docs.docker.com/engine/security/https/>

注意下面这一行是设置IP白名单和域名的

`echo subjectAltName = DNS:$HOST,IP:10.10.10.20,IP:127.0.0.1 >> extfile.cnf`

如果使用IP则参考

<https://blog.csdn.net/ChineseYoung/article/details/83107353>

生成证书之后参考
<https://www.cnblogs.com/bolingcavalry/archive/2019/09/07/11482827.html>

配置docker 【Docker的TLS连接设置（A机器）】下的
 - 配置ExecStar 参数
 - 命令 `systemctl daemon-reload && systemctl restart docker`
 
测试则看官网连接最下面一行


## 集群

只需要看

配置portainer添加远程docker集群

<https://tankeryang.github.io/posts/docker%E9%85%8D%E7%BD%AETLS%E8%AE%A4%E8%AF%81%E5%BC%80%E5%90%AF%E8%BF%9C%E7%A8%8B%E8%AE%BF%E9%97%AE/>

## intellij连接docker

![](intellij连接docker.jpg);

