package com.health.zuu.filter;

import com.health.common.util.RequestUtil;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 刘磊
 * @date 2021-03-03 ${time}
 */
@Slf4j
public class FilterPre extends ZuulFilter {

    /**
     * 拦截器类型
     *
     * @return
     */
    @Override
    public String filterType() {
//
//        pre：可以在请求被路由之前调用
//        route：在路由请求时候被调用
//        post：在route和error过滤器之后被调用
//        error：处理请求时发生错误时被调用
        return FilterConstants.PRE_TYPE;
    }

    /**
     * 过滤器排序
     *
     * @return
     */
    @Override
    public int filterOrder() {
        // 优先级为0，数字越大，优先级越低
        return FilterConstants.PRE_DECORATION_FILTER_ORDER - 1;
    }

    /**
     * 是否执行该过滤器
     * <p>
     * 可以在这里放行一些路由，如登录、基础信息、回调等
     *
     * @return
     */
    @Override
    public boolean shouldFilter() {

        //获取request
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();

        //获取请求 uri
        String uri = RequestContext.getCurrentContext().getRequest().getServletPath();

        //获取IP
        String requestIp = RequestUtil.getIPAddress(request);
        log.info("请求IP{},请求地址{}", requestIp, uri);

        return false;
    }

    @Override
    public Object run() {
        //获取request
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();

        //获取请求 uri
        String uri = RequestContext.getCurrentContext().getRequest().getServletPath();

        //获取IP
        String requestIp = RequestUtil.getIPAddress(request);
        log.info("请求IP{},请求地址{}", requestIp, uri);


        String username = request.getParameter("username");// 获取请求的参数
        if (null != username && username.equals("chhliu")) {// 如果请求的参数不为空，且值为chhliu时，则通过
            ctx.setSendZuulResponse(true);// 对该请求进行路由
            ctx.setResponseStatusCode(200);
            ctx.set("isSuccess", true);// 设值，让下一个Filter看到上一个Filter的状态
            return null;
        } else {
            ctx.setSendZuulResponse(false);// 过滤该请求，不对其进行路由
            ctx.setResponseStatusCode(401);// 返回错误码
            ctx.setResponseBody("{\"result\":\"username is not correct!\"}");// 返回错误内容
            ctx.set("isSuccess", false);
            return null;
        }
    }
}
