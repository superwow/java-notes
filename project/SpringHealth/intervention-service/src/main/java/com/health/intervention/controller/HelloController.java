package com.health.intervention.controller;

import com.health.common.bean.User;
import com.health.intervention.client.UserServiceClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 刘磊
 * @date 2021-03-08 ${time}
 */
@RestController
@Slf4j
public class HelloController {

    @Autowired
    UserServiceClient userServiceClient;

    @RequestMapping("/hello")
    public User hello() {
        log.info("发出调用 ");
        return userServiceClient.getUser("LL");
    }
}
