package com.health.intervention.client;

import com.health.common.bean.User;
import com.health.intervention.service.GetUserCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author 刘磊
 * @date 2021-03-05 ${time}
 */
@Component
public class UserServiceClient {

    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    UserServiceClient userServiceClient;


    @Bean
    public RestTemplate newRestTemplate() {
        return new RestTemplate();
    }

    @HystrixCommand(
            groupKey = "UserServiceClient-1",
            commandKey = "getUser-1",
            threadPoolKey = "user-1",
            threadPoolProperties =
                    {
                            @HystrixProperty(name = "coreSize", value = "2"),
                            @HystrixProperty(name = "maxQueueSize", value = "10")
                    },
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
                    //一个滑动窗口内最小的请求数
                    @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "5"),
                    //错误比率阈值
                    @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "75"),
                    //触发熔断的时间值
                    @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "7000"),
                    //一个滑动窗口的时间长度
                    @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "15000"),
                    //一个滑动窗口被划分的数量
                    @HystrixProperty(name = "metrics.rollingStats.numBuckets", value = "5")},
            fallbackMethod = "getUserFallback"
    )
    public User getUser(String userName) {

        ResponseEntity<User> user = null;
//
//        List<ServiceInstance> instances = discoveryClient.getInstances("user-service");
//        System.out.println("instances = " + instances);
//        if (instances.size() <= 0) {
//            System.out.println("没找到");
//            return null;
//        }
//
//        String userName = "ll";
//        String userserviceUri = String.format("%s/users/%s", instances.get(0).getUri()
//                .toString(), userName);
//        System.out.println("userserviceUri = " + userserviceUri);

        user = restTemplate.exchange("http://DESKTOP-HLVAA1P:8082/users/" + userName, HttpMethod.GET, null, User.class);
        System.out.println(user.getBody().toString());

        return user.getBody();

    }

    private User getUserFallback(String userName) {
        return new User(0L, "xxx", userName + " - error");
    }

//    @PostConstruct
    public void test() throws InterruptedException {

        AtomicInteger a = new AtomicInteger(0);


            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    for (int i = 0; i < 50; i++) {

                        new Thread(()->{
                            userServiceClient.getUser(String.valueOf(a.incrementAndGet()));
                        }).start();

                    }

                }
            }).start();

//        User user = command.execute();
    }



}
