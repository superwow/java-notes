package com.health.intervention;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author 刘磊
 * @date 2021-03-04 ${time}
 */
@SpringCloudApplication
public class InterventionApplication {
    public static void main(String[] args) {
        SpringApplication.run(InterventionApplication.class, args);
    }
}
