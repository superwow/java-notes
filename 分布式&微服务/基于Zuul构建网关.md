

# 什么是API网关

在微服务架构中，业务变化迅速，另外服务间拆分较多，所以需要一个统一的入口，来标准化客户端与服务端的访问

![20210303143647](http://qiniu.liulei.life/20210303143647.png)

当我们有了一个统一的接入层后，我们就可以在网关层进行一些业务或者非业务处理。

![20210303144213](http://qiniu.liulei.life/20210303144213.png)

# 使用zuul构建网关


引入依赖
```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-zuul</artifactId>
</dependency>
```
然后我们创建 Bootstrap 类，代码如下：
```java
@SpringBootApplication
@EnableZuulProxy//开启网关注解
public class ZuulServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZuulServerApplication.class, args);
    }
}
```
配置
```yml
server:
  port: 5555

eureka:
  instance:
    preferIpAddress: true
  client:
    registerWithEureka: true
    fetchRegistry: true
    service-url:
      defaultZone: http://127.0.0.1:8761/eureka/
spring:
  application:
    name: zuul-server
management:
  endpoints:
    web:
      exposure:
        #添加需要暴露出去的路径，"*"表示所有路径
        include: "*"
```
API 网关在其服务的消费者和提供者之间提供了一层反向代理，充当着前置负载均衡器的角色。所以，API 网关的定位决定了 Zuul 需要依赖 Ribbon。而 Zuul 可以与 Ribbon 完成无缝集成，我们在后续内容中会看到相关的示例。

# 如何使用 Zuul 实现服务路由

对于 API 网关而言，最重要的功能就是服务路由，即通过 Zuul 访问的请求会路由并转发到对应的后端服务中。通过 Zuul 进行服务访问的 URL 通用格式如下所示：

> http://zuulservice:5555/service

其中 zuulservice 代表 Zuul 服务器的地址。而这里的 service 所对应的后端服务的确定就需要依赖位于 Zuul 中的服务路由信息。在 Zuul 中，服务路由信息的设置可以使用以下几种常见做法，让我们一一来展开讨论。

## 基于服务发现映射服务路由

Zuul 可以基于注册中心的服务发现机制实现自动化服务路由功能。所以使用 Zuul 实现服务路由最常见的、也最推荐的做法就是利用这种自动化的路由映射关系来确定路由信息。

从开发角度讲，系统自动映射也最简单。我们不需要做任何事情，因为在 Eureka 中已经保存了各种服务定义信息。而服务定义信息中包含了各个服务的名称，所以 Zuul 就可以把这些服务的名称与目标服务进行自动匹配。匹配的规则就是直接将目标服务映射到服务名称上。

例如，我们在 user-service 的配置文件中通过以下配置项指定了该服务的名称为 userservice：
```yml
spring:
    application:
        name: userservice
```
这样我们就可以通过http://zuulservice:5555/userservice访问到该服务，注意该 URL 中的目标服务就是 userservice，与服务定义中的名称保持一致。

## 从zuul取出服务列表与路由

访问以下地址

<http://localhost:5555/actuator/routes>

如果报404 的话可以尝试以下配置
```yml
//2.2.4 及以后
management:
  endpoints:
    web:
      exposure:
        #添加需要暴露出去的路径，"*"表示所有路径
        include: "*"
```
返回
```json
{"/user-service/**":"user-service"}
```
## 基于动态配置映射服务路由

基于服务发现机制的系统自动映射非常方便，但也有明显的局限性。在日常开发过程中，我们往往对服务映射关系有更多的定制化需求，比方说不使用默认的服务名称来命名目标服务，或者在各个请求路径之前加一个统一的前缀（Prefix）等。Zuul 充分考虑到了这些需求，开发和运维人员可以通过配置实现服务路由的灵活映射。

首先我们可以在 zuul-server 工程下的 application.yml 配置文件中为 user-service 配置特定的服务名称与请求地址之间的映射关系，如下所示：
```yml
zuul:
    routes:
        userservice: /user/**
```

注意，这里我们使用 /user 来为 user-service 指定请求根地址。现在我们访问 <http://zuulservice:5555/user/> 时，就相当于将请求发送给了 Eureka 中的 userservice 实例。

现在我们重启 zuul-server 并访问 <http://localhost:5555/actuator/routes> 端点，得到的服务路由映射关系就变成了如下结果：

```json
{
    "/user/**":"userservice",
    "/userservice/**":"userservice"
}
```
可以看到在原有路由信息的基础上，Zuul 生成了一条新的路由信息，对应配置文件中的配置。现在，相当于将访问某一个服务的入口变成了两个：一个是系统自动映射的路由，一个是通过配置所生成的路由。当我们不希望系统自动映射的路由被外部使用时，我们就可以通过 ignored-services 配置项把它们从服务路由中去掉。再次以 userservice 为例，ignored-services 配置项的使用方法如下所示。
```yml
zuul:
    routes:
        ignored-services: 'userservice'
        userservice: /user/**
```


让我们考虑另外一个比较常见的应用场景，在一个大型的微服务架构中，可能会有非常多的微服务。这就需要对这些服务进行全局性的规划，可以通过模块或子系统的方式进行管理。表现在路由信息上，在各个服务请求地址上添加一个前缀用来标识模块和子系统是一项最佳实践。针对这种场景，就可以用到 Zuul 提供的“prefix”配置项，示例如下所示：
```yml
zuul:
    prefix:  /springhealth
    routes:
        ignored-services: 'userservice'
        userservice: /user/**
```
现在访问 <http://localhost:5555/actuator/routes> 端点，可以看到所配置的前缀已经生效，如下所示：
```json
{
    "/springhealth/user/**":"userservice" 
}
```
## 基于静态配置映射服务路由

当我们的系统中存在一个第三方服务时，该服务无法注册到我们的注册中心中来。我们可以这样配置。
```yml
zuul:
    routes:
        thirdpartyservice:
            path: /thirdpartyservice/**
            url: http://thirdparty.com/thirdpartyservice
```
现在的服务路由信息就会变成如下结果：
```json
{

    "/springhealth/thirdpartyservice/**":"http://thirdparty.com/thirdpartyservice",
    "/springhealth/user/**":"userservice"
}
```
在上文中介绍 Zuul 的定位时，我们提到 Zuul 能够与 Ribbon 进行整合。而这种整合也来自手工设置静态服务路由的方式，具体实现方式如下所示：
```yml
zuul:
    routes:
        thirdpartyservice:
            path: /thirdpartyservice/**
            serviceId: thirdpartyservice

ribbon:
    eureka:
        enabled: false

thirdpartyservice:
    ribbon:
        listOfServers: http://thirdpartyservice1:8080,http://thirdpartyservice2:8080
```

这里，我们配置了一个 thirdpartyservice 路由信息，通过“/ thirdpartyservice /**”映射到 serviceId 为“thirdpartyservice”的服务中。然后我们希望通过自定义 Ribbon 的方式来实现客户端负载均衡，这时候就需要关闭 Ribbon 与 Eureka 之间的关联。可以通过“ribbon.eureka.enabled: false”配置项完成这一目标。在不使用 Eureka 的情况下，我们需要手工指定 Ribbon 的服务列表。“users.ribbon.listOfServers”配置项为我们提供了这方面的支持，如在上面的示例中“http://thirdpartyservice1:8080，http://thirdpartyservice2:8080”就为 Ribbon 提供了两个服务定义作为实现负载均衡的服务列表。