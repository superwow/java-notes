
# Ribbon 基本架构

在深入讨论 Netflix Ribbon 之前，我们可以做一个简单的抽象。作为一款客户端负载均衡工具，要做的事情无非就是两件：

- 第一件事情：获取注册中心中的服务器列表；
- 第二件事情：在这个服务列表中选择一个服务进行调用。

针对这两个问题，Netflix Ribbon 提供了自身的一套基本架构，并抽象了一批核心类，让我们来一起看一下核心类。

## Netflix Ribbon 中的核心类

Netflix Ribbon 的核心接口 ILoadBalancer 就是围绕着上述两个问题来设计的，该接口位于 com.netflix.loadbalancer 包下，定义如下：
```java
public interface ILoadBalancer {

    //添加后端服务
    public void addServers(List<Server> newServers);

    //选择一个后端服务
    public Server chooseServer(Object key); 

    //标记一个服务不可用
    public void markServerDown(Server server);

    //获取当前可用的服务列表
    public List<Server> getReachableServers();

    //获取所有后端服务列表
    public List<Server> getAllServers();
}
```

ILoadBalancer 接口的类层结构如下所示：
![20210303093719](http://qiniu.liulei.life/20210303093719.png)

其中 AbstractLoadBalancer 是个抽象类，只定义了两个抽象方法，并不构成一种模板方法的结构。所以我们直接来看 ILoadBalancer 接口，该接口最基本的实现类是 BaseLoadBalancer，可以说负载均衡的核心功能都可以在这个类中得以实现。这个类代码非常多且杂，我们在理解上需要对其进行裁剪，从而抓住重点。

我们先来梳理 BaseLoadBalancer 包含的作为一个负载均衡器应该具备的一些核心组件，比较重要的有以下三个。

- IRule

    IRule 接口是对负载均衡策略的一种抽象，可以通过实现这个接口来提供各种适用的负载均衡算法，我们在上一课时介绍 @RibbonClient 注解时已经看到过这个接口。该接口定义如下：
    ```java
    public interface IRule{
        //核心方法，负责负载均衡
        public Server choose(Object key);
        public void setLoadBalancer(ILoadBalancer lb);
        public ILoadBalancer getLoadBalancer();
    }
    ```
- IPing

    IPing 接口判断目标服务是否存活，定义如下：
    ```java
    public interface IPing {
        public boolean isAlive(Server server);
    }
    ```
    可以看到 IPing 接口中只有一个 isAlive() 方法，通过对服务发出"Ping"操作来获取服务响应，从而判断该服务是否可用。

- LoadBalancerStats

    LoadBalancerStats 类记录负载均衡的实时运行信息，用来作为负载均衡策略的运行时输入。

    注意，在 BaseLoadBalancer 内部维护着 allServerList 和 upServerList 这两个线程的安全列表，所以对于 ILoadBalancer 接口定义的 addServers、getReachableServers、getAllServers 这几个方法而言，主要就是对这些列表的维护和管理工作。以 addServers 方法为例，它的实现如下所示：
    ```java
    public void addServers(List<Server> newServers) {

        if (newServers != null && newServers.size() > 0) {
            try {

                ArrayList<Server> newList = new ArrayList<Server>();
                newList.addAll(allServerList);
                newList.addAll(newServers);
                setServersList(newList);
            } catch (Exception e) {

                logger.error("LoadBalancer [{}]: Exception while adding Servers", name, e);
            }
        }
    }
    ```

    果然，这里使用了 IRule 接口的 choose 方法。接下来就让我们看看 Ribbon 中的 IRule 接口为我们提供了具体哪些负载均衡算法。

# Netflix Ribbon 中的负载均衡策略

一般而言，负载均衡算法可以分成两大类，即静态负载均衡算法和动态负载均衡算法。静态负载均衡算法比较容易理解和实现，典型的包括随机（Random）、轮询（Round Robin）和加权轮询（Weighted Round Robin）算法等。所有涉及权重的静态算法都可以转变为动态算法，因为权重可以在运行过程中动态更新。例如动态轮询算法中权重值基于对各个服务器的持续监控并不断更新。另外，基于服务器的实时性能分析分配连接是常见的动态策略。典型动态算法包括源 IP 哈希算法、最少连接数算法、服务调用时延算法等。

回到 Netflix Ribbon，IRule 接口的类层结构如下图所示：

![20210303095330](http://qiniu.liulei.life/20210303095330.png)

- BestAvailableRule 策略

    选择一个并发请求量最小的服务器，逐个考察服务器然后选择其中活跃请求数最小的服务器。

- WeightedResponseTimeRule 策略

    该策略与请求的响应时间有关，显然，如果响应时间越长，就代表这个服务的响应能力越有限，那么分配给该服务的权重就应该越小。而响应时间的计算就依赖于前面介绍的 ILoadBalancer 接口中的 LoadBalancerStats。WeightedResponseTimeRule 会定时从 LoadBalancerStats 读取平均响应时间，为每个服务更新权重。权重的计算也比较简单，即每次请求的响应时间减去每个服务自己平均的响应时间就是该服务的权重。

- AvailabilityFilteringRule 策略

    通过检查 LoadBalancerStats 中记录的各个服务器的运行状态，过滤掉那些处于一直连接失败或处于高并发状态的后端服务器。