# Ribbon组件


在服务治理体系中，服务调用是和 **负载均衡**结合在一起的。Ribbon 就是 Spring Cloud

![20210303084404](http://qiniu.liulei.life/20210303084404.png)

# Ribbon的核心功能

Ribbon 组件同样来自 Netflix，它的定位是一款用于提供客户端负载均衡的工具软件。Ribbon 会自动地基于某种内置的负载均衡算法去连接服务实例，我们也可以设计并实现自定义的负载均衡算法并嵌入 Ribbon 中。同时，Ribbon 客户端组件提供了一系列完善的辅助机制用来确保服务调用过程的可靠性和容错性，包括连接超时和重试等。Ribbon 是客户端负载均衡机制的典型实现方案，所以需要嵌入在服务消费者的内部进行使用。

因为 Netflix Ribbon 本质上只是一个工具，而不是一套完整的解决方案，所以 Spring Cloud Netflix Ribbon 对 Netflix Ribbon 做了封装和集成，使其可以融入以 Spring Boot 为构建基础的技术体系中。基于 Spring Cloud Netflix Ribbon，通过注解就能简单实现在面向服务的接口调用中，自动集成负载均衡功能，使用方式主要包括以下两种：

## 使用 @LoadBalanced 注解

@LoadBalanced 注解用于修饰发起 HTTP 请求的 RestTemplate 工具类，并在该工具类中自动嵌入客户端负载均衡功能。开发人员不需要针对负载均衡做任何特殊的开发或配置。

## 使用 @RibbonClient 注解

Ribbon 还允许你使用 @RibbonClient 注解来完全控制客户端负载均衡行为。这在需要定制化负载均衡算法等某些特定场景下非常有用，我们可以使用这个功能实现更细粒度的负载均衡配置。

在今天的课程中，我们会对这两种使用方式做详细展开，而在下一课时中，我们还将会从源码层面讨论其背后的实现机制。事实上，无论使用哪种方法，我们首先需要明确如何通过 Eureka 提供的 DiscoveryClient 工具类查找注册在 Eureka 中的服务，这是 Ribbon 实现客户端负载均衡的基础。上一课时已经介绍了 DiscoveryClient 与 Eureka 服务器的通信机制，今天我们来看看如何通过 DiscoveryClient 获取服务信息。

## 使用 DiscoveryClient 获取服务实例信息

获取服务列表
```java
@Autowired
DiscoveryClient discoveryClient;

List<String> serviceNames = discoveryClient.getServices();
```
获取服务实例的详细信息
```java
List<ServiceInstance> serviceInstances = discoveryClient.getInstances(serviceName);
```
```java
public interface ServiceInstance {
    //服务实例的唯一性 Id
    String getServiceId();
    //主机
    String getHost();
    //端口
    int getPort();
    //URI
    URI getUri();
    //元数据
    Map<String, String> getMetadata();
    …
}
```

显然，一旦获取了一个 ServiceInstance 列表，我们就可以基于常见的随机、轮询等算法来实现客户端负载均衡，也可以基于服务的 URI 信息等实现各种定制化的路由机制。一旦确定负载均衡的最终目标服务，就可以使用 HTTP 工具类来根据服务的地址信息发起远程调用。

```java
@Bean
public RestTemplate getRestTemplate() {
    return new RestTemplate();
}

@Autowired
RestTemplate restTemplate;

@PostConstruct
public void init() throws InterruptedException {

    //
    List<ServiceInstance> instances = discoveryClient.getInstances("user-service");

    if (instances.size() <= 0) {
        //没找到服务实例
        return;
    }

    String userName = "ll";
    String userserviceUri = String.format("%s/users/%s", instances.get(0).getUri()
            .toString(), userName);

    ResponseEntity<User> user =
            restTemplate.exchange(userserviceUri, HttpMethod.GET, null, User.class, userName);
    System.out.println("user = " + user);
}
```

## 通过 @Loadbalanced 注解调用服务

如果你掌握了 RestTemplate 的使用方法，那么在 Spring Cloud 中基于 Ribbon 来实现负载均衡非常简单，要做的事情就是在 RestTemplate 上添加一个注解，仅此而已。
```java
@LoadBalanced
@Bean
public RestTemplate getRestTemplate(){
    return new RestTemplate();
}
```
## 通过 @RibbonClient 注解自定义负载均衡策略

在前面的演示中，我们完全没有感觉到 Ribbon 组件的存在。在基于 @LoadBalanced 注解执行负载均衡时，采用的是 Ribbon 内置的负载均衡机制。默认情况下，Ribbon 使用的是轮询策略，我们无法控制具体生效的是哪种负载均衡算法。但在有些场景下，我们就需要对负载均衡这一过程进行更加精细化的控制，这时候就可以用到 @RibbonClient 注解。Spring Cloud Netflix Ribbon 提供 @RibbonClient 注解的目的在于通过该注解声明自定义配置，从而来完全控制客户端负载均衡行为。@RibbonClient 注解的定义如下：
```java
public @interface RibbonClient {

    //同下面的 name 属性
    String value() default "";

    //指定服务名称
    String name() default "";

    //指定负载均衡配置类
    Class<?>[] configuration() default {};
}
```

通常，我们需要指定这里的目标服务名称以及负载均衡配置类。所以，为了使用 @RibbonClient 注解，我们需要创建一个独立的配置类，用来指定具体的负载均衡规则。以下代码演示的就是一个自定义的配置类 SpringHealthLoadBalanceConfig：

```java
@Configuration

public class SpringHealthLoadBalanceConfig{

    @Autowired
    IClientConfig config;

    @Bean
    @ConditionalOnMissingBean
    public IRule springHealthRule(IClientConfig config) {
        return new RandomRule();
    }
}
```

显然该配置类的作用是使用 RandomRule 替换 Ribbon 中的默认负载均衡策略 RoundRobin。我们可以根据需要返回任何自定义的 IRule 接口的实现策略，关于 IRule 接口的定义放在下一课时进行讨论。


有了这个 SpringHealthLoadBalanceConfig 之后，我们就可以在调用特定服务时使用该配置类，从而对客户端负载均衡实现细粒度的控制。在 intervention-service 中使用 SpringHealthLoadBalanceConfig 实现对 user-service 访问的示例代码如下所示：
```java
@SpringBootApplication

@EnableEurekaClient

@RibbonClient(name = "userservice", configuration = SpringHealthLoadBalanceConfig.class)

public class InterventionApplication{

	@Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    public static void main(String[] args) {
        SpringApplication.run(InterventionApplication.class, args);
    }
}
```

可以注意到，我们在 @RibbonClient 中设置了目标服务名称为 userservice，配置类为 SpringHealthLoadBalanceConfig。现在每次访问 user-service 时将使用 RandomRule 这一随机负载均衡策略。

对比 @LoadBalanced 注解和 @RibbonClient 注解，如果使用的是普通的负载均衡场景，那么通常只需要 @LoadBalanced 注解就能完成客户端负载均衡。而如果我们要对 Ribbon 运行时行为进行定制化处理时，就可以使用 @RibbonClient 注解。