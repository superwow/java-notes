
# feign 工作原理

1. @FeignClient 接口

1. Spring 为该注解的类生成代理对象

1. RequestTemplate 根据参数构造request 对象

1. 将请求进行编码

1. 拦截器打印日志

1. 基于负载均衡、重试器发出请求

1. 获取相应，进行解码

1. 返回response

# feign 负载均衡原理

feign 内部集成了 ribbon，当feign 向某个服务发出请求时，通过 ribbon 获取服务地址，ribbon 会通过 负载均衡算法得出相应的地址