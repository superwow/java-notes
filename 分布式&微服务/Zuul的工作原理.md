
# ZuulFilter

zuul 响应http请求的过程是典型的过滤器结构，内部使用 ZuulFilter 来实现这一机制。

Filter 的顶层接口定义如下：
```java
public interface IZuulFilter {

    //是否执行 过滤器，返回true则执行
    boolean shouldFilter();

    //执行过滤器
    Object run() throws ZuulException;
}
```

## ZuulFilter抽象类

IZuulFilter 的直接实现类是 ZuulFilter。ZuulFilter是一个抽象类，提供了两个抽象方法：
```java
//过滤器类型
abstract public String filterType();

//过滤器顺序	
abstract public int filterOrder();
```
- filterType()- 过滤器的类型，内置过滤器分为 
    - PRE：可以在请求被路由之前调用
    - ROUTING：在路由请求时候被调用
    - POST：在route和error过滤器之后被调用
    - ERROR：处理请求时发生错误时被调用

- filterOrder：过滤器的执行顺序，数字越小越先执行


然后在 ZuulFilter 类中最核心的就是负责执行 Filter 的 runFilter() 方法，该方法的执行流程并不复杂，如下所示：
```java
public ZuulFilterResult runFilter() {
    ZuulFilterResult zr = new ZuulFilterResult();
    //过滤器是否禁用
    if (!isFilterDisabled()) {
        //是否执行过滤器
        if (shouldFilter()) {
            Tracer t = TracerFactory.instance().startMicroTracer("ZUUL::" + this.getClass().getSimpleName());
            try {
                Object res = run();
                zr = new ZuulFilterResult(res, ExecutionStatus.SUCCESS);
            } catch (Throwable e) {
                t.setName("ZUUL::" + this.getClass().getSimpleName() + " failed");
                zr = new ZuulFilterResult(ExecutionStatus.FAILED);
                zr.setException(e);
            } finally {
                t.stopAndLog();
            }
        } else {
            zr = new ZuulFilterResult(ExecutionStatus.SKIPPED);
        }
    }
    return zr;
}
```

# FilterRegistry 

我们有了过滤器，系统中就应该有一个管理过滤器的组件，我们称这个组件为过滤器链。

在ZUUL中使用了一个叫做过滤器注册表 FilterRegistry  的组件来保存和维护所有的 zuulFilter。

ZuulFilter 定义如下：
```java
public class FilterRegistry {

    
    private static final FilterRegistry INSTANCE = new FilterRegistry();

    public static final FilterRegistry instance() {
        return INSTANCE;
    }

    //使用ConcurrentHashMap 来保存 Filter
    private final ConcurrentHashMap<String, ZuulFilter> filters = new ConcurrentHashMap<String, ZuulFilter>();

    private FilterRegistry() {
    }

    public ZuulFilter remove(String key) {
        return this.filters.remove(key);
    }

    public ZuulFilter get(String key) {
        return this.filters.get(key);
    }

    public void put(String key, ZuulFilter filter) {
        this.filters.putIfAbsent(key, filter);
    }

    public int size() {
        return this.filters.size();
    }

    public Collection<ZuulFilter> getAllFilters() {
        return this.filters.values();
    }

}
```
# ZuulFilter 的加载

在Zuul 中使用 FilterLoader 来加载 ZuulFilter。它用来编译、载入、和校验过滤器。

我们首先来看一下它的成员变量：
```java
//Filter 文件名与修改时间的映射
private final ConcurrentHashMap<String, Long> filterClassLastModified = new ConcurrentHashMap<String, Long>();

//Filter 名称与代码的映射
private final ConcurrentHashMap<String, String> filterClassCode = new ConcurrentHashMap<String, String>();

//Filter 名称与名称的映射，作用是判断该 Filter 是否存在
private final ConcurrentHashMap<String, String> filterCheck = new ConcurrentHashMap<String, String>();

//Filter 类型与 List<ZuulFilter> 的映射
private final ConcurrentHashMap<String, List<ZuulFilter>> hashFiltersByType = new ConcurrentHashMap<String, List<ZuulFilter>>();

//前面提到的 FilterRegistry 单例
private FilterRegistry filterRegistry = FilterRegistry.instance();

//重点： 动态代码编译器实例，Zuul 提供的默认实现是 GroovyCompiler
static DynamicCodeCompiler COMPILER;

//ZuulFilter 工厂类
static FilterFactory FILTER_FACTORY = new DefaultFilterFactory();
```
这些变量中唯一值得注意的就是 DynamicCodeCompiler。顾名思义，它一种动态代码编译器。

DynamicCodeCompiler 接口定义如下：
```java
public class GroovyCompiler implements DynamicCodeCompiler {

    //构建类加载器
    //然后调用它的 parseClass 方法即可将一个 Groovy 文件加载为一个 Java 类。
    GroovyClassLoader getGroovyClassLoader() {
        return new GroovyClassLoader();
    }

    //
    @Override
    public Class compile(String sCode, String sName) {
        GroovyClassLoader loader = getGroovyClassLoader();
        Class groovyClass = loader.parseClass(sCode, sName);
        return groovyClass;
    }

    @Override
    public Class compile(File file) throws IOException {
        GroovyClassLoader loader = getGroovyClassLoader();
        Class groovyClass = loader.parseClass(file);
        return groovyClass;
    }
}
```

有了 GroovyCompile 提供的动态加载和编译代码的能力，我们再回到 FilterLoader。FilterLoader 分别提供了 getFilter、putFilter 和 getFiltersByType 这三个工具方法，其中实际涉及加载和存储 Filter 的方法只有 putFilter，该方法如下所示：

```java
public boolean putFilter(File file) throws Exception {
    String sName = file.getAbsolutePath() + file.getName();
    //判断文件是否被修改过
    if (filterClassLastModified.get(sName) != null && (file.lastModified() != filterClassLastModified.get(sName))) {
        //被修改过则移除该Filter
        LOG.debug("reloading filter " + sName);
        filterRegistry.remove(sName);
    }
    //FilterRegistry是否缓存了该Filter
    ZuulFilter filter = filterRegistry.get(sName);
    if (filter == null) {
        //加载 filter 为 Class
        Class clazz = COMPILER.compile(file);
        if (!Modifier.isAbstract(clazz.getModifiers())) {
            //实例化该Filter ->  反射创建
            filter = (ZuulFilter) FILTER_FACTORY.newInstance(clazz);
            List<ZuulFilter> list = hashFiltersByType.get(filter.filterType());
            if (list != null) {
                hashFiltersByType.remove(filter.filterType()); //rebuild this list
            }
            filterRegistry.put(file.getAbsolutePath() + file.getName(), filter);
            filterClassLastModified.put(sName, file.lastModified());
            return true;
        }
    }

    return false;
}
```
1. 首先根据修改时间判断文件是否被修改，如果被修改则从 FilterRegistry 移除该Filter

1. 然后根据 FilterRegistry 是否存在缓存，来加载 filter 并实例化。

对于 putFilter 方法我们已经知道是如何执行的，但是参数 file 对象时从和而来的?

实际上 file 对象是由 FilterFileManager  类提供的。顾名思义，这个类的作用是管理这些 File 对象。FilterFileManager 中包含了如下所示的 manageFiles 和 processGroovyFiles 方法，后者调用了 FilterLoader 的 putFilter 方法以进行 Groovy 文件的处理。
```java
void manageFiles() throws Exception, IllegalAccessException, InstantiationException {

    List<File> aFiles = getFiles();
    processGroovyFiles(aFiles);
}

void processGroovyFiles(List<File> aFiles) throws Exception, InstantiationException, IllegalAccessException {

    for (File file : aFiles) {
        FilterLoader.getInstance().putFilter(file);
    }
}
```
那么这个 manageFiles 方法是什么时候被调用的呢？注意到在 FilterFileManager 中存在一个每隔 pollingIntervalSeconds 秒就会轮询一次的后台守护线程，该线程就会调用 manageFiles 方法来重新加载 Filter，如下所示：
```java
void startPoller() {

    poller = new Thread("GroovyFilterFileManagerPoller") {
        public void run() {
            while (bRunning) {
                try {
                    //每隔 pollingIntervalSeconds 秒就调用 manager Filers重新加载Filter
                    sleep(pollingIntervalSeconds * 1000);
                    manageFiles();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };
    poller.setDaemon(true);
    poller.start();
}
```
分析到这里，我们对 ZuulFilter 的加载和管理流程已经非常清晰了。Zuul 通过文件来存储各种 ZuulFilter 的定义和实现逻辑，然后启动一个守护线程定时轮询这些文件，确保变更之后的文件能够动态加载到 FilterRegistry 中。

# RequestContext 与上下文

使用过 Zuul 的同学都知道其中的 RequestContext 对象。我们可以通过该对象将业务信息放到请求上下文（Context）中，并使其在各个 ZuulFilter 中进行传递。上下文机制是众多开源软件中所必备的一种需求和实现方案。

在网络环境中 每个请求都由一个独立的线程进行处理，也就是说 HTTP 的参数总是绑定在线程中的。那么 RequestContext 必须是线程安全的 ZUUL 使用了 ThreadLocal 来保证线程安全，如下所示：
```java
protected static final ThreadLocal<? extends RequestContext> threadLocal = new ThreadLocal<RequestContext>() {

    //这里重写了 ThreadLocal 的 initialValue() 方法。重写目的是确保ThreadLocal 的 get() 方法始终会获取一个 RequestContext 实例。这样做的原因是因为默认情况下 ThreadLocal 的 get() 方法可能会返回 null，而通过重写 initialValue() 方法可以在返回 null 时自动初始化一个 RequestContext 实例。
    @Override
    protected RequestContext initialValue() {
        try {
            return contextClass.newInstance();
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }
};
```

用于获取 RequestContext 的 getCurrentContext 方法如下所示：

```java
public static RequestContext getCurrentContext() {
    RequestContext context = threadLocal.get();
    return context;
}
```

我们在开发业务代码时可以先获取这个 RequestContext 对象，进而获取该对象中所存储的各种业务数据。RequestContext 为我们提供了大量的 getter/setter 方法对来完成这一操作。与 Zuul 中的其他组件一样，通过分析代码，我们发现 RequestContext 的实现也是直接对 ConcurrentHashMap 做了一层封装，其中 Key 是 String 类型，而 Value 是 Object 类型，因此我们可以将任何想要传递的数据放到 RequestContext 中。

# HTTP 请求与过滤器执行

接下去我们分析如何使用加载好的 ZuulFilter。Zuul 提供了 FilterProcessor 类来执行 Filter，参考如下所示的 runFilters 方法：
```java
public Object runFilters(String sType) throws Throwable {
    boolean bResult = false;

    // 1. 根据传入的filter类型，通过filterLoader获取对应的filter列表
    List<ZuulFilter> list = FilterLoader.getInstance().getFiltersByType(sType);

    if (list != null) {
        // 2. 遍历filter列表
        for (int i = 0; i < list.size(); i++) {
            ZuulFilter zuulFilter = list.get(i);
            // 3. 调用每个filter 的runFilter方法，获取执行结果
            Object result = processZuulFilter(zuulFilter);
            if (result != null && result instanceof Boolean) {
                bResult |= ((Boolean) result);
            }
        }
    }
    return bResult;
}
```

我们同时看到 FilterProcessor 基于 runFilters 方法衍生出 postRoute()、preRoute()、error() 和 route() 这四个方法，分别对应 Zuul 中的四种过滤器类型。其中 PRE 过滤器在请求到达目标服务器之前调用，ROUTING 过滤器把请求发送给目标服务，POST 过滤器在请求从目标服务返回之后执行，而 ERROR 过滤器则在发生错误时执行。同时，这四种过滤器有一定的执行顺序，如下所示：

![20210304102623](http://qiniu.liulei.life/20210304102623.png)

在上图中，可以看到每一种过滤器对应了一个 HTTP 请求的执行生命周期。因此，我们可以在不同的过滤器中添加针对该生命周期的特定执行逻辑。例如，在日常开发过程中，常见的做法是在 PRE 过滤器中执行身份验证和记录请求日志等操作；而在 POST 过滤器返回的响应中添加消息头或者各种统计信息等。


ZuulServlet 生命周期。Spring cloud 中所有请求都是 HTTP 请求，Zuul 作为一个服务网关同样也需要完成对 HTTP 请求的响应。ZuulServlet 是 Zuul 中对 HttpServlet 接口的一个实现类。我们知道 Servlet 中最核心的就是 service 方法，ZuulServlet 的 service 方法如下所示：
```java
 public void service(javax.servlet.ServletRequest servletRequest, javax.servlet.ServletResponse servletResponse) throws ServletException, IOException {
    try {
        init((HttpServletRequest) servletRequest, (HttpServletResponse) servletResponse);

        // Marks this request as having passed through the "Zuul engine", as opposed to servlets
        // explicitly bound in web.xml, for which requests will not have the same data attached
        RequestContext context = RequestContext.getCurrentContext();
        context.setZuulEngineRan();

        try {
            preRoute();
        } catch (ZuulException e) {
            error(e);
            postRoute();
            return;
        }
        try {
            route();
        } catch (ZuulException e) {
            error(e);
            postRoute();
            return;
        }
        try {
            postRoute();
        } catch (ZuulException e) {
            error(e);
            return;
        }

    } catch (Throwable e) {
        error(new ZuulException(e, 500, "UNHANDLED_EXCEPTION_" + e.getClass().getName()));
    } finally {
        RequestContext.getCurrentContext().unset();
    }
}
```

注意这里的 postRoute()、preRoute()、error() 和 route() 这四个方法同样只是对 ZuulRunner 中同名方法的直接封装。你可以尝试通过这段代码梳理 Zuul 中四种过滤器的执行顺序。

# Spring Cloud 集成 ZuulFilter

针对 Zuul 的整体架构，在设计上的初衷是把所有 ZuulFilter 通过 Groovy 文件的形式进行管理和加载。但是，很遗憾，在 Spring Cloud 体系中并没有使用此策略来加载 ZuulFilter。那么 Spring Cloud 的 ZuulFilter是如何实现加载的呢？

spring-cloud-netflix-zuul 同样是一个 Spring Boot 应用程序，所以我们还是直接进入 ZuulServerAutoConfiguration 类，并找到了如下所示的 ZuulFilterConfiguration 类：
```java
@Configuration(proxyBeanMethods = false)
protected static class ZuulFilterConfiguration {

    @Autowired
    private Map<String, ZuulFilter> filters;

    @Bean
    public ZuulFilterInitializer zuulFilterInitializer(CounterFactory counterFactory,
            TracerFactory tracerFactory) {
        FilterLoader filterLoader = FilterLoader.getInstance();
        FilterRegistry filterRegistry = FilterRegistry.instance();
        return new ZuulFilterInitializer(this.filters, counterFactory, tracerFactory,
                filterLoader, filterRegistry);
    }
}
```

这里我们看到了熟悉的 FilterLoader 和 FilterRegistry，通过 ZuulFilterInitializer 的构造函数引入了过滤器加载的流程。同时，我们发现 filters 变量是一个 Key 为 String、Value 为 ZuulFilter 的 Map，而该变量上添加了 @Autowired 注解。这就意味着 Spring 会把 ZuulFilter 的 bean 自动装载到 Map 对象中。

我们在 ZuulServerAutoConfiguration 以及它的子类 ZuulProxyAutoConfiguration 中发现了很多添加了 @Bean 注解并以“Filter”结尾的类，其形式如下所示：

```java
@Bean
public ServletDetectionFilter servletDetectionFilter() {
    return new ServletDetectionFilter();
}
```
这个 ServletDetectionFilter 类就是 ZuulFilter 的子类，在 Spring 容器中会被自动注入ZuulFilterConfiguration 的 filters 对象中。

获取了容器中自动注入的 ZuulFilter 之后，我们继续跟进 ZuulFilterInitializer，发现了如下所示的 contextInitialized 方法：
```java
@PostConstruct
public void contextInitialized() {
    log.info("Starting filter initializer");
    TracerFactory.initialize(tracerFactory);
    CounterFactory.initialize(counterFactory);
    for (Map.Entry<String, ZuulFilter> entry : this.filters.entrySet()) {
        filterRegistry.put(entry.getKey(), entry.getValue());
    }
}
```
注意到该方法开启了 @PostConstruct 注解，因此在 Spring 容器中，一旦 ZuulFilterInitializer 的构造函数执行完成，就会执行这个 contextInitialized 方法。而在该方法中，我们通过 FilterRegistry 的 put 方法把从容器中自动注入的各个 Filter 添加到 FilterRegistry 中。同样，在 ZuulFilterInitializer 也存在一个被添加了 @PostConstruct 注解的 contextDestroyed 方法，该方法中调用了 filterRegistry 的 remove 方法删除 Filter。

至此，Spring Cloud 通过 Netflix Zuul 提供的 FilterLoader 和 FilterRegistry 等工具类完成了两者之间的集成。