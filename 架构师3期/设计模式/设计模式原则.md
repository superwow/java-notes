



# 开闭原则

开闭原则（Open-Closed Principle, OCP），即对扩展开放，对修改关闭。

我们在程序需要扩展时，不能修改原有的代码，而是通过增加类的方式完成行为的扩展。为了达到这样的效果我们通常使用**接口**和**抽象类**进行实现。

# 里氏代换原则

里氏代换原则（Liskov Substitution Principle），即任何基类出现的地方，子类也一定可以出现。

当派生类替换掉基类时，且软件的 功能不受到影响，基类才能被复用。另外派生类会为基类提供新的行为。

里氏代换原则是对开闭原则的一个补充。而实现开闭原则的方式就是抽象化，基类与子类就是抽象化的具体实现，而里氏替换原则就是对抽象化的具体规范。



# 依赖倒转原则

依赖倒转原则（Dependence Inversion Principle）是开闭原则的基础，具体是指：针对接口编程，面对抽象而不是具体。


# 单一职责

单一职责（Simple Responsibility Pinciple，SRP），一个类只负责一个功能领域中的相应职责，或者可以定义为：就一个类而言，应该只有一个引起它变化的原因。

通俗一点：

1. 就是一个类越复杂，可能被复用的可能性越小
2. 类越复杂，变动代码可能会有意想不到的错误。


# 接口隔离原则

接口隔离原则（Interface Segregation Principle）即使用多个相互独立的接口比使用单个接口要好。这样可以降低类之间的耦合度。


# 合成复用原则

合成复用原则（Composite Reuse Principle），即尽量使用合成/聚合的方式，而不是使用继承。

# 总结
<https://gitmind.cn/app/doc/0a9665771>