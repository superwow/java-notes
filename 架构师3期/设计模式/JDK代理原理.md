# JDK代理场景

1. 由们自己定义并调用的增强类的方式
    1. 日志
    1. 请求转发、过滤
    1. 事务。。。

1. 注解也是由代理进行实现的



# JDK代理使用

首先我们先看一下JDK代理是如何使用的

## 接口
```java
public interface Controller {
    void request();
}
```

## 接口实现
```java
public class RequestController implements Controller {
    @Override
    public void request() {
        System.out.println("去查询数据库了");
    }
}
```

## 代理类实现
```java
public class JDKRequestProxy implements InvocationHandler {

    /**
     * 被代理对象
     */
    private Object target;

    /**
     * 获取代理实例
     *
     * @param target
     * @return
     */
    public Object getInstance(Object target) {
        this.target = target;
        Class<?> clazz = target.getClass();
        return Proxy.newProxyInstance(
                clazz.getClassLoader(),
                clazz.getInterfaces(),
                this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args)
        throws Throwable {

        System.out.println("request执行开始");
        Object result = method.invoke(this.target, args);
        System.out.println("request执行结束");
        return result;
    }
}
```
注意：

1. 该类实现了 `InvocationHandler` 接口

1. 我们在构造函数中传入了被代理对象并且通过 `InvocationHandler` 的 `invoke` 方法 反射执行了接口的方法


## 执行
```java
public static void main(String[] args) {
    Controller controller = (Controller) new JDKRequestProxy()
        .getInstance(new RequestController());
    System.out.println(controller.getClass());
    controller.request();
}
```
输出
```java
class com.sun.proxy.$Proxy0
request执行开始
去查询数据库了
request执行结束
```
## 代码解析

1. 我们在 `main` 方法中调用的对象是由 `getInstance` 方法返回的代理类

    ```java
    public Object getInstance(Object target) {
        //传入被代理对象
        this.target = target;
        //获取代理对象 元类
        Class<?> clazz = target.getClass();
        //生成代理实例
        return Proxy.newProxyInstance(
                clazz.getClassLoader(),
                clazz.getInterfaces(),
                this);
    }
    ```
1. 在上面我们说 `getInstance` 方法是返回的代理对象, 我们可以通过下面的代码进行验证
    ```java
    Controller controller = (Controller) new JDKRequestProxy()
        .getInstance(new RequestController());
    System.out.println(controller.getClass());
    ```
    输出
    ```java
    class com.sun.proxy.$Proxy0
    ```
    至此我们可以确定，JDK为我们生成了一个代理类，并且可以强制转为 `Controller` 接口类型，这说明代理类实现了  `Controller` 接口

1. 随后我们执行 `request` 方法
    ```java
    controller.request();
    ```

    输出
    ```java
    request执行开始
    去查询数据库了
    request执行结束
    ```
    我们可以看到，执行的其实是， 我们定义的代理类的 `invoke` 方法
    ```java
    @Override
    public Object invoke(Object proxy, Method method, Object[] args)
        throws Throwable {
        System.out.println("request执行开始");
        Object result = method.invoke(this.target, args);
        System.out.println("request执行结束");
        return result;
    }
    ```
    至此我们可以得出结论，当我们使用JDK代理时，JVM会帮助我们生成一个行为代理类。在我们调用自己定义的方法时，会完成代理过程，也就是调用了 `invoke` 方法。接下来我们去看一下JDK代理的原理

# JDK代理原理 

我们可以使用JVM参数 
> -Djdk.proxy.ProxyGenerator.saveGeneratedFiles=true

就可以在`main` 函数运行时，生成代理类的class，可以通过idea查看


<img src="../../../java-notes/img/jvm-paramproxy.png">


## 生成代理类

我们以上面的例子生成代理类

```java
package com.sun.proxy;

import demo.pattern.proxy.Controller;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.lang.reflect.UndeclaredThrowableException;

public final class $Proxy0 extends Proxy implements Controller {
    private static Method m1;
    private static Method m2;
    private static Method m3;
    private static Method m0;

    public $Proxy0(InvocationHandler var1) throws  {
        super(var1);
    }

    public final boolean equals(Object var1) throws  {
        try {
            return (Boolean)super.h.invoke(this, m1, new Object[]{var1});
        } catch (RuntimeException | Error var3) {
            throw var3;
        } catch (Throwable var4) {
            throw new UndeclaredThrowableException(var4);
        }
    }

    public final String toString() throws  {
        try {
            return (String)super.h.invoke(this, m2, (Object[])null);
        } catch (RuntimeException | Error var2) {
            throw var2;
        } catch (Throwable var3) {
            throw new UndeclaredThrowableException(var3);
        }
    }

    public final void request() throws  {
        try {
            super.h.invoke(this, m3, (Object[])null);
        } catch (RuntimeException | Error var2) {
            throw var2;
        } catch (Throwable var3) {
            throw new UndeclaredThrowableException(var3);
        }
    }

    public final int hashCode() throws  {
        try {
            return (Integer)super.h.invoke(this, m0, (Object[])null);
        } catch (RuntimeException | Error var2) {
            throw var2;
        } catch (Throwable var3) {
            throw new UndeclaredThrowableException(var3);
        }
    }

    static {
        try {
            m1 = Class.forName("java.lang.Object").getMethod("equals", Class.forName("java.lang.Object"));
            m2 = Class.forName("java.lang.Object").getMethod("toString");
            m3 = Class.forName("demo.pattern.proxy.Controller").getMethod("request");
            m0 = Class.forName("java.lang.Object").getMethod("hashCode");
        } catch (NoSuchMethodException var2) {
            throw new NoSuchMethodError(var2.getMessage());
        } catch (ClassNotFoundException var3) {
            throw new NoClassDefFoundError(var3.getMessage());
        }
    }
}
```

## 类定义
首先我们来看它的类定义

```java
public final class $Proxy0 extends Proxy implements Controller
```
1. 该类使用了 `final` 修饰，表明了该代理类不可被继承

1. 该类继承了 `Proxy` 类

1. 该类实现了我们自己定义的 `Controller` 接口

1. 该类在构造函数中注入了一个 `InvocationHandler` 类型的成员变量，并且调用了父类构造
    `super(var1);`


接下来我们看一下第二项 `Proxy` 接口

## Proxy

`Proxy` 是一个用来创建代理类的工具类。

我们接下来主要分析**构造方法**和**创建代理类**的方法


**Proxy的构造方法**

```java
protected Proxy(InvocationHandler h) {
    Objects.requireNonNull(h);
    this.h = h;
}
```
我们可以看到构造方法中将 `InvocationHandler` 类型的变量设置为成员变量。

由于生成的代理类继承了 `Proxy` 类，并且在构造函数中调用了该方法，如下
```java
public $Proxy0(InvocationHandler var1) throws  {
    super(var1);
}
```
而 `h` 变量就是我们自己的代理类的实例的引用 `JDKRequestProxy`。

所以我们就是将自己的代理类的引用赋值到生成的代理类的成员变量`h`。


**创建代理类**

首先看一下我们的调用方式
```java
public Object getInstance(Object target) {
    this.target = target;
    Class<?> clazz = target.getClass();
    return Proxy.newProxyInstance(clazz.getClassLoader(), clazz.getInterfaces(), this);
}
```
我们调用了 `Proxy` 的 `newProxyInstance` 方法，传入了`Classloader` 和对象的接口，以及我们自己的代理类。

接下来我们来看下 `newProxyInstance` 方法是如何生成代理对象的


```java
@CallerSensitive
public static Object newProxyInstance(ClassLoader loader,
                                        Class<?>[] interfaces,
                                        InvocationHandler h) {
    Objects.requireNonNull(h);

    final Class<?> caller = System.getSecurityManager() == null
                                ? null
                                : Reflection.getCallerClass();

    /*
        * Look up or generate the designated proxy class and its constructor.
        */
    Constructor<?> cons = getProxyConstructor(caller, loader, interfaces);

    return newProxyInstance(caller, cons, h);
}
```



1. 首先进行非空判断

1. 获取 安全管理器

1. 创建代理对象，并获取构造函数

1. 实例化代理对象，将我们自己的代理类的引用传入


## JDK原理总结

1. 创建一个带有接口的类，称之为被代理类

1. 创建一个代理类实现 `InvocationHandler` 接口，

   1.  重写 `invoke` 方法，并编写代理的逻辑，并调用被代理方法（反射调用）

   1. 调用 `Proxy.newProxyInstance()` 传入类加载器、接口数组、代理类的引用

1. 接下来 `Proxy.newProxyInstance()` ，会重新生成一个类，这个新类会实现所有的接口，并继承 `Proxy`.

1. 将新生成的类便以为 `Class` 字节码，并加载到JVM中运行，随后返回给调用者

1. 最后我们将返回的代理对象，强制转换为我们的接口类型，此时我们调用方法即可，这样就会执行我们的增强方法

## 缺点

1. 我们要增强的类必须继承接口

1. 使用反射调用方法，效率没有那么高



# 手写JDKProxy

我们接下来按照上面的方式来手写一个JDK代理工具

## 思路

1. 创建接口，并让被代理类实现该接口

1. 创建自己的 `InvocationHandler` 接口

1. 手写代理工具类，实现以下功能

    1. 动态生成 新代理类的 .java 文件，继承原有接口

    1. 将.java编译为 .class 文件

    1. 将 .class 文件加载到JVM当中

    1. 返回代理类

1. 创建代理类，并实现自己的 `InvocationHandler` 接口,调用工具类的实例化代理类的方法，获得代理类，并进行方法调用


## 实现

理解思路后我们对其进行实现

### 创建接口

接口

```java
public interface Controller {
    void request();
}
```
被代理类
```java
public class RequestController implements Controller {
    @Override
    public void request() {
        System.out.println("去查询数据库了");
    }
}
```
### 创建自己的InvocationHandler接口
自己的 `InvocationHandler` 接口
```java
public interface MyInvocationHandler {
    Object invoke(Object proxy, Method method, Object[] args)
            throws Throwable;
}
```

### 实现代理工具类

```java

public class MyProxy {

    public static final String ln = "\r\n";

    public static Object newProxyInstance(MyClassLoader classLoader,
                                          Class<?>[] interfaces,
                                          MyInvocationHandler h) {
        try {
            //1、动态生成源代码.java文件
            String src = generateSrc(interfaces);

//           System.out.println(src);
            //2、Java文件输出磁盘
            String filePath = MyProxy.class.getResource("").getPath();

            File f = new File(filePath + "$Proxy0.java");

            FileWriter fw = new FileWriter(f);
            fw.write(src);
            fw.flush();
            fw.close();

            //3、把生成的.java文件编译成.class文件
            JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
            //标准Java文件管理器
            StandardJavaFileManager manage = compiler.getStandardFileManager(
                    null,
                    null,
                    null);
            //从磁盘获取Java文件
            Iterable iterable = manage.getJavaFileObjects(f);

            JavaCompiler.CompilationTask task = compiler.getTask(null, manage,
                    null,
                    null,
                    null,
                    iterable);
            task.call();
            manage.close();

            //4、编译生成的.class文件加载到JVM中来
            Class proxyClass = classLoader.findClass("$Proxy0");

            Constructor c = proxyClass.getConstructor(MyInvocationHandler.class);

            f.delete();

            //5、返回字节码重组以后的新的代理对象
            return c.newInstance(h);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String generateSrc(Class<?>[] interfaces) {
        StringBuffer sb = new StringBuffer();

        //包名
        sb.append(MyProxy.class.getPackage() + ";" + ln);

        //导入接口 - 这里为了简化我们只导入首个接口
        sb.append("import " + interfaces[0].getName() + ";" + ln);

        //导入反射
        sb.append("import java.lang.reflect.*;" + ln);

        //设置新类继承我们的几口
        sb.append("public class $Proxy0 implements " + interfaces[0].getSimpleName() + "{" + ln);

        //我们自己的成员
        sb.append("    " + MyInvocationHandler.class.getSimpleName() + " h;" + ln);
        sb.append("    " + "public $Proxy0(" + MyInvocationHandler.class.getSimpleName() + " h) { " + ln);
        sb.append("        " + "this.h = h;" + ln);
        sb.append("    }" + ln);
        for (Method m : interfaces[0].getMethods()) {
            Class<?>[] params = m.getParameterTypes();

            StringBuffer paramNames = new StringBuffer();
            StringBuffer paramValues = new StringBuffer();
            StringBuffer paramClasses = new StringBuffer();

            for (int i = 0; i < params.length; i++) {
                Class clazz = params[i];
                String type = clazz.getName();
                String paramName = toLowerFirstCase(clazz.getSimpleName());
                paramNames.append(type + " " + paramName);
                paramValues.append(paramName);
                paramClasses.append(clazz.getName() + ".class");
                if (i > 0 && i < params.length - 1) {
                    paramNames.append(",");
                    paramClasses.append(",");
                    paramValues.append(",");
                }
            }

            sb.append("    public " + m.getReturnType().getName() + " " + m.getName() + "(" + paramNames.toString() + ") {" + ln);
            sb.append("        try{" + ln);
            sb.append("            Method m = " + interfaces[0].getName() + ".class.getMethod(\"" + m.getName() + "\",new Class[]{" + paramClasses.toString() + "});" + ln);
            sb.append("            " + (hasReturnValue(m.getReturnType()) ? "return " : "") + getCaseCode("this.h.invoke(this,m,new Object[]{" + paramValues + "})", m.getReturnType()) + ";" + ln);
            sb.append("        }catch(Error _ex) { }" + ln);
            sb.append("        catch(Throwable e){" + ln);
            sb.append("            throw new UndeclaredThrowableException(e);" + ln);
            sb.append("        }" + ln);
            sb.append(getReturnEmptyCode(m.getReturnType()));
            sb.append("    }" + ln);
        }
        sb.append("}" + ln);
        System.out.println("------------------------生成类开始--------------------------");
        System.out.println(sb);
        System.out.println("------------------------生成类结束--------------------------");
        return sb.toString();
    }


    private static Map<Class, Class> mappings = new HashMap<Class, Class>();

    static {
        mappings.put(int.class, Integer.class);
    }

    private static String getReturnEmptyCode(Class<?> returnClass) {
        if (mappings.containsKey(returnClass)) {
            return "return 0;";
        } else if (returnClass == void.class) {
            return "";
        } else {
            return "return null;";
        }
    }

    private static String getCaseCode(String code, Class<?> returnClass) {
        if (mappings.containsKey(returnClass)) {
            return "((" + mappings.get(returnClass).getName() + ")" + code + ")." + returnClass.getSimpleName() + "Value()";
        }
        return code;
    }

    private static boolean hasReturnValue(Class<?> clazz) {
        return clazz != void.class;
    }

    private static String toLowerFirstCase(String src) {
        char[] chars = src.toCharArray();
        chars[0] += 32;
        return String.valueOf(chars);
    }

}
```
上面的代码最重要的是 创建代理实力的方法
```java
public static Object newProxyInstance(MyClassLoader classLoader,
                                        Class<?>[] interfaces,
                                        MyInvocationHandler h) {
    try {
        //1、动态生成源代码.java文件
        String src = generateSrc(interfaces);

        //2、Java文件输出磁盘
        String filePath = MyProxy.class.getResource("").getPath();

        File f = new File(filePath + "$Proxy0.java");

        FileWriter fw = new FileWriter(f);
        fw.write(src);
        fw.flush();
        fw.close();

        //3、把生成的.java文件编译成.class文件
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        //标准Java文件管理器
        StandardJavaFileManager manage = compiler.getStandardFileManager(
                null,
                null,
                null);
        //从磁盘获取Java文件
        Iterable iterable = manage.getJavaFileObjects(f);

        JavaCompiler.CompilationTask task = compiler.getTask(null, manage,
                null,
                null,
                null,
                iterable);
        task.call();
        manage.close();

        //4、编译生成的.class文件加载到JVM中来
        Class proxyClass = classLoader.findClass("$Proxy0");

        Constructor c = proxyClass.getConstructor(MyInvocationHandler.class);

        f.delete();

        //5、返回字节码重组以后的新的代理对象
        return c.newInstance(h);
    } catch (Exception e) {
        e.printStackTrace();
    }
    return null;
}
```
由于我们将java文件和class文件输出到了磁盘所以我们重写了classloader
```java
public class GPClassLoader extends ClassLoader {

    private File classPathFile;
    public GPClassLoader(){
        String classPath = GPClassLoader.class.getResource("").getPath();
        this.classPathFile = new File(classPath);
    }

    //从磁盘寻找并加载Class文件
    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {

        String className = GPClassLoader.class.getPackage().getName() + "." + name;
        if(classPathFile  != null){
            File classFile = new File(classPathFile,name.replaceAll("\\.","/") + ".class");
            if(classFile.exists()){
                FileInputStream in = null;
                ByteArrayOutputStream out = null;
                try{
                    in = new FileInputStream(classFile);
                    out = new ByteArrayOutputStream();
                    byte [] buff = new byte[1024];
                    int len;
                    while ((len = in.read(buff)) != -1){
                        out.write(buff,0,len);
                    }
                    return defineClass(className,out.toByteArray(),0,out.size());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
```
### 实现自定义的代理类

```java
public class RequestProxy implements MyInvocationHandler {

    public RequestProxy() {
    }

    private Object target;
    public Object getInstance(Object target) throws Exception{
        this.target = target;
        Class<?> clazz = target.getClass();
        return MyProxy.newProxyInstance(new MyClassLoader(),clazz.getInterfaces(),this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        before();
        Object obj = method.invoke(this.target,args);
        after();
        return obj;
    }

    private void before(){
        System.out.println("前置增强");
    }

    private void after(){
        System.out.println("后置增强");
    }
}
```

### 调用执行
```java
public static void main(String[] args) throws Exception {
        Controller obj = (Controller) new RequestProxy().getInstance(new RequestController());
        System.out.println(obj.getClass());
        obj.request();
}
```
输出
```
class demo.pattern.proxy.myProxy.$Proxy0
前置增强
去查询数据库了
后置增强
```
我们可以看到输出的class是生成的代理类的，而我们调用的方法也是增强了的