
# 概念
Servlet（Server Applet），全称Java Servlet，未有中文译文。是用Java编写的服务器端程序。其主要功能在于交互式地浏览和修改数据，生成动态Web内容。狭义的Servlet是指Java语言实现的一个接口，广义的Servlet是指任何实现了这个Servlet接口的类，一般情况下，人们将Servlet理解为后者。

Servlet运行于支持Java的应用服务器中。从实现上讲，Servlet可以响应任何类型的请求，但绝大多数情况下Servlet只用来扩展基于HTTP协议的Web服务器。

 
# Servlet使用

我们这里使用注解版的Servlet

```java
@WebServlet("/")
@Slf4j
public class DispatchServlet extends HttpServlet {


    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        log.info("初始化 HelloServlet");
    }

    @Override
    protected void service(HttpServletRequest req,
                           HttpServletResponse resp)
                           throws ServletException, IOException {
        log.info("request path is " + req.getServletPath());
        log.info("request method is " + req.getMethod());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
        throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
        throws ServletException, IOException {
        super.doPost(req, resp);
    }

    @Override
    public void destroy() {
        super.destroy();
    }
}
```

# Servlet的生命周期

- 实例化 - Servlet容器创建Servlet实例

- 初始化 - 该容器调用init方法

- 服务 - 如果请求Servlet，则容器调用Service方法，在Service方法中会按照请求类型调用不同的方法处理请求，如get - doGet，post - doPost。。。。

- 销毁 - 销毁实例之前调用destory方法


在 spring 中通过 dispacherServlet 来匹配调用 controller 来处理请求