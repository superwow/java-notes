

# Spring MVC的请求处理流程


![20210223102326](http://qiniu.liulei.life/20210223102326.png)

1. `DispatcherServlet` 是 `Spring MVC` 中的前端控制器，负责接收 `request` ，并将 `request` 转发给对应的处理组件

1. `HandlerMapping` 是 `Spring MVC` 中完成 `url` 到 `Controller` 映射的组件。 `DispatcherServlet` 接收请求后，通过 `HandlerMapping` 查找处理 `request` 的 `controller`.

1. 在确定好处理的 `handler` ->`controller` , 会使用 `HandlerAdapter` 并调用实际的处理 `request` 的方法, 并返回 `ModelAndView` 对象。

1. `ViewResolver`  结合 `Model` 渲染 视图 `view`

1. `DispatcherServlet` 将视图渲染结果，返回给客户端

# SpringMVC的工作机制

在容器初始化时，建立 `url` 和 `Controller` 的对应关系，保存到 `Map(url,controller)` 中. `tomcat` 启动时会通知 `Spring` 初始化容器，随后 `Spring MVC` ，会遍历容器中的 `bean` ，获取每一个 `controller` 中所有方法的 `url`,随后保存起来。

这样就可以根据 `request` 迅速定位到 `controller` ,但是 `Map` 中只保存了 `url` 和 `Controller` 的对应关系，所以还需要确定具体的处理方法 -> `method` 。也就是通过拼接 `url`，来找到具体的处理方法。

确定处理请求的 `method` 后，接下来就是参数绑定，将 `request` 的参数传入 `method`.


# 源码 - todo

# 优化 - todo

<https://blog.csdn.net/win7system/article/details/90674757>



