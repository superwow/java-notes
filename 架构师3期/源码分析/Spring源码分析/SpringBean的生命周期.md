
# bean生命周期概述

我们知道在Spring并不是一启动容器，就开启Bean的实例化，只有当客户端，显式或隐式的调用 `BeanFactory.getBean(。。。)` 方法时，他才会触发Bean的实例化。 当然也可以选择 `ApplicationContext` 容器，该容器启动时，会立即扫描并注册Bean。

`BeanFactory` 的 `getBean` 方法，其实是启动 `bean` 实例化的入口,真正的逻辑在 `AbstractAutowireCapableBeanFactory ` 的 `doCreateBean` 中完成，如下图：

![2a90a57e3bb96cc6ffa2619babe72bc4](http://qiniu.liulei.life/2a90a57e3bb96cc6ffa2619babe72bc4.jpg)

我们原来使用 new 创建对象，当对象在脱离作用域时，则会被回收，对于后续操作我们无法干涉。当我们采用了Spring容器后 Spring会对容器被的Bean 给与一个统一的生命周期的管理，同时我们也可以在这个阶段对Bean进行增强、修改等操作。

# 代码验证

## Bean实例化

在 `doCreateBean`  方法中，首先进行实例化工作，主要由 `createBeanInstance` 方法实现，该方法返回一个 beanWrapper 对象，该对象是Spring的一个Bean基础结构的顶级接口。此时Bean只是被刚刚实例化出来， 基础的属性都没有设置。一般我们是用不到这个类的，而是通过 beanFactory 使用。

BeanWrapper 接口有一个默认实现类 BeanWrapperImpl，其主要作用是对 Bean 进行“包裹”，然后对这个包裹的 bean 进行操作，比如后续注入 bean 属性。

在实例化 bean 过程中，Spring 采用“策略模式”来决定采用哪种方式来实例化 bean，一般有反射和 CGLIB 动态字节码两种方式。

`InstantiationStrategy` 定义了 Bean 实例化策略的抽象接口，其子类 `SimpleInstantiationStrategy` 提供了基于反射来实例化对象的功能，但是不支持方法注入方式的对象实例化。`CglibSubclassingInstantiationStrategy` 继承 `SimpleInstantiationStrategy`，他除了拥有父类以反射实例化对象的功能外，还提供了通过 CGLIB 的动态字节码的功能进而支持方法注入所需的对象实例化需求。默认情况下，Spring 采用 `CglibSubclassingInstantiationStrategy`。

## 激活Aware

当 Spring 完成 bean 对象实例化并且设置完相关属性和依赖后，则会开始 bean 的初始化进程（` #initializeBean(...)` ），初始化第一个阶段是检查当前 bean 对象是否实现了一系列以 Aware 结尾的的接口。

Aware 接口为 Spring 容器的核心接口，是一个具有标识作用的超级接口，实现了该接口的 bean 是具有被 Spring 容器通知的能力，通知的方式是采用回调的方式。

常用的有以下Aware：

- BeanNameAware：对该 bean 对象定义的 beanName 设置到当前对象实例中

- BeanClassLoaderAware：将当前 bean 对象相应的 ClassLoader 注入到当前对象实例中

- BeanFactoryAware：BeanFactory 容器会将自身注入到当前对象实例中，这样当前对象就会拥有一个 BeanFactory 容器的引用。

## BeanPostProcessor

初始化第二个阶段则是 BeanPostProcessor 增强处理，在该阶段 BeanPostProcessor 会处理当前容器内所有符合条件的实例化后的 bean 对象。它主要是对 Spring 容器提供的 bean 实例对象进行有效的扩展，允许 Spring 在初始化 bean 阶段对其进行定制化修改，如处理标记接口或者为其提供代理实现。

分别有前置处理和后置处理两个方法

## InitializingBean 和 init-method

InitializingBean 是一个接口，它为 Spring Bean 的初始化提供了一种方式，它有一个 #afterPropertiesSet() 方法，在 bean 的初始化进程中会判断当前 bean 是否实现了 InitializingBean，如果实现了则调用 #afterPropertiesSet() 方法，进行初始化工作。然后再检查是否也指定了 init-method ，如果指定了则通过反射机制调用指定的 init-method 方法

## DisposableBean 和 destroy-method

与 InitializingBean 和 init-method 用于对象的自定义初始化工作相似，DisposableBean和 destroy-method 则用于对象的自定义销毁工作。

当一个 bean 对象经历了实例化、设置属性、初始化阶段，那么该 bean 对象就可以供容器使用了（调用的过程）。当完成调用后，如果是 singleton 类型的 bean ，则会看当前 bean 是否应实现了 DisposableBean 接口或者配置了 destroy-method 属性，如果是的话，则会为该实例注册一个用于对象销毁的回调方法，便于在这些 singleton 类型的 bean 对象销毁之前执行销毁逻辑。

但是，并不是对象完成调用后就会立刻执行销毁方法，因为这个时候 Spring 容器还处于运行阶段，只有当 Spring 容器关闭的时候才会去调用。但是， Spring 容器不会这么聪明会自动去调用这些销毁方法，而是需要我们主动去告知 Spring 容器。

- 对于 BeanFactory 容器而言，我们需要主动调用 #destroySingletons() 方法，通知 BeanFactory 容器去执行相应的销毁方法。

- 对于 ApplicationContext 容器而言，调用 #registerShutdownHook() 方法。

## 验证
```java
public class LifeCycleBean implements BeanNameAware,BeanFactoryAware,
                                        BeanClassLoaderAware,
                                        BeanPostProcessor,
        InitializingBean,DisposableBean {

    private String test;

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        System.out.println("属性注入....");
        this.test = test;
    }

    public LifeCycleBean(){ // 构造方法
        System.out.println("构造函数调用...");
    }

    public void display(){
        System.out.println("方法调用...");
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("BeanFactoryAware 被调用...");
    }

    @Override
    public void setBeanName(String name) {
        System.out.println("BeanNameAware 被调用...");
    }

    @Override
    public void setBeanClassLoader(ClassLoader classLoader) {
        System.out.println("BeanClassLoaderAware 被调用...");
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) 
        throws BeansException {
        System.out.println("BeanPostProcessor postProcessBeforeInitialization 被调用...");
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) 
        throws BeansException {
        System.out.println("BeanPostProcessor postProcessAfterInitialization 被调用...");
        return bean;
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("DisposableBean destroy 被调动...");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("InitializingBean afterPropertiesSet 被调动...");
    }

    public void initMethod(){
        System.out.println("init-method 被调用...");
    }

    public void destroyMethdo(){
        System.out.println("destroy-method 被调用...");
    }

}
```
LifeCycleBean 继承了 `BeanNameAware` , `BeanFactoryAware` , `BeanClassLoaderAware` , `BeanPostProcessor` , `InitializingBean` , `DisposableBean` 六个接口，同时定义了一个 test 属性用于验证属性注入和提供一个 #display() 方法用于模拟调用。


XML配置

```xml
<bean id="lifeCycle" class="org.springframework.core.test.lifeCycleBean"
        init-method="initMethod" destroy-method="destroyMethdo">
    <property name="test" value="test"/>
</bean>
```
输出
```
构造函数调用...
构造函数调用...
属性注入....
BeanNameAware 被调用...
BeanClassLoaderAware 被调用...
BeanFactoryAware 被调用...
BeanPostProcessor postProcessBeforeInitialization 被调用...
InitializingBean afterPropertiesSet 被调动...
init-method 被调用...
BeanPostProcessor postProcessAfterInitialization 被调用...
方法调用...
方法调用完成，容器开始关闭....
DisposableBean destroy 被调动...
destroy-method 被调用...
```
我们可将上面代码执行过程简述如下

1. Spring 根据实例化策略对bean 进行实例化

1. 如果bean 实现了 `BeanNameAware` 接口，则调用 `setBeanName` 方法

1. 如果 `Bean` 实现了， BeanClassLoaderAware 接口，则 调用 `setBeanClassLoader` 方法

1. 如果该 bean 实现了 `BeanFactoryAware` 接口，则调用 `setBeanFactory`方法

1. 如果该容器注册了 `BeanPostProcessor`，则会调用#`postProcessBeforeInitialization(Object bean, String beanName)` 方法,完成 bean 前置处理

1. 如果该 `bean` 实现了 `InitializingBean` 接口，则调用`afterPropertiesSet` 方法。

1. 如果该 bean 配置了 `init-method` 方法，则调用其指定的方法。

1. 初始化完成后，如果该容器注册了 `BeanPostProcessor` 则会调用 `postProcessAfterInitialization` 方法,完成 bean 的后置处理。

1. 在容器进行关闭之前，如果该 bean 实现了 `DisposableBean` 接口，则调用 `destroy`方法

1. 在容器进行关闭之前，如果该 `bean` 配置了 `destroy-method` ，则调用其指定的方法。

结束
# 通过BeanFactory查看Bean生命周期

我们通过 `IDEA` 来查看源码，主要来看它的注释

我们首先在类的注释上找到这一句话：
```java
 * <p>Bean factory implementations should support the standard bean lifecycle interfaces
 * as far as possible. The full set of initialization methods and their standard order is:
 ```
翻译为：Bean工厂实现应支持标准Bean生命周期接口,全套初始化方法及其标准顺序为：

我们继续向下看
```java
 * <ol>
 * <li>BeanNameAware's {@code setBeanName}
 * <li>BeanClassLoaderAware's {@code setBeanClassLoader}
 * <li>BeanFactoryAware's {@code setBeanFactory}
 * <li>EnvironmentAware's {@code setEnvironment}
 * <li>EmbeddedValueResolverAware's {@code setEmbeddedValueResolver}
 * <li>ResourceLoaderAware's {@code setResourceLoader}
 * (only applicable when running in an application context)
 * <li>ApplicationEventPublisherAware's {@code setApplicationEventPublisher}
 * (only applicable when running in an application context)
 * <li>MessageSourceAware's {@code setMessageSource}
 * (only applicable when running in an application context)
 * <li>ApplicationContextAware's {@code setApplicationContext}
 * (only applicable when running in an application context)
 * <li>ServletContextAware's {@code setServletContext}
 * (only applicable when running in a web application context)
 * <li>{@code postProcessBeforeInitialization} methods of BeanPostProcessors
 * <li>InitializingBean's {@code afterPropertiesSet}
 * <li>a custom init-method definition
 * <li>{@code postProcessAfterInitialization} methods of BeanPostProcessors
 * </ol>
```

1. 首先是各种 Aware ，
1. 其次是 BeanPostProcessor 前置处理，
1. 随后是 init 方法
1. 最后是 BeanPostProcessor 后置处理

上面是 `Bean` 的 `创建`以及`初始化`的步骤，下面我们来看销毁的步骤：


```java
 * <p>On shutdown of a bean factory, the following lifecycle methods apply:
 * <ol>
 * <li>{@code postProcessBeforeDestruction} methods of DestructionAwareBeanPostProcessors
 * <li>DisposableBean's {@code destroy}
 * <li>a custom destroy-method definition
 * </ol>
```
我们可以看到在容器关闭时 

1. 如果 `bean` 实现了 `DisposableBean` 接口，则调用 `destroy`方法，

1. 如果 `bean` 配置了 `destroy-method` ，则调用其指定的方法。


