# AOP概念

AOP (Aspect Orient Programming) ，作为面向对象编程的一种补充，广泛应用于处理一些具有横切性质的系统级服务，如事务管理、安全检查、缓存、对象池管理、日志打印等。

AOP 实现的关键就在于为对象实现代理行为。AOP 可以分为静态代理 和动态代理两类。

- 静态代理：是指在编译时生成代理类，所以也称之为编译时增强。

- 动态代理：是指在运行时在内存中临时生成代理类，所以也被称之为运行时增强。

动态代理一般通过 JDK动态代理、字节码生成技术 来实现。


## SpringAOP设计与实现

想要了解Spring AOP 的设计，我们需要了解几方面

1. Java程序在JVM中执行的特征

1. Java程序运行流程



当我们连续调用一些方法时，在线程中的栈呈现以下情况：

![20201201164504](http://qiniu.liulei.life/20201201164504.png)

从线程角度看，`JVM` 处理 `Java` 程序的基本单位是方法调用。实际上，JVM 运行的最基本单位的指令（即原子指令）是汇编语言性质的机器字节码。

我们也可以使用 `Thread.dumpStack()` 方法，输出调用栈：





## SpringAOP术语

要完全理解Spring AOP首先要理解AOP的核心概念和术语，这些术语并不是Spring指定的，而且很不幸，这些术语并不能直观理解，但是，如果Spring使用自己的术语，那将更加令人困惑。

- **Aspect**：切面，由一系列切点、增强和引入组成的模块对象，可定义优先级，从而影响增强和引入的执行顺序。事务管理（Transaction management）在java企业应用中就是一个很好的切面样例。


- **Join point**：接入点，程序执行期的一个点，例如方法执行、类初始化、异常处理。 在Spring AOP中，接入点始终表示方法执行。


- **Advice**：增强，切面在特定接入点的执行动作，包括 “around,” “before” and "after"等多种类型。包含Spring在内的许多AOP框架，通常会使用拦截器来实现增强，围绕着接入点维护着一个拦截器链。


- **Pointcut**：切点，用来匹配特定接入点的谓词（表达式），增强将会与切点表达式产生关联，并运行在任何切点匹配到的接入点上。通过切点表达式匹配接入点是AOP的核心，Spring默认使用AspectJ的切点表达式。


- **Introduction**：引入，为某个type声明额外的方法和字段。Spring AOP允许你引入任何接口以及它的默认实现到被增强对象上。


- **Target object**：目标对象，被一个或多个切面增强的对象。也叫作被增强对象。既然Spring AOP使用运行时代理（runtime proxies），那么目标对象就总是代理对象。


- **AOP proxy**：AOP代理，为了实现切面功能一个对象会被AOP框架创建出来。在Spring框架中AOP代理的默认方式是：有接口，就使用基于接口的JDK动态代理，否则使用基于类的CGLIB动态代理。但是我们可以通过设置proxy-target-class="true"，完全使用CGLIB动态代理。


- **Weaving**：织入，将一个或多个切面与类或对象链接在一起创建一个被增强对象。织入能发生在编译时 （compile time ）(使用AspectJ编译器)，加载时（load time），或运行时（runtime） 。Spring AOP默认就是运行时织入，可以通过枚举AdviceMode来设置。



# SpringAOP加载分析

我们来看一下 Spring Aop 是如何加入到Spring中来的。

开启Aop有两种方式
- XML方式：`<aop:aspectj-autoproxy/>`
- 注解方式：`@EnableAspectJAutoProxy`

上面这两种其实原理相同都是通过注册一个Bean 来实现的。

我们这里介绍一下使用注解开启AOP的流程。、

`Spring` 解析`BeanDefinition` 时，发现 `<aop:aspectj-autoproxy/>` 标签，则调用 AOP 框架进行自定义解析 - 详细过程请看 《加载BeanDefinition》 概述
```java
// BeanDefinitionParserDelegate.java

@Nullable
public BeanDefinition parseCustomElement(Element ele) {
    return parseCustomElement(ele, null);
}

@Nullable
public BeanDefinition parseCustomElement(Element ele, 
                                        @Nullable BeanDefinition containingBd) {
    // 获取 namespaceUri
    String namespaceUri = getNamespaceURI(ele);
    if (namespaceUri == null) {
        return null;
    }
    // 根据 namespaceUri 获取相应的 Handler
    NamespaceHandler handler = this.readerContext
                        .getNamespaceHandlerResolver().resolve(namespaceUri);
    if (handler == null) {
        error("Unable to locate Spring NamespaceHandler for XML schema namespace [" 
            + namespaceUri + "]", ele);
        return null;
    }
    // 调用自定义的 Handler 处理 - 这里调用Aop的 handle
    return handler.parse(ele, 
                        new ParserContext(this.readerContext, this, containingBd));
}
```
这里调用 `AopNamespaceHandler`，我们来看一下
```java
public class AopNamespaceHandler extends NamespaceHandlerSupport {

    //这里注册了一个BeanDefinition 的解析器
    @Override
    public void init() {
        // In 2.0 XSD as well as in 2.5+ XSDs
        registerBeanDefinitionParser("config", 
            new ConfigBeanDefinitionParser());

        //重点是这句
        registerBeanDefinitionParser("aspectj-autoproxy", 
            new AspectJAutoProxyBeanDefinitionParser());

        registerBeanDefinitionDecorator("scoped-proxy", 
            new ScopedProxyBeanDefinitionDecorator());

        // Only in 2.0 XSD: moved to context namespace in 2.5+
        registerBeanDefinitionParser("spring-configured", 
            new SpringConfiguredBeanDefinitionParser());
    }

}
```
然后到类 `AspectJAutoProxyBeanDefinitionParser：`
```java
class AspectJAutoProxyBeanDefinitionParser implements BeanDefinitionParser {

   @Override
   @Nullable
   public BeanDefinition parse(Element element, ParserContext parserContext) {
        //重点
        AopNamespaceUtils.registerAspectJAnnotationAutoProxyCreatorIfNecessary(
                parserContext, 
                element);
        extendBeanDefinition(element, parserContext);
        return null;
   }
   ...
}
```
再进去`registerAspectJAnnotationAutoProxyCreatorIfNecessary()` 方法
```java
public static BeanDefinition registerAspectJAnnotationAutoProxyCreatorIfNecessary(
            BeanDefinitionRegistry registry,
            @Nullable Object source) {

   return registerOrEscalateApcAsRequired(
            AnnotationAwareAspectJAutoProxyCreator.class, 
            registry, 
            source);
}
```
最终我们看到 AOP 向 `Spring` 注册了一个 `AnnotationAwareAspectJAutoProxyCreator` 的Bean，我们来看一下这个类的类图。

![20201125211921](http://qiniu.liulei.life/20201125211921.png)

我们可以看到该类实现了 `BeanPostProcessor` 接口，所以在 执行`Bean后置处理器`的方法时，我们就能够返回代理对象了

## 总结
![112521511780_0AOP启动分析_1](http://qiniu.liulei.life/112521511780_0AOP启动分析_1.Jpeg)


# 代理类创建的源码分析

接下来我们从类图、IoC容器等角度，来分析源码

## 类图

`AbstractAdvisorAutoProxyCreator` 类是代理功能的主要实现类，我们来看一下它的类图

![20201125211731](http://qiniu.liulei.life/20201125211731.png)

我们需要注意的是，该类实现了 `BeanPostProcessor` 接口，是一个 `Bean后置处理器`。

`Spring` 就是通过 `Bean后置处理器` 完成 代理类 的创建的。


## 回顾IoC源码

首先我们回顾一下 `IoC` 源码，找到是在何时创建代理对象的。

```java
//AbstractAutowireCapableBeanFactory

protected Object doCreateBean(。。。。。){

    // Instantiate the bean.
    BeanWrapper instanceWrapper = null;
    。。。。
    //实例化Bean对象
    instanceWrapper = createBeanInstance(beanName, mbd, args);
    。。。。
    //填充Bean属性
    populateBean(beanName, mbd, instanceWrapper);
    //bean初始化
    exposedObject = initializeBean(beanName, exposedObject, mbd);
    。。。。
}
```
我们的代理对象就是在 Bean 初始化时创建的，这里需要注意的是，我们在创建代理对象前，首先通过反射创建了一个普通的Bean。


```java
protected Object initializeBean(...) {
    ....
    //调用前置处理方法
    wrappedBean = applyBeanPostProcessorsBeforeInitialization(wrappedBean, beanName);
    ....
    //调用初始化方法
    invokeInitMethods(beanName, wrappedBean, mbd);
    ...
    //重点：调用后置处理方法 - 代理对象就是在这一步返回的
    wrappedBean = applyBeanPostProcessorsAfterInitialization(wrappedBean, beanName);
    ....
}
```

接下来我们重点来看后置处理器是如何创建代理类的。


## 后置处理器创建代理类
```java
//AbstractAutoProxyCreator

public Object postProcessAfterInitialization(@Nullable Object bean, 
                                            String beanName) {
    //首先检查Bean是否被创建了
    if (bean != null) {
        Object cacheKey = getCacheKey(bean.getClass(), beanName);
        if (this.earlyProxyReferences.remove(cacheKey) != bean) {
            //有必要时创建代理类 
            return wrapIfNecessary(bean, beanName, cacheKey);
        }
    }
    return bean;
}
```

继续看 `wrapIfNecessary` 方法

```java
protected Object wrapIfNecessary(Object bean, 
                                String beanName, 
                                Object cacheKey) {
   if (beanName != null && this.targetSourcedBeans.contains(beanName)) {
      return bean;
   }
   if (Boolean.FALSE.equals(this.advisedBeans.get(cacheKey))) {
      return bean;
   }
   if (isInfrastructureClass(bean.getClass()) || shouldSkip(bean.getClass(), beanName)) {
      this.advisedBeans.put(cacheKey, Boolean.FALSE);
      return bean;
   }

   // 返回匹配当前 bean 的所有的 advisor、advice、interceptor
   // 对于本文的例子，"userServiceImpl" 和 "OrderServiceImpl" 这两个 bean 创建过程中，
   //   到这边的时候都会返回两个 advisor
   Object[] specificInterceptors = getAdvicesAndAdvisorsForBean(
                                        bean.getClass(), beanName, null);
   if (specificInterceptors != DO_NOT_PROXY) {
      this.advisedBeans.put(cacheKey, Boolean.TRUE);
      // 创建代理...创建代理...创建代理...
      Object proxy = createProxy(
            bean.getClass(), beanName, 
            specificInterceptors, new SingletonTargetSource(bean));
      this.proxyTypes.put(cacheKey, proxy.getClass());
      return proxy;
   }

   this.advisedBeans.put(cacheKey, Boolean.FALSE);
   return bean;
}
```

这里有两个重要的点

- **getAdvicesAndAdvisorsForBean(。。。)**： 这个方法将得到所有的可用于拦截当前 bean 的 advisor、advice、interceptor。

- `TargetSource`：即 `createProxy` 方法中的 `new SingletonTargetSource(bean)` 参数。它用于封装真实类的信息。

我们继续往下看 `createProxy()` 方法：
```java
// 注意看这个方法的几个参数，
//   第三个参数携带了所有的 advisors
//   第四个参数 targetSource 携带了真实实现的信息
protected Object createProxy(
      Class<?> beanClass, String beanName, 
      Object[] specificInterceptors, TargetSource targetSource) {

   if (this.beanFactory instanceof ConfigurableListableBeanFactory) {
      AutoProxyUtils.exposeTargetClass(
            (ConfigurableListableBeanFactory) this.beanFactory, 
            beanName, beanClass);
   }

   // 创建 ProxyFactory 实例
   ProxyFactory proxyFactory = new ProxyFactory();
   proxyFactory.copyFrom(this);

   // 在 schema-based 的配置方式中，我们介绍过，如果希望使用 CGLIB 来代理接口，可以配置
   // proxy-target-class="true",这样不管有没有接口，都使用 CGLIB 来生成代理：
   //   <aop:config proxy-target-class="true">......</aop:config>
   if (!proxyFactory.isProxyTargetClass()) {
      if (shouldProxyTargetClass(beanClass, beanName)) {
         proxyFactory.setProxyTargetClass(true);
      }
      else {
         // 点进去稍微看一下代码就知道了，主要就两句：
         // 1. 有接口的，调用一次或多次：proxyFactory.addInterface(ifc);
         // 2. 没有接口的，调用：proxyFactory.setProxyTargetClass(true);
         evaluateProxyInterfaces(beanClass, proxyFactory);
      }
   }

   // 这个方法会返回匹配了当前 bean 的 advisors 数组

   // 对于本文的例子，"userServiceImpl" 和 "OrderServiceImpl" 
   //   到这边的时候都会返回两个 advisor

   // 注意：如果 specificInterceptors 中有 advice 和 interceptor，
   //   它们也会被包装成 advisor，进去看下源码就清楚了
   Advisor[] advisors = buildAdvisors(beanName, specificInterceptors);
   for (Advisor advisor : advisors) {
      proxyFactory.addAdvisor(advisor);
   }

   proxyFactory.setTargetSource(targetSource);
   customizeProxyFactory(proxyFactory);

   proxyFactory.setFrozen(this.freezeProxy);
   if (advisorsPreFiltered()) {
      proxyFactory.setPreFiltered(true);
   }

   return proxyFactory.getProxy(getProxyClassLoader());
}
```
Spring 在这个方法内创建了一个 `proxyFactory` 实例，
随后设置一些内容，重要的有以下两个属性：

- advisors 数组（advice - 增强方法、interceptor - 拦截器）

- 真实实现类。

随后通过 `proxyFactory` 创建代理类。

接下来我们重点看一下 `ProxyFactory` 


## ProxyFactory详解

接下来我们从类图的**继承关系**和**源码**来分析是如何**创建代理对象**的。


### ProxyFactory类图


![20201202081749](http://qiniu.liulei.life/20201202081749.png)


通过类图发现 `ProxyFactory` 继承了 `AdvisedSupport` 类，这也说明了上面代码中`ProxyFactory` 类添加了了 `Advisor` 数组，该数组保存着 `advice`（增强方法）、`interceptor`(拦截器)。这是AOP得以层层代理的关键。

### ProxyFactory源码解析


1. 我们首先看一下构造函数，其中几个构造函数比较重要

   无参构造
   ```java
   public ProxyFactory() {
	}
   ```
   指定 代理类实现接口 
   ```java
   public ProxyFactory(Class<?>... proxyInterfaces) {
         setInterfaces(proxyInterfaces);
   }
   public void setInterfaces(Class<?>... interfaces) {
      Assert.notNull(interfaces, "Interfaces must not be null");
      this.interfaces.clear();
      for (Class<?> ifc : interfaces) {
         //重点
         addInterface(ifc);
      }
   }
   ```
   指定 代理类实现接口 和 拦截器
   ```java
   public ProxyFactory(Class<?> proxyInterface, Interceptor interceptor) {
      addInterface(proxyInterface);
      addAdvice(interceptor);
   }
   ```
   我们除了在构造函数中指定`代理类要实现的接口 - proxyInterface` 还可以使用`addInterface(proxyInterface)`方法为生成的代理类添加接口，后面可以看到当指定代理类实现接口时(不设置强制使用CGLib)，`Spring` 会使用 `JDK 动态代理` 创建对象，具体的我们会在Spring AOP 调用分析 说到


1. 继续看 `ProxyFactory.getProxy()` ,该方法是创建代理对象的入口方法
   ```java
   public Object getProxy(ClassLoader classLoader) {
      return createAopProxy().getProxy(classLoader);
   }
   ```

1. 继续看 `createAopProxy` 方法

   1. 该方法首先通过 `createAopProxy()` 创建一个 `AopProxy` 的实例

   1. 随后通过 `createAopProxy(this)` 方法将当前实例传入，创建代理对象。这一点非常重要，

   ```java
   protected final synchronized AopProxy createAopProxy() {
      if (!this.active) {
         activate();
      }
      return 
         getAopProxyFactory()//创建AopProxy 对象
            .createAopProxy(this);//传入当前实例，并创建代理对象
   }
   ```

1. 查看 `getAopProxyFactory` 方法

   发现这个方法是直接返回的，那么 `this.aopProxyFactory` 是何时构建的呢
   ```java
   public AopProxyFactory getAopProxyFactory() {
      return this.aopProxyFactory;
   }
   ```
   通过查看上面的类图发现 `ProxyFactory` 继承了 `ProxyCreatorSupport` 类，而该类的构造创建了一个 `aopProxyFactory` 对象，代码如下

   ```java
   public ProxyCreatorSupport() {
      this.aopProxyFactory = new DefaultAopProxyFactory();
   }
   ```


1. 回到 `ProxyFactory.createAopProxy` 方法中，接下来看 代码中同名的`createAopProxy` 方法（它创建了 AopProxy对象，该对象用于创建代理对象）


   通过上面的代码我们知道 `getAopProxyFactory` 返回了 `DefaultAopProxyFactory` 实例 随后调用`createAopProxy(…) `，来到 `DefaultAopProxyFactory` 类：
   ```java
   //DefaultAopProxyFactory

   public class DefaultAopProxyFactory implements AopProxyFactory, Serializable {

      @Override
      public AopProxy createAopProxy(AdvisedSupport config) throws AopConfigException {

         //判定是否 指定代理类实现接口
         // (optimize，默认false) || (proxy-target-class=true) || (没有接口)
         if (config.isOptimize() 
               || config.isProxyTargetClass() 
               || hasNoUserSuppliedProxyInterfaces(config)) {

            Class<?> targetClass = config.getTargetClass();
            if (targetClass == null) {
               throw new AopConfigException(
                  "TargetSource cannot determine target class: " 
                  + "Either an interface or a target is required for proxy creation.");
            }
            // 如果要代理的类本身就是接口，也会用 JDK 动态代理
            // 我也没用过这个。。。
            if (targetClass.isInterface() || Proxy.isProxyClass(targetClass)) {
               return new JdkDynamicAopProxy(config);
            }
            //返回CGLib 的代理 工厂，用于创建实际对象
            return new ObjenesisCglibAopProxy(config);
         }
         else {
            // 如果指定代理类实现接口，会跑到这个分支
            //返回JDK动态代理工厂，用于创建实际对象
            return new JdkDynamicAopProxy(config);
         }
      }
      // 判断是否有实现自定义的接口
      private boolean hasNoUserSuppliedProxyInterfaces(AdvisedSupport config) {
         Class<?>[] ifcs = config.getProxiedInterfaces();
         return (ifcs.length == 0 
                 || (ifcs.length == 1 
                     && SpringProxy.class.isAssignableFrom(ifcs[0])));
      }

   }
   ```

   到这里，我们知道 `DefaultAopProxyFactory.createAopProxy` 方法有可能返回 `JdkDynamicAopProxy` 实例，也有可能返回 `ObjenesisCglibAopProxy` 实例。



1. 当我们有了 `AopProxy` 实例以后，我们就回到`ProxyFactory.getProxy(,,,)`，通过该实例，来创建代理对象

   ```java
   //AopProxy

   public Object getProxy(ClassLoader classLoader) {
      return createAopProxy().getProxy(classLoader);
   }
   ```



总结一下：

1. 我们首先为 `ProxyFactory` 设置了 `Advisor` 数组（增强方法、拦截器）和真实对象

1. 随后创建 `AopProxy` 实例，并将 `ProxyFactory` 实例传入。并根据`AopProxy`中是否指定代理类实现接口 配置否继承接口选择CGLIB或者JDK代理，创建 `AopProxy` 实例

   > 设置了 `proxy-target-class="true"`，那么都会使用 `CGLIB`。

1. 通过 `AopProxy` 创建代理对象

注意：JDK 动态代理基于接口，所以只有接口中的方法会被增强，而 `CGLIB` 基于类继承，需要注意就是如果方法使用了 `final` 、 `private`  修饰，是不能被增强的。


由于 `AopProxy` 实例有可能是 `CglibAopProxy` 或 `JdkDynamicAopProxy` 接下来我们看一下这三个类

## AopProxy

`AopProxy` 接口，及其子类继承关系如类图所示：

![20201202104622](http://qiniu.liulei.life/20201202104622.png)

我们来看一下 `AopProxy` 的签名

```java
public interface AopProxy {

   /**
   * 创建一个代理对象。（为目标对象创建一个代理对象。）
   */
   Object getProxy();

   /**
   * 重载方法。上面的无参方法会调用这个重载方法。
   */
   Object getProxy(@Nullable ClassLoader classLoader);
}
```

### JdkDynamicAopProxy创建代理对象

1. 首先来看一下构造方法
   ```java
   public JdkDynamicAopProxy(AdvisedSupport config) throws AopConfigException {
      Assert.notNull(config, "AdvisedSupport must not be null");
      if (config.getAdvisors().length == 0 
         && config.getTargetSource() == AdvisedSupport.EMPTY_TARGET_SOURCE) {
         throw new AopConfigException("No advisors and no TargetSource specified");
      }
      this.advised = config;
   }
   ```
   从构造器可以看出，`JdkDynamicAopProxy`依赖于`AdvisedSupport`，根据 `config` 配置信息创建动态代理对象。代码中 `config.getAdvisors()` 提供的是`Advisor` 数组，即向 `JdkDynamicAopProxy` 提供了调用链。

1. 然后来看一下 `getProxy` 方法，该方法创建了代理对象

   ```java
   @Override
   public Object getProxy(@Nullable ClassLoader classLoader) {
      if (logger.isTraceEnabled()) {
         logger.trace("Creating JDK dynamic proxy: " + this.advised.getTargetSource());
      }
      Class<?>[] proxiedInterfaces = 
         AopProxyUtils.completeProxiedInterfaces(this.advised, true);
      findDefinedEqualsAndHashCodeMethods(proxiedInterfaces);
      return Proxy.newProxyInstance(classLoader, proxiedInterfaces, this);
   }
   ```
   `java.lang.reflect.Proxy.newProxyInstance(…)`方法需要三个参数，

   - `ClassLoader，`

   - 需要实现的些接口，

   - 是 `InvocationHandler` 实例，我们看到这里传了 this，因为 `JdkDynamicAopProxy` 本身实现了 `InvocationHandler` 接口，而`JdkDynamicAopProxy` 则向代理对象提供了调用链。


到这一步Spring 就使用JDK代理创建了代理对象，随后会统一分析Spring AOP的代理对象调用流程。

### CglibAopProxy创建代理对象

同样我们先来看 `CglibAopProxy` 的类图

![20201203081636](http://qiniu.liulei.life/20201203081636.png)

随后我们来看一下构造方法

```java
public CglibAopProxy(AdvisedSupport config) throws AopConfigException {
   Assert.notNull(config, "AdvisedSupport must not be null");
   if (config.getAdvisors().length == 0 
         && config.getTargetSource() == AdvisedSupport.EMPTY_TARGET_SOURCE) {
      throw new AopConfigException("No advisors and no TargetSource specified");
   }
   this.advised = config;
   this.advisedDispatcher = new AdvisedDispatcher(this.advised);
}
```
与 `JdkDynamicAopProxy` 相同，同样依赖了`AdvisedSupport`，根据 `config` 配置信息创建动态代理对象，该对象也包含着 拦截器等信息

随后调用了 `getProxy` 方法创建代理对象
```java
@Override
public Object getProxy(ClassLoader classLoader) {
      ...
      // Configure CGLIB Enhancer...
      Enhancer enhancer = createEnhancer();
      if (classLoader != null) {
         enhancer.setClassLoader(classLoader);
         if (classLoader instanceof SmartClassLoader &&
               ((SmartClassLoader) classLoader).isClassReloadable(proxySuperClass)) {
            enhancer.setUseCache(false);
         }
      }
      enhancer.setSuperclass(proxySuperClass);
      enhancer.setInterfaces(AopProxyUtils.completeProxiedInterfaces(this.advised));
      enhancer.setNamingPolicy(SpringNamingPolicy.INSTANCE);
      enhancer.setStrategy(new ClassLoaderAwareUndeclaredThrowableStrategy(classLoader));

      Callback[] callbacks = getCallbacks(rootClass);
      Class<?>[] types = new Class<?>[callbacks.length];
      for (int x = 0; x < types.length; x++) {
         types[x] = callbacks[x].getClass();
      }
      // fixedInterceptorMap only populated at this point, after getCallbacks call above
      enhancer.setCallbackFilter(new ProxyCallbackFilter(
            this.advised.getConfigurationOnlyCopy(), this.fixedInterceptorMap, this.fixedInterceptorOffset));
      enhancer.setCallbackTypes(types);

      // Generate the proxy class and create a proxy instance.
      return createProxyClassAndInstance(enhancer, callbacks);
   }
   catch (CodeGenerationException ex) {
      ...
   }
   catch (IllegalArgumentException ex) {
      ...
   }
   catch (Throwable ex) {
      ...
   }
}
```
`CGLIB` 生成代理的核心类是 `Enhancer` 类




## 总结

![112521434007_0AOP代理类生成流程_1](http://qiniu.liulei.life/112521434007_0AOP代理类生成流程_1.Jpeg)


# SpringAOP调用分析

从上面的代码得出 `Spring AOP` 生成的代理类分别有JDK动态代理和CGLib代理。


 ## 基础代码

接口
```java
interface TestMethodInterceptorInterface {
    String doSomeThing(String someThing);
}
```

实现类
```java
public class TestMethodInterceptor implements TestMethodInterceptorInterface {
    @Override
    public void doSomeThing(String someThing) {
        System.out.println("执行被拦截的方法：" + someThing);
    }
}
```
拦截器 1 
```java
public class MethodInterceptor1 implements MethodInterceptor {
   @Override
   public Object invoke(MethodInvocation methodInvocation) throws Throwable {

      Object result = null;
      try {
            System.out.println("方法执行之前1：" + methodInvocation.getMethod().toString());
            result = methodInvocation.proceed();
            System.out.println("方法执行之后1：" + methodInvocation.getMethod().toString());
            return result;
      } catch (Exception e) {
            System.out.println("方法出现异常1:" + e.toString());
            return result;
      }
   }
}
```
拦截器 2
```java
public class MethodInterceptor2 implements MethodInterceptor {
   @Override
   public Object invoke(MethodInvocation methodInvocation) throws Throwable {

      Object result = null;
      try {
            System.out.println("方法执行之前2：" + methodInvocation.getMethod().toString());
            result = methodInvocation.proceed();
            System.out.println("方法执行之后2：" + methodInvocation.getMethod().toString());
            return result;
      } catch (Exception e) {
            System.out.println("方法出现异常2:" + e.toString());
            return result;
      }
   }
}
```
执行
```java
public static void main(String[] args) {
   //创建代理工厂
   ProxyFactory proxyFactory = new ProxyFactory();
   
   //设置 被代理对象
   proxyFactory.setTarget(new TestMethodInterceptor());
   //设置 拦截器
   proxyFactory.addAdvice(new MethodInterceptor1());
   //设置 拦截器
   proxyFactory.addAdvice(new MethodInterceptor2());
   
   //创建代理对象
   Object proxy = proxyFactory.getProxy();
   TestMethodInterceptorInterface o = (TestMethodInterceptorInterface) proxy;

   //执行方法
   o.doSomeThing("通过代理工厂设置代理对象，拦截代理方法");
}
```
输出
```
方法执行之前1：...TestMethodInterceptor.doSomeThing(java.lang.String)
方法执行之前2：...TestMethodInterceptor.doSomeThing(java.lang.String)
执行被拦截的方法：通过代理工厂设置代理对象，拦截代理方法
方法执行之后2：...TestMethodInterceptor.doSomeThing(java.lang.String)
方法执行之后1：...TestMethodInterceptor.doSomeThing(java.lang.String)
```

## 基于JDK代理调用流程
首先我们在 `Spring` 中配置 `proxy-target-class` 属性为 `false`，这样指定使用JDK动态代理。

在Spring Boot 中可以这样配置 `spring.aop.proxy-target-class=false`


### 生成代理类
首先查看生成的jdk代理类

在编译时添加

> -Djdk.proxy.ProxyGenerator.saveGeneratedFiles=true

我们这里只看继承关系和代理方法，继承关系如下：
```java
public final class $Proxy46 extends Proxy 
      implements BeanAInterface, SpringProxy, Advised, DecoratingProxy {
   .....
}
```
我们可以看到它继承了 `Proxy` 类，实现了我们自定义的 `BeanAInterface` 接口，以及`Spring` 提供的代理接口  `SpringProxy, Advised, DecoratingProxy`

代理方法：
```java
public final void log() throws  {
   try {
      super.h.invoke(this, m3, (Object[])null);
   } catch (RuntimeException | Error var2) {
      throw var2;
   } catch (Throwable var3) {
      throw new UndeclaredThrowableException(var3);
   }
}
```
`h` 则是继承自 `Proxy`类中的成员变量，定义如下：
```java
protected InvocationHandler h;
```

### 调用分析
1. 在 `log` 方法被调用时打上断点，随后运行程序

   ![20201207100022](http://qiniu.liulei.life/20201207100022.png)

1. 当运行到 `log` 方法时，点击 `f7` 步入方法, `JdkDynamicAopProxy` 的 `invoke` 方法
   ```java
   public Object invoke(Object proxy, 
                        Method method, 
                        Object[] args) throws Throwable {
      ........
   }
   ```

   对应生成类则是调用父类的 `invoke` 方法
   ![20201207100312](http://qiniu.liulei.life/20201207100312.png)
   由于我们已经步入 `JdkDynamicAopProxy` 方法，所以我们可以确定 `h` 就是 `JdkDynamicAopProxy`  的实例，它的继承关系如下：
   ```java
   final class JdkDynamicAopProxy 
      implements AopProxy, InvocationHandler, Serializable {
         。。。。。。
   }
   ``` 
   我们也可以看到  `JdkDynamicAopProxy` 确实实现了 `InvocationHandler` 

1. 我们接下来，分析 `JdkDynamicAopProxy` 的 `invoke` 方法，首先看`invoke` 中的三个 `if` 判断

   ![20201207104353](http://qiniu.liulei.life/20201207104353.png)

   - 第一个 `if`：如果代理对象执行 `equal` 方法，则执行 `JdkDynamicAopProxy` 的 `equal` 方法（即代理对象的equal方法）。也就是说Spring 对equal 方法放行

   - 第二个 `if`：是不拦截 `hashCode` 方法

   - 第三个 `if`：todo

   - 第四个 `if`：如果被代理的对象本身就是实现了Advised接口，也不做处理，直接执行

1. 随后获取拦截器调用链 （这个调用链是在`ProxyFactory`创建 `JdkDynamicAopProxy` 的时候传入的。而 `JdkDynamicAopProxy` 创建了代理对象并将自己作为构造参数传入）

   ![20201207113156](http://qiniu.liulei.life/20201207113156.png)

   ```java
   public List<Object> getInterceptorsAndDynamicInterceptionAdvice(
               Method method, 
               @Nullable Class<?> targetClass) {
      //从缓存中寻找该方法是否已经获取过
      MethodCacheKey cacheKey = new MethodCacheKey(method);
      List<Object> cached = this.methodCache.get(cacheKey);
      //如果没有获取则从容器中获取，填入缓存
      if (cached == null) {
         //从调用链工厂中获取拦截器链
         cached = this.advisorChainFactory.
                  getInterceptorsAndDynamicInterceptionAdvice(
                        this, 
                        method, 
                        targetClass);

         this.methodCache.put(cacheKey, cached);
      }
      return cached;
   }
   ```
1. 我们拿到了拦截器调用链之后，执行以下代码
    ```java
    //检查拦截器调用链是否为空
    if (chain.isEmpty()) {
        
        //反射调用目标对象方法
        Object[] argsToUse = AopProxyUtils.adaptArgumentsIfNecessary(method, args);
        retVal = AopUtils.invokeJoinpointUsingReflection(target, method, argsToUse);
    }
    else {
        //将目标对象、调用链等封装成一个 MethodInvocation 对象
        MethodInvocation invocation =
                new ReflectiveMethodInvocation(proxy, 
                                                target, 
                                                method, 
                                                args, 
                                                targetClass, 
                                                chain);
        //通过拦截器调用 拦截点
        retVal = invocation.proceed();
    }
    ```
1. 到这一步就进入到了通用调用代理对象的步骤，即JDK和CGLIB 都会进入到 `ReflectiveMethodInvocation` 类




## 基于CGLib调用流程

首先我们在程序启动时键入以下代码，就可以看到CGlib生成的类
```java
System.setProperty(DebuggingClassWriter.DEBUG_LOCATION_PROPERTY, "path");
```

我们一般会看到被代理类生成了三个类

![20201203094325](http://qiniu.liulei.life/20201203094325.png)

其中 `...$$EnhancerBySpringCGLIB$$...` 类则是，真正的代理类。我们去看一下它生成的方法。

```java
public final void log() {
   MethodInterceptor var10000 = this.CGLIB$CALLBACK_0;
   if (var10000 == null) {
      CGLIB$BIND_CALLBACKS(this);
      var10000 = this.CGLIB$CALLBACK_0;
   }

   if (var10000 != null) {
      var10000.intercept(this, CGLIB$log$0$Method, CGLIB$emptyArgs, CGLIB$log$0$Proxy);
   } else {
      super.log();
   }
}
```
当我们调用 log 方法时就会进入 `org.springframework.aop.framework.CglibAopProxy.DynamicAdvisedIntercepto` 的 `intercept` 方法

```java
// 执行目标方法时 会先执行
public Object intercept(Object proxy, 
                        Method method, 
                        Object[] args, 
                        MethodProxy methodProxy) throws Throwable {

    Object oldProxy = null;
    boolean setProxyContext = false;
    Class<?> targetClass = null;
    Object target = null;
    try {
        if (this.advised.exposeProxy) {

            // 如果需要，使调用可用
            oldProxy = AopContext.setCurrentProxy(proxy);
            setProxyContext = true;
        }
        
        //可能为空。尽量晚到，把我们的时间减到最少
        //“拥有”目标，以防它来自一个池…
        target = getTarget();
        if (target != null) {
            targetClass = target.getClass();
        }
        //获取目标方法拦截器链
        List<Object> chain = 
            this.advised.getInterceptorsAndDynamicInterceptionAdvice(method, targetClass);
        Object retVal;

        //检查我们是否只有一个InvokerInterceptor:也就是说，没有真正的建议，只是对目标的反射调用。
        if (chain.isEmpty() && Modifier.isPublic(method.getModifiers())) {

        //我们可以跳过创建方法调用:直接调用目标。注意，最终的调用程序必须是InvokerInterceptor，
        //所以我们知道它只对目标执行反射操作，没有热交换或花哨的代理。
            Object[] argsToUse = AopProxyUtils.adaptArgumentsIfNecessary(method, args);
            retVal = methodProxy.invoke(target, argsToUse);
        }
        else {
          
            // 我们需要创建一个方法调用…
            //CglibMethodInvocation 的.proceed() 方法才是最终实行我们设置的方法通知的增强器接口
            retVal = new CglibMethodInvocation(proxy, 
                                            target, 
                                            method, 
                                            args, 
                                            targetClass, 
                                            chain, 
                                            methodProxy).proceed();
        }
        retVal = processReturnType(proxy, target, method, retVal);
        return retVal;
    }
    finally {
        if (target != null) {
            releaseTarget(target);
        }
        if (setProxyContext) {
            // Restore old proxy.
            AopContext.setCurrentProxy(oldProxy);
        }
    }
}
```
`CglibMethodInvocation` 类的 `proceed` 即 进入 `ReflectiveMethodInvocation` 类的 `proceed`

## JDK与CGLib后续共同的调用


1. 我们首先看一下 `ReflectiveMethodInvocation` 的构造函数
    ```java
    protected ReflectiveMethodInvocation(
            Object proxy, 
            @Nullable Object target, Method method, @Nullable Object[] arguments,
            @Nullable Class<?> targetClass, 
            List<Object> interceptorsAndDynamicMethodMatchers) {

        this.proxy = proxy;
        this.target = target;
        this.targetClass = targetClass;
        this.method = BridgeMethodResolver.findBridgedMethod(method);
        this.arguments = AopProxyUtils.adaptArgumentsIfNecessary(method, arguments);
        this.interceptorsAndDynamicMethodMatchers = interceptorsAndDynamicMethodMatchers;
    }
    ```
    该构造就是把我们之前的代理，目标对象，拦截的 `method` 的名称，拦截方法的参数和拦截器链全部整合到了 `ReflectiveMethodInvocation` 这个类中。也就是说我们可以在该类中实现拦截器 和目标对象的调用。

1. 我们继续向下看 `ReflectiveMethodInvocation` 的 `proceed` 方法

    ```java
    public Object proceed() throws Throwable {
        //1. 方法拦截器链为空的情况下
        //2. 方法拦截器链执行到最后的情况下爱执行
        if (this.currentInterceptorIndex == 
            this.interceptorsAndDynamicMethodMatchers.size() - 1) {
            return invokeJoinpoint();
        }

        //获取下一个拦截器（currentInterceptorIndex从 -1 开始，经过++ 为0）
        Object interceptorOrInterceptionAdvice =
                this.interceptorsAndDynamicMethodMatchers.get(
                        ++this.currentInterceptorIndex);
        
        //判断拦截器是否需要动态匹配切点
        if (interceptorOrInterceptionAdvice 
            instanceof InterceptorAndDynamicMethodMatcher) {
            // 评估动态方法匹配器这里:静态部分将已经评估并找到匹配。
            InterceptorAndDynamicMethodMatcher dm =
                    (InterceptorAndDynamicMethodMatcher) interceptorOrInterceptionAdvice;
            Class<?> targetClass = (this.targetClass != null 
                                ? this.targetClass 
                                : this.method.getDeclaringClass());
            if (dm.methodMatcher.matches(this.method, targetClass, this.arguments)) {
                return dm.interceptor.invoke(this);
            }
            else {
                //动态匹配失败。跳过这个拦截器并调用链中的下一个。
                return proceed();
            }
        }
        else {
            // 它是一个拦截器，所以我们只是调用它:切入点将在构造这个对象之前被静态地计算。
            //当前 MethodInterceptor 调用invoke(this) 方法的执行在下方进行拆解
            return ((MethodInterceptor) interceptorOrInterceptionAdvice).invoke(this);
        }
    }
    ```
    第一次调用 `invoke` 方法
    ```java
    //org.springframework.aop.interceptor.ExposeInvocationInterceptor
        
    public Object invoke(MethodInvocation mi) throws Throwable {
        MethodInvocation oldInvocation = invocation.get();
        //设置当前线程的变量
        invocation.set(mi);
        try {
            //再次执行 proceed()方法
            // org.springframework.aop.framework.ReflectiveMethodInvocation.proceed()
            return mi.proceed();
        }
        finally {
            invocation.set(oldInvocation);
        }
    }
    ```
    第二次调用 `invoke` 方法 拦截器链中第二个拦截器调用,此时会根据切点调用 `AbstractAspectJAdvice` 的子类对应，对应   `@Around`、 `@After`、`@Before` 。。。等注解
    ```java
    //例如：org.springframework.aop.aspectj.AspectJAroundAdvice
    //      对应 @Around 注解的方法拦截器

    public Object invoke(MethodInvocation mi) throws Throwable {
        if (!(mi instanceof ProxyMethodInvocation)) {
            throw new IllegalStateException(
                    "MethodInvocation is not a Spring ProxyMethodInvocation: " + mi);
        }
        ProxyMethodInvocation pmi = (ProxyMethodInvocation) mi;
        //将自首次 调用 proceed() 方法传入的 this 对象，
        // 包装成 ProceedingJoinPoint 对象传入，并调用代理方法
        ProceedingJoinPoint pjp = lazyGetProceedingJoinPoint(pmi);
        JoinPointMatch jpm = getJoinPointMatch(pmi);
        return invokeAdviceMethod(pjp, jpm, null, null);
    }
    ```
    当调用 `invokeAdviceMethod` 时 ，会进入我们自定义的 代理方法，例如
    ```java
    @Around("aopLog()")
    public Object aroundMethod(ProceedingJoinPoint joinPoint) {
        ....

        Object result = joinPoint.proceed();

        ....
        return result
    }
    ```
    当我们调用 `joinPoint.proceed()` 实际上就是调用 `ReflectiveMethodInvocation` 类的 `proceed()` 方法，
    当进入 `proceed()` 方法后，会第三次调用 `invoke` 方法，随后按照调用次序依次返回。
    即：拦截器1 -> 拦截器2 -> 目标对象 -> 拦截器2 -> 拦截器1

    

## 总结

![121609341445_0AOP调用流程_1](http://qiniu.liulei.life/121609341445_0AOP调用流程_1.Jpeg)


