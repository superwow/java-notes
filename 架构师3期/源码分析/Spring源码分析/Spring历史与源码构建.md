
# SPring简化开发的四个基本策略

1. 基于POJO的轻量级和最小侵入性编程

1. 通过依赖注入和面向接口实现松耦合

1. 基于切面和声明式编程

1. 通过切面和模板减少样板式代码

# Spring模块划分

<img src="../../../../java-notes/img/spring模块划分.png">

<table style="width:100%" >
    <tr>
        <th>模块名称</th>
        <th>主要功能</th>
    </tr>
    <tr>
        <td>spring-core</td>
        <td>IOC和DI的最基本实现</td>
    </tr>
    <tr>
        <td>spring-beans</td>
        <td>Bean工厂与Bean的装配</td>
    </tr>
    <tr>
        <td>spring-context</td>
        <td>定义基础的SpringContext上下文即IoC容器</td>
    </tr>
    <tr>
        <td>spring-context-support</td>
        <td>对SpringIOC容器的扩展支持，以及IOC子容器</td>
    </tr>
    <tr>
        <td>spring-context-indexer</td>
        <td>Spring的类管理组件和Classpath扫描</td>
    </tr>
    <tr>
        <td>spring-expression</td>
        <td>Spring表达式语言</td>
    </tr>
    <tr>
        <td>spring-aop</td>
        <td>面向切面的应用模块，整合ASM、CGLIB、JDKProxy</td>
    </tr>
    <tr>
        <td>spring-aspects</td>
        <td>继承Aspectj</td>
    </tr>
    <tr>
        <td>spring-instrument</td>
        <td>动态的Class loading模块</td>
    </tr>
    <tr>
        <td>spring-jdbc</td>
        <td>Spring提供的JDBC抽象框架的主要实现模块</td>
    </tr>
    <tr>
        <td>spring-tx</td>
        <td>JDBC事务控制实现</td>
    </tr>
    <tr>
        <td>spring-orm</td>
        <td>主要集成hibernate、Java Persistence API - jpa 和 Java Data Objects - JDO</td>
    </tr>
    <tr>
        <td>spring-oxm</td>
        <td>将Spring与XML互相映射</td>
    </tr>
    <tr>
        <td>spring-jms</td>
        <td>Java Messager Service能够接收和发送消息</td>
    </tr>
    <tr>
        <td>spring-web</td>
        <td>提供了最基础的web支持，主要建立在核心容器上，并能通过Servlet和Listeners来初始化IoC容器</td>
    </tr>
    <tr>
        <td>spring-webmvc</td>
        <td>实现了 model view controller 的web应用</td>
    </tr>
    <tr>
        <td>spring-websocket</td>
        <td>与前端的全双工协议</td>
    </tr>
    <tr>
        <td>spring-webflux</td>
        <td>一个响应式 web框架，可以用来建立异步的额非阻塞的事件驱动的服务</td>
    </tr>
    <tr>
        <td>spring-messaging</td>
        <td>从spring4开始加入的一个模块，主要职责是为spring框架集成的一些基础的报文传送应用</td>
    </tr>
</table>

## 模块间的依赖关系

<img src="../../../../java-notes/img/spring各模块之间的依赖关系.png">