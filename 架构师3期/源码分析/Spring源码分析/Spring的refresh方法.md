
Spring 的 refresh 方法是Spring的核心方法，它承担着启动容器、扫描配置、创建bean 、注册事件、国际化等功能。 

# 类图


![20201116212232](http://qiniu.liulei.life/20201116212232.png)


我们来看一下主要的几个接口：

- BeanFactory：是用于访问`Spring Bean`容器的根接口，是一个单纯的`Bean`工厂，也就是常说的`IOC`容器的顶层定义

- ResourceLocal：使我们用来加载资源的顶层定于

- MessageSource：国际化的顶层定义

- ApplicationEventPublisher：事件顶层定义


# 代码分析

首先我们来看一下入口

```java
public class ClassPathXmlApplicationContext 
            extends AbstractXmlApplicationContext {
    。。。。。。

    public ClassPathXmlApplicationContext(String[] configLocations, 
                                        boolean refresh, 
                                        ApplicationContext parent)
                                            throws BeansException {
        super(parent);
         //设置配置路径
        setConfigLocations(configLocations);
        if (refresh) {
            refresh();  //重点！！！
        }
    }
    .....................
}
```
接着我们来看一下本文的重点 `refresh` 方法
```java
@Override
public void refresh() throws BeansException, IllegalStateException {
    synchronized (this.startupShutdownMonitor) {
        
        // 刷新前的预处理;
        prepareRefresh();

        // 获取BeanFactory；默认实现是DefaultListableBeanFactory，在创建容器的时候创建的
        ConfigurableListableBeanFactory beanFactory = obtainFreshBeanFactory();

        // BeanFactory的预准备工作（BeanFactory进行一些设置，
        //      比如context的类加载器，BeanPostProcessor和XXXAware自动装配等）
        prepareBeanFactory(beanFactory);

        try {
            //BeanFactory准备工作完成后进行的后置处理工作
            postProcessBeanFactory(beanFactory);

            // 执行BeanFactoryPostProcessor的方法；
            invokeBeanFactoryPostProcessors(beanFactory);

            //注册BeanPostProcessor（Bean的后置处理器），在创建bean的前后等执行
            registerBeanPostProcessors(beanFactory);

            //初始化MessageSource组件（做国际化功能；消息绑定，消息解析）；
            initMessageSource();

            //初始化事件派发器
            initApplicationEventMulticaster();

            // 子类重写这个方法，在容器刷新的时候可以自定义逻辑；如创建Tomcat，Jetty等WEB服务器
            onRefresh();

            //注册应用的监听器。
            //  就是注册实现了ApplicationListener接口的监听器bean，
            //  这些监听器是注册到ApplicationEventMulticaster中的
            registerListeners();

             //初始化所有剩下的非懒加载的单例bean
            finishBeanFactoryInitialization(beanFactory);

            //完成context的刷新。
            //  主要是调用LifecycleProcessor的onRefresh()方法，
            //  并且发布事件（ContextRefreshedEvent）
            finishRefresh();
        } catch (BeansException ex) {
            if (logger.isWarnEnabled()) {
                logger.warn("Exception encountered during context initialization - " +
                        "cancelling refresh attempt: " + ex);
            }

            //  销毁已经创建的Bean
            destroyBeans();

            // 重置容器激活标签
            cancelRefresh(ex);

            // 抛出异常
            throw ex;
        } finally {
            // Reset common introspection caches in Spring's core, since we
            // might not ever need metadata for singleton beans anymore...
            resetCommonCaches();
        }
    }
}
```
## 刷新前的预处理 - prepareRefresh

```java
// AbstractApplicationContext.java

protected void prepareRefresh() {
    // 设置启动日期
    this.startupDate = System.currentTimeMillis();

    // 设置 context 当前状态
    this.closed.set(false);
    this.active.set(true);

    if (logger.isInfoEnabled()) {
        logger.info("Refreshing " + this);
    }

    // 初始化context environment（上下文环境）中的占位符属性来源
    initPropertySources();

    // 对属性进行必要的验证
    getEnvironment().validateRequiredProperties();

    //初始化事件集合
    this.earlyApplicationEvents = new LinkedHashSet<>();
}
```
`prepareRefresh` 要在 `refresh` 准备以下事情

- 设置 `Spring` 容器启动时间

- 设置容器活跃状态，撤销关闭状态

- 设置 `context environment`（上下文环境）中的占位符属性来源

- 验证必须属性

- 初始化事件集合

## 获取BeanFactory - obtainFreshBeanFactory();

```java
// AbstractApplicationContext.java

protected ConfigurableListableBeanFactory obtainFreshBeanFactory() {
    //刷新BeanFactory
    refreshBeanFactory();
    return getBeanFactory();
}
```
核心方法就在 `refreshBeanFactory()` 方法，该方法的核心任务就是创建 `BeanFactory` 并对其就行一番初始化。如下：

```java
//AbstractRefreshableApplicationContext


@Override
protected final void refreshBeanFactory() throws BeansException {

    // 若已有 BeanFactory ，销毁容器内的 Bean，并销毁 BeanFactory
    if (hasBeanFactory()) {
        destroyBeans();
        closeBeanFactory();
    }

    try {
        // 创建 BeanFactory 对象
        DefaultListableBeanFactory beanFactory = createBeanFactory();
        //指定序列化编号
        beanFactory.setSerializationId(getId());
        //定制BeanFactory的相关属性
        customizeBeanFactory(beanFactory);
        //加载BeanDefinition
        loadBeanDefinitions(beanFactory);
        //设置上下文的BeanFactory
        this.beanFactory = beanFactory;
    }
    catch (IOException ex) {
        throw new ApplicationContextException("I/O error parsing bean definition source for " + getDisplayName(), ex);
    }
}
```

1. 判断当前容器是否存在一个 `BeanFactory`，如果存在则对其进行销毁和关闭


1. 创建一个 `BeanFactory` 实例，其实就是 `DefaultListableBeanFactory` 。

1. 自定义 `BeanFactory`

1. 加载 `BeanDefinition` 。

1. 将创建好的 `beanFactory` 的引用交给的 `context` 来管理


重点看一下第四步，我们在这里跟进一下：
```java
//AbstractXmlApplicationContext

@Override
protected void loadBeanDefinitions(DefaultListableBeanFactory beanFactory) throws BeansException, IOException {
    // Create a new XmlBeanDefinitionReader for the given BeanFactory.
    XmlBeanDefinitionReader beanDefinitionReader = new XmlBeanDefinitionReader(beanFactory);

    // Configure the bean definition reader with this context's
    // resource loading environment.
    beanDefinitionReader.setEnvironment(this.getEnvironment());
    beanDefinitionReader.setResourceLoader(this);
    beanDefinitionReader.setEntityResolver(new ResourceEntityResolver(this));

    // Allow a subclass to provide custom initialization of the reader,
    // then proceed with actually loading the bean definitions.
    initBeanDefinitionReader(beanDefinitionReader);
    //在这里会调用 实现了 NamespaceHandlerSupport  接口的类，执行其 parse 方法
    loadBeanDefinitions(beanDefinitionReader);
}
```

在这里通过 `BeanFactory` ，去加载 `BeanDefinition`

## BeanFactory的预准备工作 - prepareBeanFactory

我们在上一步已经加载了 `BeanFactory` 、 `BeanDefinition`,但距离生产还不是很够
，还需要配置 `context` 的 `ClassLoader` 和后置处理器等。

```java
protected void prepareBeanFactory(ConfigurableListableBeanFactory beanFactory) {
    //设置ClassLoader
    beanFactory.setBeanClassLoader(getClassLoader());

    //设置beanFactory的表达式语言处理器,Spring3开始增加了对语言表达式的支持,
    //      默认可以使用#{bean.xxx}的形式来调用相关属性值
    beanFactory.setBeanExpressionResolver(
        new StandardBeanExpressionResolver(beanFactory.getBeanClassLoader()));
    // 为beanFactory增加一个默认的propertyEditor
    beanFactory.addPropertyEditorRegistrar(
        new ResourceEditorRegistrar(this, getEnvironment()));

    // 添加ApplicationContextAwareProcessor
    // Configure the bean factory with context callbacks.
    beanFactory.addBeanPostProcessor(new ApplicationContextAwareProcessor(this));

    // 设置忽略自动装配的接口
    beanFactory.ignoreDependencyInterface(EnvironmentAware.class);
    beanFactory.ignoreDependencyInterface(EmbeddedValueResolverAware.class);
    beanFactory.ignoreDependencyInterface(ResourceLoaderAware.class);
    beanFactory.ignoreDependencyInterface(ApplicationEventPublisherAware.class);
    beanFactory.ignoreDependencyInterface(MessageSourceAware.class);
    beanFactory.ignoreDependencyInterface(ApplicationContextAware.class);


    // 设置几个自动装配的特殊规则
    // BeanFactory interface not registered as resolvable type in a plain factory.
    // MessageSource registered (and found for autowiring) as a bean.
    beanFactory.registerResolvableDependency(BeanFactory.class, beanFactory);
    beanFactory.registerResolvableDependency(ResourceLoader.class, this);
    beanFactory.registerResolvableDependency(ApplicationEventPublisher.class, this);
    beanFactory.registerResolvableDependency(ApplicationContext.class, this);

    // Register early post-processor for detecting inner beans as ApplicationListeners.
    beanFactory.addBeanPostProcessor(new ApplicationListenerDetector(this));

    // 增加对AspectJ的支持
    // Detect a LoadTimeWeaver and prepare for weaving, if found.
    if (beanFactory.containsBean(LOAD_TIME_WEAVER_BEAN_NAME)) {
        beanFactory.addBeanPostProcessor(
                new LoadTimeWeaverAwareProcessor(beanFactory));
        // Set a temporary ClassLoader for type matching.
        beanFactory.setTempClassLoader(
                new ContextTypeMatchClassLoader(beanFactory.getBeanClassLoader()));
    }

    // 注册默认的系统环境bean
    // Register default environment beans.
    if (!beanFactory.containsLocalBean(ENVIRONMENT_BEAN_NAME)) {
        beanFactory.registerSingleton(ENVIRONMENT_BEAN_NAME, getEnvironment());
    }
    if (!beanFactory.containsLocalBean(SYSTEM_PROPERTIES_BEAN_NAME)) {
        beanFactory.registerSingleton(SYSTEM_PROPERTIES_BEAN_NAME, 
                                    getEnvironment().getSystemProperties());
    }
    if (!beanFactory.containsLocalBean(SYSTEM_ENVIRONMENT_BEAN_NAME)) {
        beanFactory.registerSingleton(SYSTEM_ENVIRONMENT_BEAN_NAME, 
                                    getEnvironment().getSystemEnvironment());
    }
}
```

## BeanFactory准备后的后置处理 - postProcessBeanFactory

```java
// AbstractApplicationContext.java

protected void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) {
    beanFactory.addBeanPostProcessor(
            new ServletContextAwareProcessor(this.servletContext, this.servletConfig));
    beanFactory.ignoreDependencyInterface(ServletContextAware.class);
    beanFactory.ignoreDependencyInterface(ServletConfigAware.class);

    WebApplicationContextUtils
        .registerWebApplicationScopes(beanFactory, this.servletContext);

    WebApplicationContextUtils
        .registerEnvironmentBeans(beanFactory, this.servletContext, this.servletConfig);
}
```

1. 添加 `ServletContextAwareProcessor` 到 `BeanFactory` 容器中，该 `processor` 实现 `BeanPostProcessor` 接口，主要用于将 `ServletContext` 传递给实现了 `ServletContextAware` 接口的 `bean`

1. 忽略 `ServletContextAware、ServletConfigAware`

1. 注册 `WEB` 应用特定的域（scope）到 `beanFactory` 中，以便 `WebApplicationContext` 可以使用它们。比如 `request` , `session` , `globalSession` , `application`

1. 注册 `WEB` 应用特定的 `Environment bean` 到 `beanFactory` `中，以便WebApplicationContext` 可以使用它们。如： `contextParameters`, `contextAttributes`


## 激活BeanFactory处理器 - invokeBeanFactoryPostProcessors

```java
//PostProcessorRegistrationDelegate.invokeBeanFactoryPostProcessors


public static void invokeBeanFactoryPostProcessors(
        ConfigurableListableBeanFactory beanFactory, 
        List<BeanFactoryPostProcessor> beanFactoryPostProcessors) {

    // 定义一个 set 保存所有的 BeanFactoryPostProcessors
    Set<String> processedBeans = new HashSet<>();

    //如果当前 BeanFactory 为 BeanDefinitionRegistry
    if (beanFactory instanceof BeanDefinitionRegistry) {
        BeanDefinitionRegistry registry = (BeanDefinitionRegistry) beanFactory;
        // BeanFactoryPostProcessor 集合
        List<BeanFactoryPostProcessor> regularPostProcessors = new ArrayList<>();
        //BeanDefinitionRegistryPostProcessor 集合
        List<BeanDefinitionRegistryPostProcessor> registryProcessors = new ArrayList<>();

        //迭代注册的 beanFactoryPostProcessors
        for (BeanFactoryPostProcessor postProcessor : beanFactoryPostProcessors) {
            // 如果是 BeanDefinitionRegistryPostProcessor，则调用 postProcessBeanDefinitionRegistry 进行注册，
            // 同时加入到 registryProcessors 集合中
            if (postProcessor instanceof BeanDefinitionRegistryPostProcessor) {
                BeanDefinitionRegistryPostProcessor registryProcessor =
                        (BeanDefinitionRegistryPostProcessor) postProcessor;
                registryProcessor.postProcessBeanDefinitionRegistry(registry);
                registryProcessors.add(registryProcessor);
            }
            else {
                // 否则当做普通的 BeanFactoryPostProcessor 处理
                // 添加到 regularPostProcessors 集合中即可，便于后面做后续处理
                regularPostProcessors.add(postProcessor);
            }
        }

        // 用于保存当前处理的 BeanDefinitionRegistryPostProcessor
        List<BeanDefinitionRegistryPostProcessor> currentRegistryProcessors = new ArrayList<>();

        // 首先处理实现了 PriorityOrdered (有限排序接口)的 BeanDefinitionRegistryPostProcessor
        String[] postProcessorNames =
                beanFactory.getBeanNamesForType(BeanDefinitionRegistryPostProcessor.class, true, false);
        for (String ppName : postProcessorNames) {
            if (beanFactory.isTypeMatch(ppName, PriorityOrdered.class)) {
                currentRegistryProcessors.add(beanFactory.getBean(ppName, BeanDefinitionRegistryPostProcessor.class));
                processedBeans.add(ppName);
            }
        }
        // 排序
        sortPostProcessors(currentRegistryProcessors, beanFactory);
        // 加入registryProcessors集合
        registryProcessors.addAll(currentRegistryProcessors);
        // 调用所有实现了 PriorityOrdered 的 BeanDefinitionRegistryPostProcessors 的 postProcessBeanDefinitionRegistry()
        invokeBeanDefinitionRegistryPostProcessors(currentRegistryProcessors, registry);
        // 清空，以备下次使用
        currentRegistryProcessors.clear();

        // 其次，调用是实现了 Ordered（普通排序接口）的 BeanDefinitionRegistryPostProcessors
		// 逻辑和 上面一样
        postProcessorNames = beanFactory.getBeanNamesForType(BeanDefinitionRegistryPostProcessor.class, true, false);
        for (String ppName : postProcessorNames) {
            if (!processedBeans.contains(ppName) && beanFactory.isTypeMatch(ppName, Ordered.class)) {
                currentRegistryProcessors.add(beanFactory.getBean(ppName, BeanDefinitionRegistryPostProcessor.class));
                processedBeans.add(ppName);
            }
        }
        sortPostProcessors(currentRegistryProcessors, beanFactory);
        registryProcessors.addAll(currentRegistryProcessors);
        invokeBeanDefinitionRegistryPostProcessors(currentRegistryProcessors, registry);
        currentRegistryProcessors.clear();

        // 最后调用其他的 BeanDefinitionRegistryPostProcessors
        boolean reiterate = true;
        while (reiterate) {
            reiterate = false;
            // 获取 BeanDefinitionRegistryPostProcessor
            postProcessorNames = beanFactory.getBeanNamesForType(BeanDefinitionRegistryPostProcessor.class, true, false);
            for (String ppName : postProcessorNames) {
                // 没有包含在 processedBeans 中的（因为包含了的都已经处理了）
                if (!processedBeans.contains(ppName)) {
                    currentRegistryProcessors.add(beanFactory.getBean(ppName, BeanDefinitionRegistryPostProcessor.class));
                    processedBeans.add(ppName);
                    reiterate = true;
                }
            }
            // 与上面处理逻辑一致
            sortPostProcessors(currentRegistryProcessors, beanFactory);
            registryProcessors.addAll(currentRegistryProcessors);
            invokeBeanDefinitionRegistryPostProcessors(currentRegistryProcessors, registry);
            currentRegistryProcessors.clear();
        }

        // 调用所有 BeanDefinitionRegistryPostProcessor (包括手动注册和通过配置文件注册)
        // 和 BeanFactoryPostProcessor(只有手动注册)的回调函数(postProcessBeanFactory())
        invokeBeanFactoryPostProcessors(registryProcessors, beanFactory);
        invokeBeanFactoryPostProcessors(regularPostProcessors, beanFactory);
    }

    else {
        // 如果不是 BeanDefinitionRegistry 只需要调用其回调函数（postProcessBeanFactory()）即可
        invokeBeanFactoryPostProcessors(beanFactoryPostProcessors, beanFactory);
    }

    // Do not initialize FactoryBeans here: We need to leave all regular beans
    // uninitialized to let the bean factory post-processors apply to them!
    String[] postProcessorNames =
            beanFactory.getBeanNamesForType(BeanFactoryPostProcessor.class, true, false);

    // 这里同样需要区分 PriorityOrdered 、Ordered 和 no Ordered
    List<BeanFactoryPostProcessor> priorityOrderedPostProcessors = new ArrayList<>();
    List<String> orderedPostProcessorNames = new ArrayList<>();
    List<String> nonOrderedPostProcessorNames = new ArrayList<>();
    for (String ppName : postProcessorNames) {
        // 已经处理过了的，跳过
        if (processedBeans.contains(ppName)) {
            // skip - already processed in first phase above
        }
        // PriorityOrdered
        else if (beanFactory.isTypeMatch(ppName, PriorityOrdered.class)) {
            priorityOrderedPostProcessors.add(beanFactory.getBean(ppName, BeanFactoryPostProcessor.class));
        }
        // Ordered
        else if (beanFactory.isTypeMatch(ppName, Ordered.class)) {
            orderedPostProcessorNames.add(ppName);
        }
        // no Ordered
        else {
            nonOrderedPostProcessorNames.add(ppName);
        }
    }

    // First, PriorityOrdered 接口
    sortPostProcessors(priorityOrderedPostProcessors, beanFactory);
    invokeBeanFactoryPostProcessors(priorityOrderedPostProcessors, beanFactory);

    // Next, Ordered 接口
    List<BeanFactoryPostProcessor> orderedPostProcessors = new ArrayList<>(orderedPostProcessorNames.size());
    for (String postProcessorName : orderedPostProcessorNames) {
        orderedPostProcessors.add(beanFactory.getBean(postProcessorName, BeanFactoryPostProcessor.class));
    }
    sortPostProcessors(orderedPostProcessors, beanFactory);
    invokeBeanFactoryPostProcessors(orderedPostProcessors, beanFactory);

    // Finally, no ordered
    List<BeanFactoryPostProcessor> nonOrderedPostProcessors = new ArrayList<>(nonOrderedPostProcessorNames.size());
    for (String postProcessorName : nonOrderedPostProcessorNames) {
        nonOrderedPostProcessors.add(beanFactory.getBean(postProcessorName, BeanFactoryPostProcessor.class));
    }
    invokeBeanFactoryPostProcessors(nonOrderedPostProcessors, beanFactory);

    // Clear cached merged bean definitions since the post-processors might have
    // modified the original metadata, e.g. replacing placeholders in values...
    beanFactory.clearMetadataCache();
}
```

上述代码较长，但是处理逻辑较为单一，就是对`BeanDefinitionRegistryPostProcessors`  `BeanFactoryPostProcessor` 按照 `PriorityOrdered` 、 `Ordered`、`no ordered` 三种方式分开处理、调用。

## 注册Bean后置处理器 - registerBeanPostProcessors

注册拦截`Bean`创建的`Bean`处理器，即注册 `BeanPostProcessor`

```java
// AbstractApplicationContext.java

public static void registerBeanPostProcessors(
        ConfigurableListableBeanFactory beanFactory, AbstractApplicationContext applicationContext) {

    // 所有的 BeanPostProcessors
    String[] postProcessorNames = beanFactory.getBeanNamesForType(BeanPostProcessor.class, true, false);

    // 注册 BeanPostProcessorChecker
    // 主要用于记录一些 bean 的信息，这些 bean 不符合所有 BeanPostProcessors 处理的资格时
    int beanProcessorTargetCount = beanFactory.getBeanPostProcessorCount() + 1 + postProcessorNames.length;
    beanFactory.addBeanPostProcessor(new BeanPostProcessorChecker(beanFactory, beanProcessorTargetCount));

    // 区分 PriorityOrdered、Ordered 、 no ordered
    List<BeanPostProcessor> priorityOrderedPostProcessors = new ArrayList<>();
    List<String> orderedPostProcessorNames = new ArrayList<>();
    List<String> nonOrderedPostProcessorNames = new ArrayList<>();
    // MergedBeanDefinition
    List<BeanPostProcessor> internalPostProcessors = new ArrayList<>();
    for (String ppName : postProcessorNames) {
        if (beanFactory.isTypeMatch(ppName, PriorityOrdered.class)) {
            BeanPostProcessor pp = beanFactory.getBean(ppName, BeanPostProcessor.class);
            priorityOrderedPostProcessors.add(pp);
            if (pp instanceof MergedBeanDefinitionPostProcessor) {
                internalPostProcessors.add(pp);
            }
        }
        else if (beanFactory.isTypeMatch(ppName, Ordered.class)) {
            orderedPostProcessorNames.add(ppName);
        }
        else {
            nonOrderedPostProcessorNames.add(ppName);
        }
    }

    // First, PriorityOrdered
    sortPostProcessors(priorityOrderedPostProcessors, beanFactory);
    registerBeanPostProcessors(beanFactory, priorityOrderedPostProcessors);

    // Next, Ordered
    List<BeanPostProcessor> orderedPostProcessors = new ArrayList<>();
    for (String ppName : orderedPostProcessorNames) {
        BeanPostProcessor pp = beanFactory.getBean(ppName, BeanPostProcessor.class);
        orderedPostProcessors.add(pp);
        if (pp instanceof MergedBeanDefinitionPostProcessor) {
            internalPostProcessors.add(pp);
        }
    }
    sortPostProcessors(orderedPostProcessors, beanFactory);
    registerBeanPostProcessors(beanFactory, orderedPostProcessors);

    // onOrdered
    List<BeanPostProcessor> nonOrderedPostProcessors = new ArrayList<>();
    for (String ppName : nonOrderedPostProcessorNames) {
        BeanPostProcessor pp = beanFactory.getBean(ppName, BeanPostProcessor.class);
        nonOrderedPostProcessors.add(pp);
        if (pp instanceof MergedBeanDefinitionPostProcessor) {
            internalPostProcessors.add(pp);
        }
    }
    registerBeanPostProcessors(beanFactory, nonOrderedPostProcessors);

    // Finally, all internal BeanPostProcessors.
    sortPostProcessors(internalPostProcessors, beanFactory);
    registerBeanPostProcessors(beanFactory, internalPostProcessors);

    // 重新注册用来自动探测内部ApplicationListener的post-processor，这样可以将他们移到处理器链条的末尾
    beanFactory.addBeanPostProcessor(new ApplicationListenerDetector(applicationContext));
}
```

## 多语言初始化 - initMessageSource


其实该方法就是初始化一个 MessageSource 接口的实现类，主要用于国际化/i18n。

```java
// AbstractApplicationContext.java

protected void initMessageSource() {
    ConfigurableListableBeanFactory beanFactory = getBeanFactory();
    // 包含 “messageSource” bean
    if (beanFactory.containsLocalBean(MESSAGE_SOURCE_BEAN_NAME)) {
        this.messageSource = beanFactory.getBean(MESSAGE_SOURCE_BEAN_NAME, MessageSource.class);
        // 如果有父类
        // HierarchicalMessageSource 分级处理的 MessageSource
        if (this.parent != null && this.messageSource instanceof HierarchicalMessageSource) {
            HierarchicalMessageSource hms = (HierarchicalMessageSource) this.messageSource;
            if (hms.getParentMessageSource() == null) {
                // 如果没有注册父 MessageSource，则设置为父类上下文的的 MessageSource
                hms.setParentMessageSource(getInternalParentMessageSource());
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Using MessageSource [" + this.messageSource + "]");
        }
    }
    else {
        // 使用 空 MessageSource
        DelegatingMessageSource dms = new DelegatingMessageSource();
        dms.setParentMessageSource(getInternalParentMessageSource());
        this.messageSource = dms;
        beanFactory.registerSingleton(MESSAGE_SOURCE_BEAN_NAME, this.messageSource);
        if (logger.isDebugEnabled()) {
            logger.debug("Unable to locate MessageSource with name '" + MESSAGE_SOURCE_BEAN_NAME +
                    "': using default [" + this.messageSource + "]");
        }
    }
}
```

## 初始化事件广播器 - initApplicationEventMulticaster

```java
// AbstractApplicationContext.java

protected void initApplicationEventMulticaster() {
    ConfigurableListableBeanFactory beanFactory = getBeanFactory();

    // 如果存在 applicationEventMulticaster bean，则获取赋值
    if (beanFactory.containsLocalBean(APPLICATION_EVENT_MULTICASTER_BEAN_NAME)) {
        this.applicationEventMulticaster =
                beanFactory.getBean(APPLICATION_EVENT_MULTICASTER_BEAN_NAME, ApplicationEventMulticaster.class);
        if (logger.isDebugEnabled()) {
            logger.debug("Using ApplicationEventMulticaster [" + this.applicationEventMulticaster + "]");
        }
    }
    else {
        // 没有则新建 SimpleApplicationEventMulticaster，并完成 bean 的注册
        this.applicationEventMulticaster = new SimpleApplicationEventMulticaster(beanFactory);
        beanFactory.registerSingleton(APPLICATION_EVENT_MULTICASTER_BEAN_NAME, this.applicationEventMulticaster);
        if (logger.isDebugEnabled()) {
            logger.debug("Unable to locate ApplicationEventMulticaster with name '" +
                    APPLICATION_EVENT_MULTICASTER_BEAN_NAME +
                    "': using default [" + this.applicationEventMulticaster + "]");
        }
    }
}
```

如果当前容器中存在 `applicationEventMulticaster` 的 `bean`，则对 `applicationEventMulticaster` 赋值，否则新建一个 `SimpleApplicationEventMulticaster` 的对象（默认的），并完成注册。

## onRefresh

预留给 `AbstractApplicationContext` 的子类用于初始化其他特殊的 bean，该方法需要在所有单例 bean 初始化之前调用。

## 注册监听器 - registerListeners

在所有 `bean` 中查找` listener bean`，然后注册到广播器中

```java
// AbstractApplicationContext.java

protected void registerListeners() {
    // 注册静态 监听器
    for (ApplicationListener<?> listener : getApplicationListeners()) {
        getApplicationEventMulticaster().addApplicationListener(listener);
    }

    String[] listenerBeanNames = getBeanNamesForType(ApplicationListener.class, true, false);
    for (String listenerBeanName : listenerBeanNames) {
        getApplicationEventMulticaster().addApplicationListenerBean(listenerBeanName);
    }

    // 至此，已经完成将监听器注册到ApplicationEventMulticaster中，下面将发布前期的事件给监听器。
    Set<ApplicationEvent> earlyEventsToProcess = this.earlyApplicationEvents;
    this.earlyApplicationEvents = null;
    if (earlyEventsToProcess != null) {
        for (ApplicationEvent earlyEvent : earlyEventsToProcess) {
            getApplicationEventMulticaster().multicastEvent(earlyEvent);
        }
    }
}
```


## 初始化所有单例Bean - finishBeanFactoryInitialization

初始化剩下的单例Bean(非延迟加载的)
```java
// AbstractApplicationContext.java

protected void finishBeanFactoryInitialization(ConfigurableListableBeanFactory beanFactory) {
    // 初始化转换器
    if (beanFactory.containsBean(CONVERSION_SERVICE_BEAN_NAME) &&
            beanFactory.isTypeMatch(CONVERSION_SERVICE_BEAN_NAME, ConversionService.class)) {
        beanFactory.setConversionService(
                beanFactory.getBean(CONVERSION_SERVICE_BEAN_NAME, ConversionService.class));
    }

    // 如果之前没有注册 bean 后置处理器（例如PropertyPlaceholderConfigurer），则注册默认的解析器
    if (!beanFactory.hasEmbeddedValueResolver()) {
        beanFactory.addEmbeddedValueResolver(strVal -> getEnvironment().resolvePlaceholders(strVal));
    }

    // 初始化 Initialize LoadTimeWeaverAware beans early to allow for registering their transformers early.
    String[] weaverAwareNames = beanFactory.getBeanNamesForType(LoadTimeWeaverAware.class, false, false);
    for (String weaverAwareName : weaverAwareNames) {
        getBean(weaverAwareName);
    }

    // 停止使用临时的 ClassLoader
    beanFactory.setTempClassLoader(null);

    //
    beanFactory.freezeConfiguration();

    // 初始化所有剩余的单例（非延迟初始化）
    beanFactory.preInstantiateSingletons();
}
```

## 完成刷新 - finishRefresh

主要是调用 `LifecycleProcessor#onRefresh()` ，并且发布事件`（ContextRefreshedEvent）`。

```java
// AbstractApplicationContext.java

protected void finishRefresh() {
    // Clear context-level resource caches (such as ASM metadata from scanning).
    clearResourceCaches();

    // Initialize lifecycle processor for this context.
    initLifecycleProcessor();

    // Propagate refresh to lifecycle processor first.
    getLifecycleProcessor().onRefresh();

    // Publish the final event.
    publishEvent(new ContextRefreshedEvent(this));

    // Participate in LiveBeansView MBean, if active.
    LiveBeansView.registerApplicationContext(this);
}
```

# 总结

![112320445768_0refresh(1)_1](http://qiniu.liulei.life/112320445768_0refresh(1)_1.jpg)