
# IoC与DI


IoC(Inversion Control)控制反转：所谓控制反转，就是把我们代码中需要 new 的对象，交给容器进行管理。

DI(Dependency Injection)：依赖注入，就是指，被动接收依赖类，而不是自己创建依赖。也就是说从容器中查找依赖的类，并赋值/注入。


接下来我们如果自己设计一个IoC容器完成DI功能，那么我们应该怎么考虑呢？


# 实现IoC容器需要解决的问题

## 对象间的关系如何表示？

如果A对象依赖B对象，因为现在不允许手动创建对象，那么我们如何完成A、B两个类的功能呢？

我们可以使用 XML、Properties等配置文件进行表示。


## 描述对象关系的文件放在何种路径下？



classPath、network、filesystem、ServletContext。。。。

## 有着不一样的配置文件如何统一

在内部需要一个统一的对象定义，将所有外部的配置转化为内部的定义

## 如何加载不同的配置文件

针对与不同的配置文件，采用不同的解析器，使用策略模式进行加载。

## 对象的创建

使用反射+工厂模式创建对象






# SpringIoc的五大组件体系

- Resource体系：对一切资源的抽象，如不同的资源采用不同的加载策略

- BeanDefition体系：用来描述Spring中的Bean对象

- BeandefinitionReader：它能够读取Spring配置文件的内容，并转换为BeanDefition

- BeanFactory体系：内部维护着一个BeanDefition Map，它能根据 BeanDefition，对Bean进行创建和管理

- ApplicationContext体系：这个就是Spring 容器，它继承自 BeanFactory 并扩展了 BeanFactory的功能。

我们下面对着五大体系进行简要说明

## Resource体系

`Resource`接口位于 `org.springframework.core.io` 包下。它是对资源的抽象，它的每一个类都代表了一种资源的访问策略。

<img src="../../../../java-notes/img/Resource.jpg">

主要常用的有如下：

- ClassPathResource：类文件资源

- FileSystemResource：文件系统资源

- UrlResource：url资源

- ByteArrayResource：字节数组资源

- ServletContextResource：访问相对于ServletContext路径下的资源的实现类，如JSP

- InputStreamResource：输入流资源

### ResourceLoader体系
 
 有了统一的资源抽象，那就就应该有资源加载，Spring 利用`org.springframework.core.io`包下的 `ResourceLoader` 

 <img src="../../../../java-notes/img/ResourceLoader体系.jpg">

 - 类文件加载器

 - 系统文件加载器

 - Ant风格的路径匹配加载器(类似于 **/*.xml)。

## BeanDefinition体系

`BeanDefinition` 位于 `org.springframework.beans.factory.config` 包下，用来描述 Spring 中的 bean的元信息。

<img src="../../../../java-notes/img/BeanDefinition体系.jpg">

## BeanDefinitionReader体系

`BeanDefinitionReader` 位于 `org.springframework.beans.factory.support` 包下，它是用来读取配置文件、注解，并将其转化为IoC内部的数据结构： `BeanDefinition`

<img src="../../../../java-notes/img/BeanDefinitionReader.jpg">

## BeanFactory体系

`BeanFactory` 位于 `org.springframework.beans.factory` 包下，是一个纯粹的`bean容器`,它是Ioc必备的数据结构。其中 `beanFactroy` 内部维护着一个 `BeanDefinition` 类型的Map，并根据 `BeanDefinition` 进行 `bean`的创建和管理.

<img src="../../../../java-notes/img/BeanFactory 体系.jpg">

- BeanFactory 有三个直接子类 

    - ListableBeanFactory、
    
    - HierarchicalBeanFactory 
    
    - AutowireCapableBeanFactory


- DefaultListableBeanFactory 为最终默认实现，它实现了所有接口。

## ApplicationContext 体系

ApplicationContext 位于 `org.springframework.context` 包中，它就是 `Spring` 容器，它叫做应用上下文。它继承了以下接口来提供服务。

1. 继承 `BeanFactory` 接口，提供Bean创建以及管理等功能。

1. 继承 `MessageSource` 接口，提供了国际化标准访问策略

1. 继承 `ApplicationEventPublisher` 接口，提供事件机制。

1. 扩展 `ResourLoader` 可以用来加载各种资源。

1. 对Web的支持

<img src="../../../../java-notes/img/ApplicationContext.jpg">


## 小结

上面五个体系是 Spring 最核心的部分。

我们可以这样理解：

1. `ResourceLoader` 与 `BeanDefinitionReader` 将资源加载为 `Resource` 

1. `BeanFactory` 解析 `beanDefinition` 并实例化并存储到一个Map中，然后将实例注入到需要这些实例的地方。

1. 而 `ApplicationContext` 就是实现以上功能的那个类。