
# Spring启动流程

Spring 启动时，会以某种方式加载元数据，并将其解析注册到容器内部，然后根据这些信息绑定整个系统的对象。最终组成一个轻量级的应用系统。

Spring 在实现以上功能时，分为两个阶段：容器初始化阶段和加载bean阶段

1. **容器初始化阶段：**

    - 首先，加载元数据，使用 `Resource` 和 `ResourceLoader` 体系

    - 然后，容器会对元数据进行分解和分析，并将分析的信息组成 `BeanDefition`

    - 最后，将 `BeanDefition` 保存注册到相应的 BeanDefitionRegisty中。至此Spring IoC初始化完成

1. **加载Bean阶段：**

    - 当我们显式或者隐式调用 `BeanFactory.getBean(..)` 时，则会触发`Bean`的加载

    - 当 `Bean` 开始加载时,首先会检查 Bean 是否在容器中有注册过(缓存)，如果没有则会依据 `beanDefinition` 实例化 `Bean`,并将其注册到容器中，随后返回。



# Bean加载流程

Bean加载流程如下

## 调用getBean

当我们显式或隐式的调用`getBean(String name)` 时，会触发 Bean 的加载。

我们传入 `Bean` 名称，向容器中索取 `Bean`。
```java
@Override
public Object getBean(String name) throws BeansException {
    return doGetBean(name, null, null, false);
}         
```

## doGetBean

在 `getBean` 中 我们直接调用了 `doGetBean` 方法

### 获取Bean在容器中的名称

由于我们的 `name` 参数，可能是 `beanName`、`aliasName` 、FactoryBean，所以我们在这里对其进行转换.

### 从单例的Bean缓存中获取Bean

单例的`Bean` 在 `Spring` 中只会被创建一次，第一次创建后会将该Bean加载到缓存中，后面获取Bean则会直接从缓存中获取。


### 原型模式依赖检查

当 A、B两个Bean都是原型模式(多例)时，Spring将直接抛出异常。

- 对于单例模式。Spring在创建Bean时，并不是等bean完全创建好后，才将bean添加至缓存中，而是提前将创建bean 的 ObjectFactory 添加到缓存中，这样一旦下一个bean 创建的时候，需要依赖直接使用ObjectFactory

 - 但是原型模式，是无法使用缓存的，所以Spring并不处理，原型模式的循环依赖

 ### 从parentBeanFactory获取Bean

当不满足以下两个条件时，从parentBeanFactory获取Bean

 1. 缓存单例不为空 

 1. 创建参数不为空

当前容器缓存中如果没有对应的BeanDefition对象，则尝试从父类工厂 parentbeanFactory 加载，然后再去递归调用getBean方法

### 指定的 Bean 标记为创建状态

当获取的parentBeanFactory 为null，或者已从容器中找到beanDefition时，**标记Bean为创建状态**

### 获取beanDefition

根据BeanName  从容器中获取对应的 GenericBeanDefinition 对象，并将其转换为 RootBeanDefinition 对象

随后检查 RootBeanDefinition 对象


### 依赖Bean处理

注意此处说的是强依赖 - 构造依赖

- 每个Bean 都不是单独工作的，当bean有可能依赖别的Bean，也有可能被别的bean所依赖

- 当Bean依赖别的Bean时，被依赖的bean会优先加载，在Spring加载时，初始化某个bean时，如果该Bean依赖其他Bean，则会先初始化该bean的依赖

### 实例化化不同作用域的Bean

- Spring Bean的作用域默认为 单例，

- 对于不同的作用域会有不同的初始化策略

### 类型转换
- 在调用 #doGetBean(...) 方法时，有一个 requiredType 参数。该参数的功能就是将返回的 Bean 转换为 requiredType 类型。

- 当然就一般而言，我们是不需要进行类型转换的，也就是 requiredType 为空（比如 #getBean(String name) 方法）。但有，可能会存在这种情况，比如我们返回的 Bean 类型为 String ，我们在使用的时候需要将其转换为 Integer，那么这个时候 requiredType 就有用武之地了。当然我们一般是不需要这样做的。

# Bean加载总结


入口 `AbstractBeanFactory.getBean(String name)`,随后调用同一个类中的`doGetBean`方法，开启`Bean`的加载

1. 从缓存中获取单例 Bean 

    1. `AbstractBeanFactory`了调用父类方法,`DefaultSingletonBeanRegistry.getSingleton(String beanName)`，从储存Bean的三级Map容器中获取bean



1. 如果bean未创建，并且有父工厂，则从 父工厂 获取 `Bean` 

    如果该类有父工厂，并且容器中不存在该类的`beanDefition`，则开启加载

    1. 如果，父类容器为 AbstractBeanFactory ，直接递归查找
        ```java
        //是否是AbstractBeanFactory 的子类
        if (parentBeanFactory instanceof AbstractBeanFactory) {
            //递归查找
            return ((AbstractBeanFactory) parentBeanFactory).doGetBean(
                nameToLookup, requiredType, args, typeCheckOnly);
        }
        ```
    1. 当构造参数不为空时，使用特定的参数初始化 `Bean`

        ```java
        else if (args != null) {
            //使用特定的参数初始化Bean
            return (T) parentBeanFactory.getBean(nameToLookup, args);
        }
        ```
    1. 直接使用 名称查找 从 parentBeanFactory 获取 Bean 对象
        ```java
        else {
            return (T) parentBeanFactory.getBean(nameToLookup);
        }
        ```
            

1. 指定的 Bean 标记为已经创建或即将创建
    ```java
        if (!typeCheckOnly) {
        markBeanAsCreated(beanName);
    }
    ```
1. 获取 `BeanDefition` ，并将其转换为 `RootBeanDefinition`，这是因为不同的配置文件加载的 `BeanDefition` 有所不同

    ```java
    //获取BeanDefition
    final RootBeanDefinition mbd = getMergedLocalBeanDefinition(beanName);
    //检查beanDefition的合法性
    checkMergedBeanDefinition(mbd, beanName, args);
    ```
1. 依赖 Bean 处理
    ```java
    //从Bean定义中获取依赖
    String[] dependsOn = mbd.getDependsOn();
    //遍历依赖
    if (dependsOn != null) {
        for (String dep : dependsOn) {
            //检测依赖是否循环
            if (isDependent(beanName, dep)) {
                throw new BeanCreationException(mbd.getResourceDescription(), 
                        beanName,
                        "Circular depends-on relationship between '" 
                            + beanName + "' and '" + dep + "'");
            }
            //注册依赖
            registerDependentBean(dep, beanName);
            //如果依赖未创建则创建依赖
            getBean(dep);
        }
    }
    ```
    每个 Bean 都不是单独工作的，它会依赖其他 Bean，其他 Bean 也会依赖它。
    对于依赖的 Bean ，它会优先加载，所以，在 Spring 的加载顺序中，在初始化某一个 Bean 的时候，首先会初始化这个 Bean 的依赖。

1. 创建各个作用域的 `bean`，单例模式、原型模式、指定scope的作用域

    Spring Bean 的作用域默认为 singleton 。还有其他作用域，如 prototype、request、session 等。不同的作用域有不同的初始化策略。

    除了上面使用父工厂创建 `Bean` ，之外，这一步也是创建 `Bean` 的，我们将在下一篇文章讲述



1. 类型转换 - 将bean类型转换为需要的类型

