

# Redis 数据类型

redis 自身是一个 Map ，采用 key value 形式存储。

![](https://gitee.com/superwow/pic/raw/master/img/20210328160147.png)

- string -> String

- hash -> HashMap

- list -> LinkedList

- set -> hashSet

- sorted_set -> TreeSet



## String

- 存储的数据：单个数据，最贱的数据存储类型，也是最常用的数据存储类型

- 存储数据的格式：一个存储空间保存一个数据

- 存储内容：通常使用字符串，如果字符串以整数的形式展示，可以作为数字操作使用

![](https://gitee.com/superwow/pic/raw/master/img/20210328160400.png)

### 操作

- 添加修改

    > set key value

- 获取

    > get key

- 删除数据

    > del key

- 添加修改多个数据

    > mset key1 value  key2  value

- 获取多个数据

    > mget key1 key2 

- 获取数据字符个数（字符串长度）

    > strlen key


- 追加信息

    > append key value

### 扩展操作


- 设置数值数据增加指定范围的值

    ```
    incr key
    incrby key increment
    incrbyfloat key increment
    ```

- 设置数值数据减少指定范围的值

    ```
    decr key
    decrby key increment
    ```
注意：按数值进行操作的数据，如果原始数据不能转成数值，或超过了redis数值上线范围，将会报错。9223372036854775807 (java中long型数据最大值，Long.MAX_VALUE)

### String 数据时效性设置

- 设置数据具有指定的声明周期
    ```
    setex key seconds value
    psetex key milliseconds value
    ```

### String类型应用场景

主页高频访问信息显示控制，例如新浪微博大V主页显示粉丝数与微博数量

## Hash

- 新的存储需求：对一系列存储的数据进行编组，方便管理，典型应用存储对象信息
- 需要的内存结构：一个存储空间保存多少个键值对数据
- hash类型：底层使用哈希表结构实现数据存储

![](https://gitee.com/superwow/pic/raw/master/img/20210328213446.png)

## hash类型数据的基本操作

- 添加/修改数据
    > hset key field value

- 获取数据

    ```
    hget key field
    hgetall key
    ```

- 删除数据

    ```
    hdel key field1 [field2]
    ```

- 添加/修改多个数据

    ```
    hmset key field1 value1 field2 calue2
    ```

## 应用场景

即某样事物有多个属性

- 购物车

## list类型

- 数据存储需求：存储多个数据，并对数据进入存储空间的顺序进行区分

- 需要的存储数据：一个存储空间保存多个数据，且通过数据可以体现进入顺序

- list类型：保存多个数据，底层使用双向链表存储结构实现

![](https://gitee.com/superwow/pic/raw/master/img/20210328214037.png)

### 操作-todo

### 业务场景

- twitter、新浪微博、腾讯微博中个人用于的关注列表需要按照用户的关注顺序进行展示，粉丝列表需要将最近关注的粉丝列在前面

- 新闻、资讯类网站如何将最新的新闻或资讯按照发生的事件顺序展示

- 企业运营过程中，系统将产生出大量的运营数据，如何保障多台服务器操作日志的统一顺序输出？

解决方案

- 依赖list的数据具有顺序的特征对信息进行管理

- 使用队列模型解决多路信息汇总合并的问题

- 使用栈模型解决最新消息的问题



## Set类型

- 新的存储需求：存储大量的数据，在查询方面提供更高的效率
- 需要的存储结构：能够保存大量的数据，搞笑的内部存储机制，便于查询
- set类型：与hash存储结构完全相同，仅存储键，不存储值（nil),并且值式不允许重复的

### 业务场景

![](https://gitee.com/superwow/pic/raw/master/img/20210328214448.png)


解决方案

- 求两个集合的交、并、差集
    ```
    sinter key1 [key2]
    sunion key1 [key2]
    sdiff key1 [key2]
    ```

- 求两个集合的交、并、差集并存储到指定集合中
    ```
    sinterstore destination key1 [key2]
    sunionstore destination key1 [key2]
    sdiffstore destination key1 [key2]
    ```

- 将指定数据从原始集合移动到目标集合中
    ```
    smove source destination member
    ```

## sorted_set


- 新的存储需求：根据排序有利于数据的有效显示，需要提供一种可以根据自身特征进行排序的方式
- 需要的存储结构：新的存储模型，可以保存可排序的数据
- sorted_set类型：在set的存储结构基础上添加可排序字段


![](https://gitee.com/superwow/pic/raw/master/img/20210328215746.png)

### 业务场景

![](https://gitee.com/superwow/pic/raw/master/img/20210328215850.png)