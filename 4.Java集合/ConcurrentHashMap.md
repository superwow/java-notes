# 概述
`ConcurrentHashMap` 是一种线程安全的散列表，它位于 `java.util.concurrent`包。

# 内部实现

1.7版本采用了 `segment` + `HashEntry`的方式实现，使用分段锁来保证线程安全。

1.8版本则放弃了分段锁的设计，使用了 数组 + `CAS` + `synchronized` 来保证线程安全.


## 1.7

### 储存结构
<img src="../../java-notes/img/concurrenthashmap-1.7.jpg">

它是由 `segment` 数组，与 `HashEntry` 组成的，同 `HashMap`一样同样都是数组+链表。
```
//Segment 数组，存放数据时首先需要定位到具体的 Segment 中。
final Segment<K,V>[] segments;
transient Set<K> keySet;
transient Set<Map.Entry<K,V>> entrySet;
```
`segment` 是 `concurrentHashMap` 的一个内部类 结构如下
```
static final class Segment<K,V> extends ReentrantLock implements Serializable {

    private static final long serialVersionUID = 2249069246763182397L;
    
    // 和 HashMap 中的 HashEntry 作用一样，真正存放数据的桶
    transient volatile HashEntry<K,V>[] table;

    transient int count;

    transient int modCount;

    transient int threshold;

    final float loadFactor;
}
```

再来看 `HashEntry`

```
static final class HashEntry<K,V> {
    final int hash;
    final K key;
    volatile V value;
    volatile HashEntry<K,V> next;
}
```
和 HashMap 非常类似，唯一的区别就是其中的核心数据如 value ，以及链表都是 volatile 修饰的，保证了获取时的可见性。

原理上来说：`ConcurrentHashMap` 采用了分段锁技术，其中` Segment` 继承于 `ReentrantLock`。不会像 `HashTable` 那样不管是 `put` 还是 `get` 操作都需要做同步处理，理论上 `ConcurrentHashMap` 支持 `CurrencyLevel` (Segment 数组数量)的线程并发。每当一个线程占用锁访问一个 `Segment` 时，不会影响到其他的 `Segment。`

### 添加元素

1. 根据key定位到对应的`segment`，并调用 `segment`对应的`put`方法

    ```
     public V put(K key, V value) {
        Segment<K,V> s;
        if (value == null)
            throw new NullPointerException();
        int hash = hash(key);
        int j = (hash >>> segmentShift) & segmentMask;
        if ((s = (Segment<K,V>)UNSAFE.getObject          // nonvolatile; recheck
             (segments, (j << SSHIFT) + SBASE)) == null) //  in ensureSegment
            s = ensureSegment(j);
        return s.put(key, hash, value, false);
    }
    ```

1. 使用自旋获取`segment`内部的锁

    在put方法内调用scanAndLock方法。

    <img src="../../java-notes/img/concurrenthashMap-putToscanAndLock.png">

    scanAndLockForPut方法，使用自旋方式获取锁，如果自旋达到最大次数则使用阻塞方式，能保证获取到锁，并根据key定位hashEntry节点，如果没有则返回null

    <img src="../../java-notes/img/concurrenthashmap-1.7-put-scanAndLockForPut.jpg">

1. 存储键值对

    1. 遍历hashEntry数组，如果找到对应的节点，则覆盖旧值。并将旧值赋予oldvalue

    <img src="../../java-notes/img/concurrenthashmap-put-1.png">

    1. 如果没有在链表上找到对应节点，则新建节点，并将其加入到链表中。检查是否需要扩容

    <img src="../../java-notes/img/concurrenthashmap-put-2.png">

1. 解锁，并返回旧值

### 获取一个元素

get 逻辑比较简单：

只需要将 Key 通过 Hash 之后定位到具体的 Segment ，再通过一次 Hash 定位到具体的元素上。

由于 HashEntry 中的 value 属性是用 volatile 关键词修饰的，保证了内存可见性，所以每次获取时都是最新值。

ConcurrentHashMap 的 get 方法是非常高效的，因为整个过程都不需要加锁。

### 扩容机制

### 移除机制


## 1.8 

1.7版本虽然较好的解决了并发问题但是在链表过长时，遍历效率太低，所以1.8版本改为数组 + 链表 + 红黑树。这点与1.8版本的hashMap 非常相似。


### 储存结构

1.8版本采用了Node数组来代替segment数组，并且使用 volatile 修饰，保证可见性。

```
transient volatile Node<K,V>[] table;

private transient volatile Node<K,V>[] nextTable;
。。。
```
node 结构，也可以看到 val 和next节点都使用了volatile修饰，也保证了可见性。
```
static class Node<K,V> implements Map.Entry<K,V> {
    final int hash;
    final K key;
    volatile V val;
    volatile Node<K,V> next;
}
```

### 添加元素

查看下图，将添加元素分为6步。

<img src="../../java-notes/img/concurrenthashMap-1.8-put.jpg">

1. 计算key的hash值

    <img src="../../java-notes/img/concurrenthashMap-1.8-put-1.jpg">

1. 判断数组是否需要初始化

    <img src="../../java-notes/img/concurrenthashMap-1.8-put-2.jpg">

1. 定位数组中的节点`f`，如果为空表示可以写入数据，利用`CAS`尝试写入，失败则自旋保证成功

    <img src="../../java-notes/img/concurrenthashMap-1.8-put-3.jpg">

1. 如果当前位置的 `hashcode == MOVED == -1`,则需要进行扩容。

    <img src="../../java-notes/img/concurrenthashMap-1.8-put-4.jpg">

1. 如果都不满足，则利用 synchronized 锁写入数据。

    链表写入

    <img src="../../java-notes/img/concurrenthashmap-1.8-put-5.jpg">

    红黑树写入

    <img src="../../java-notes/img/concurrenthashmap-1.8-put-6.jpg">

1. 如果数量大于 TREEIFY_THRESHOLD 则要转换为红黑树，如果存在旧值则返回旧值，如果不存在则跳出循环。然后检查是否需要扩容

    <img src="../../java-notes/img/concurrenthashmap-1.8-put-7.jpg">

### 扩容





# 面试题



