# 概述

`ArrayList`，是基于**数组**实现的，支持**扩容**的动态数组.




# 使用场景

1. `ArrayList` 底层基于数组实现，所以查询操作效率会比较高。

2. 删除和插入性能较差，因为要移动数据（可以使用`LinkindList`）

3. 不适合在多线程情况下使用，在多线程情况下可以使用`CopyOnWriteList`

# ArrayList源码



## 类图
ArrayList 实现的接口、继承的抽象类，如下图所示

<img src="../../java-notes/img/ArrayList-架构图.png">

实现了四个接口,分别是

- `java.util.List` 接口，提供数组的添加、删除、修改、迭代遍历等操作

- `java.util.RandomAccess` 接口，表示 ArrayList 支持快速的随机访问。

    RandomAccess: <https://juejin.im/post/5a26134af265da43085de060>

- `java.io.Serializable` 接口，表示 ArrayList 支持序列化的功能。

    序列化与反序列化：<https://www.jianshu.com/p/e554c787c286>

- `java.lang.Cloneable` 接口，表示 ArrayList 支持克隆。

    克隆： <https://www.zhihu.com/question/52490586/answer/130786763>
    
继承了 `java.util.AbstractList` 抽象类，而 `AbstractList` 提供了 `List` 接口的骨架实现，大幅度的减少了实现迭代遍历相关操作的代码。可能这样表述有点抽象，胖友点到 `java.util.AbstractList` 抽象类中看看，例如说 `iterator()`、`indexOf(Object o)` 等方法。

 不过实际上，在下面中我们会看到，`ArrayList` 大量重写了 `AbstractList` 提供的方法实现。所以，`AbstractList` 对于 `ArrayList` 意义不大，更多的是 `AbstractList` 其它子类享受了这个福利。


## 属性

```
//该属性是用来储存数据。
//1. 当添加新的元素时，如果该数组不够，会创建新数组，。
//2. 并将原数组的元素拷贝到新数组，将该变量指向新数组。
/**
* The array buffer into which the elements of the ArrayList are stored.
* The capacity of the ArrayList is the length of this array buffer. Any
* empty ArrayList with elementData == DEFAULTCAPACITY_EMPTY_ELEMENTDATA
* will be expanded to DEFAULT_CAPACITY when the first element is added.
*/
transient Object[] elementData; // non-private to simplify nested class access


//当前已使用的容量大小
/**
* The size of the ArrayList (the number of elements it contains).
*
* @serial
*/
private int size;
```

- `elementData` 属性：元素数组。

- `size` 属性：已使用的数组大小。


## 添加一个元素

`add(E e)` 方法，顺序添加单个元素到数组

```
public boolean add(E e) {
    //确保有足够的容量来存储数据 - 如果检测到容量不足就会扩容
    ensureCapacityInternal(size + 1);  // Increments modCount!!
    //在数组的size位置添加新值,并将size++
    elementData[size++] = e;
    return true;
}
```

## 扩容
通过上面的`add(E e)` 方法，我们发现`ArrayList`使用 `void ensureCapacityInternal(int minCapacity)`方法进行扩容，代码如下：

```
private void ensureCapacityInternal(int minCapacity) {
    //如果数组等于默认的空数组
    if (elementData == DEFAULTCAPACITY_EMPTY_ELEMENTDATA) {
        //从默认容量10与传入容量取出较大的作为数组的容量
        minCapacity = Math.max(DEFAULT_CAPACITY, minCapacity);
    }
    //确保有足够容量
    ensureExplicitCapacity(minCapacity);
}
```
接下来查看 `void ensureExplicitCapacity(int minCapacity) `方法源码：

```
private void ensureExplicitCapacity(int minCapacity) {
    //将修改次数+1
    //该属性定义在父类 AbstractList 上，用于记录数组修改次数。
    modCount++;

    //如果要扩容的容量等于当前容量则什么也不做直接添加元素即可
    //如果要扩容的容量大于原有容量则扩容
    // overflow-conscious code
    if (minCapacity - elementData.length > 0)
        grow(minCapacity);
}
```
接下来我们来到了真正的扩容方法`void grow(int minCapacity) `:

```
private void grow(int minCapacity) {

    //1. 取出原先数组容量
    // overflow-conscious code
    int oldCapacity = elementData.length;
    //2. 原数组容量+原数组容量/2 (右移一位等于除以2)
    int newCapacity = oldCapacity + (oldCapacity >> 1);
    //3. 确保容量不低于原容量
    if (newCapacity - minCapacity < 0)
        newCapacity = minCapacity;
    //4. 确保容量不大于int最大值
    if (newCapacity - MAX_ARRAY_SIZE > 0)
        newCapacity = hugeCapacity(minCapacity);
    //创建新数组并将原数组内容复制到新数组，扩容结束。
    // minCapacity is usually close to size, so this is a win:
    elementData = Arrays.copyOf(elementData, newCapacity);
}
```

# ArrayList的使用


## 去重

### String去重

1. **不推荐过于啰嗦** - 利用 `set` 互异性(不重复)去重，不改变原有的顺序。  
    ```
    public static List listDuplicate1(List<String> list) {
        System.out.println("list = [" + list.toString() + "]");
        List<String> listNew = new ArrayList<>();
        Set set = new HashSet();
        for (String str : list) {
            if (set.add(str)) {
                listNew.add(str);
            }
        }
        return listNew;
    }
    ```

2. **推荐** - 使用 `set` 与 `ArrayList` 构造函数将去重。  
    除 `HashSet` 外也可使用其他 `set` 来实现
    ```
    public static List listDuplicate2(List<String> list) {
        List<String> listNew = new ArrayList<>(new HashSet(list));
        return listNew;
    }
    ```
3. **需要排序时使用** - 使用 `TreeSet` 与 `ArrayList` 构造函数,去重并自然排序。  
    如果需要其他排序方式,需要重写 `TreeSet` 的 `int compare(Order a, Order b) ` 方法
    ```
    public static List listDuplicate3(List<String> list) {
        List<String> listNew = new ArrayList<>(new TreeSet<String>(list));
        return listNew;
    }
    ```
4. **不推荐** - 使用 `ArrayList` 函数 `contains(Object o)` 去重。  
    `contains` 内部使用遍历来确定数据是否存在。所以此方式仍然是**双重遍历法**
    ```
    public static List listDuplicate4(List<String> list) {
        List<String> listNew = new ArrayList<>();
        for (String str : list) {
            if (!listNew.contains(str)) {
                listNew.add(str);
            }
        }
        return listNew;
    }
    ```
5. **推荐在线程数较多情况下使用**
    使用`java8` 的 `Stream`去重。
    ```
    List newList = list.stream().distinct().collect(Collectors.toList());
    ```



### 对象去重


实体对象
```
class User {
    private int id;//省略构造函数和get、set方法
    //省略:equals、HashCode、toString方法
    //注: equals 和 HashCode 方法可以使用 lombok 的 @EqualsAndHashCode 来实现
}
```
对象集合
```
List<User> list = Arrays.asList(new User(1), 
                                new User(1),
                                new User(1), 
                                new User(3), 
                                new User(4),
                                new User(3), 
                                new User(4));
```

1. **推荐** - 使用 `Set` 去重，需要对象实现 `equals` 和 `HashCode` 方法
    ```
    List<User> unique = new ArrayList<User>(new HashSet<User>(list));
    ```
2. **推荐** - 使用 `java8` 的 `Stream` 的 `distinct` 方法，同样需要实现 `equals` 和 `HashCode` 方法
    ```
    List<User> unique = list.stream().distinct().collect(Collectors.toList());
    ```

### 根据对象字段去重 

实体对象

```
class A {
    String a;
    String b;
}
```

1. **不推荐**：
    ```
    List b = a.stream().collect(
        Collectors.collectingAndThen(
                Collectors.toCollection(() -> 
                    new TreeSet<>(Comparator.comparing(A::getA))),
                ArrayList::new));
    ```

1. **推荐***：
    ```
    private static List<A> getDistinctInfo(List<A> list) {
        Set<A> set = new TreeSet<>(new Comparator<A>() {
            @Override
            public int compare(A o1, A o2) {
                int compareToResult = 1;//==0表示重复
                if(o1.getA().equals( o2.getA())) {
                    compareToResult = 0;
                }
                return compareToResult;
            }
        });
        set.addAll(list);
        return new ArrayList<>(set);
    }
    ```


## 排序


实体对象
```
@EqualsAndHashCode
@Data
class User implements Comparable<User> {
    private int id;
    private String name;
    //省略构造
    @Override
    public int compareTo(User o) {
        return this.id - o.getId();//升序
        return o.getId() - this.id;//逆序
        //如果是String可以使用以下的这个方法
        ///string.compareTo("");
    }
}
```
集合
```
List<User> list = Arrays.asList(new User(3), new User(4),new User(1));
```


1. 使用 `Collections` 工具类的 `sort` 方法，或者使用该方法的重载.
    需要对象实现 `Comparable` 接口，重写 `compareTo` 方法,
    ```
    Collections.sort(integers);
    ```
    重载
    ```
    Collections.sort(list, new Comparator<User>() {
        @Override
        public int compare(User o1, User o2) {
            return o1.getId() - o2.getId();
        }
    });
    ```
    使用 `Lambda` 简化后
    ```
    Collections.sort(list, (User u1, User u2) -> {
        return u1.getId() - u2.getId();
    });
2. 使用 `JAVA8` 中单独的 `Comparator` 的 `comparing`方法，来实现根据 `User` 的 `id`进行比较的操作
    ```
    Collections.sort(list, Comparator.comparing(User::getId));
    ```
3. `java8` 的 `list` 自带排序方法
    ```
    list.sort(new Comparator<User>() {
        @Override
        public int compare(User o1, User o2) {
            return o1.getId() - o2.getId();
        }
    });
    ```
    使用 `Lambda` 简化方后
    ```
    list.sort((User h1, User h2) -> h1.getId() - h2.getId());
    ```
4. `Java8` 使用多个条件进行排序  
    `Lambda` 提供了更复杂的表达式，还可以先对 `name` 排序再根据 `age` 进行排序
    ```
    Comparator<User> comparator = (h1, h2) -> {
        if (h1.getName().equals(h2.getName())) {
            return Integer.compare(h1.getId(), h2.getId());
        }
        return h1.getName().compareTo(h2.getName());
    };
    list.sort(comparator);
    ```
5. **推荐使用** - `Java8` 使用多个条件进行排序+组合的方式
    `Comparator` 对这种组合的排序有更优雅实现，从JDK8开始，我们可以使用链式操作进行复合操作来构建更复杂的逻辑
    ```
    list.sort(Comparator.comparing(User::getName).thenComparing(User::getId));
    ```
     反序
    ```
    list.sort(Comparator.comparing(User::getName).reversed()
            .thenComparing(User::getId).reversed());
    ```

## 反序

使用 `Collections.reverse(list);` 方法

