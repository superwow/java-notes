# 概述

`HashMap` 是一种散列表，用于存储Key-value形式的数据。


# Map简介

`Java`为数据结构中的映射定义了一个接口java.util.Map，此接口主要有四个常用的实现类，分别是`HashMap`、`Hashtable`、`LinkedHashMap`和`TreeMap`，类继承关系如下图所示：

<img src="../../java-notes/img/java-Map.png">

1. **HashMap**：它根据键的`hashCode`值存储数据，在大多数情况下可以直接定位到它的值，因而具有很快的访问速度，但是遍历的顺序是不确定的。`HashMap` 允许一条记录的键的值为`null`，允许多条记录的值为`null`。`HashMap` 是非线程安全的。如果需要满足线程安全，可以用`Collections`的 `synchronizedMap(Map<K,V> map)` 方法使`hashMap` 具有线程安全能力，或者使用 `ConcurrentHashMap`。

1. **Hashtable**：`Hashtable`是遗留类，很多映射的常用功能与`HashMap`类似，不同的是它承自`Dictionary`类，并且是线程安全的，任一时间只有一个线程能写Hashtable，并发性不如`ConcurrentHashMap`，因为`ConcurrentHashMap`引入了分段锁。`Hashtable`不建议在新代码中使用，不需要线程安全的场合可以用`HashMap`替换，需要线程安全的场合可以用`ConcurrentHashMap`替换。


1. **LinkedHashMap**：`LinkedHashMap`是`HashMap`的一个子类,保存了记录的插入顺序，在用 `iterator` 遍历时，先得到的记录肯定是先插入的，也可以在构造时带参数，按照访问次序排序。

1. **TreeMap**：`TreeMap` 实现了 `SortedMap` 接口，能够把它保存的记录根据键排序，默认是按键值的升序排序，也可以指定排序的比较器，当用 `iterator` 遍历 `TreeMap`  



# 内部实现

要搞清楚`HashMap` ,首先要知道`HashMap` 是什么，即它的**存储结构 - 字段**;

其次要明白它能干什么，即他的**功能实现-方法**。

## 存储结构-字段

从结构来讲，`HashMap` 是数组+链表+红黑树(JDK1.8增加了红黑树部分)实现的，如下图所示：

<img src="../../java-notes/img/java-hash-map储存结构.png">

这里需要讲清楚两个问题：数据底层具体存储是什么？这样的存储方式有什么优点？

从源码可知，HashMap类中有一个非常重要的字段，就是 Node[] table，即哈希桶数组，明显它是一个Node的数组。我们来看它们的源码。

```
//数组
transient Node<K,V>[] table;

//链表节点
static class Node<K,V> implements Map.Entry<K,V> {
        final int hash;    //用来定位数组索引位置
        final K key; 
        V value;    
        Node<K,V> next;   //链表的下一个node

        Node(int hash, K key, V value, Node<K,V> next) { ... }
        public final K getKey(){ ... }
        public final V getValue() { ... }
        public final String toString() { ... }
        public final int hashCode() { ... }
        public final V setValue(V newValue) { ... }
        public final boolean equals(Object o) { ... }
}
```
从上面的源码我们得知`HashMap` 使用了一个单项的链表，每个节点存储着下一个节点的指针，还有确定的数组下标，key以及value。而每个这样的节点都存储在数组中。


## HashMap的存储机制

我们知道`hashMap`采用了数组加链表的组合，来存储数据。这种方式教育链地址法。在这种数据结构中，我们会将`key` 进行 `hash` 运算，然后对数组长度进行取余得到下标。

这里`hashMap` 使用 `&`运算来代替取余运算，这样效率更高。

接下来我们来看一下`hashMap`是如何存储值的.
```
 map.put("LL","男");
```
在接下来我们看一下`put`方法的源码

<img src="../../java-notes/img/hashmap-put.png">

```
final V putVal(int hash, K key, V value, boolean onlyIfAbsent,
                   boolean evict) {
    Node<K,V>[] tab; Node<K,V> p; int n, i;
    if ((tab = table) == null || (n = tab.length) == 0)
        n = (tab = resize()).length;
    if ((p = tab[i = (n - 1) & hash]) == null)
        tab[i] = newNode(hash, key, value, null);
    else {
        Node<K,V> e; K k;
        if (p.hash == hash &&
            ((k = p.key) == key || (key != null && key.equals(k))))
            e = p;
        else if (p instanceof TreeNode)
            e = ((TreeNode<K,V>)p).putTreeVal(this, tab, hash, key, value);
        else {
            for (int binCount = 0; ; ++binCount) {
                if ((e = p.next) == null) {
                    p.next = newNode(hash, key, value, null);
                    if (binCount >= TREEIFY_THRESHOLD - 1) // -1 for 1st
                        treeifyBin(tab, hash);
                    break;
                }
                if (e.hash == hash &&
                    ((k = e.key) == key || (key != null && key.equals(k))))
                    break;
                p = e;
            }
        }
        if (e != null) { // existing mapping for key
            V oldValue = e.value;
            if (!onlyIfAbsent || oldValue == null)
                e.value = value;
            afterNodeAccess(e);
            return oldValue;
        }
    }
    ++modCount;
    if (++size > threshold)
        resize();
    afterNodeInsertion(evict);
    return null;
}
```
接下来我们将分为集中情况来讲解。

### 第一种情况-数组储存节点。

1. `hashMap` 首先对`key`进行了`hash`运算。
    ```
    static final int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }
    ```
    这时`key` 为`null`时直接使用0下标的数组位置，非`null`的`key`则正常进行`hash`运算。

1. 接下来我们看 `putVal`() 是如何实现的(在这一步我们只讨论正常插入数组节点的情况)

    1. 首先判断table 数组是否为null

        <img src="../../java-notes/img/hashmap-table=null.png">

        我们可以看到当数组为null或者数组长度为0时，扩容数组（注意这时n等于数组长度）。

    1. 如果key对应的数组的值为null，则创建节点并插入

        <img src="../../java-notes/img/hashmap-newNodeInsertTable.png">

        注意在if判断中已经将数组下标为i的值，赋值给p了。


        


### 第二种情况-链表尾部插入新节点

1. 如果key对应的数组的值不为null，则对比数组下节点的key，如果相同将p赋值给e

    <img src="../../java-notes/img/hashmap-nodeOverWriteNode.png">

2. （上面条件不成立）如果节点为红黑树节点，则进行存储

    <img src="../../java-notes/img/hashmap-treenode存储.png">

    红黑树会将key的hash值，进行运算，找到一个合适的位置进行存储。

3. （上面条件不成立）如果下一个节点为null，则创建一个新节点赋到原节点的尾部。

    <img src="../../java-notes/img/hashmap-findNextNodeIndex.png">

    这里使用了尾插法，是为了解决jdk1.7使用的头插法在多线程情况下形成的死循环问题。

4. 在e不为null的情况下，将新值赋给e的value字段，并返回旧值。

    <img src="../../java-notes/img/hashmap-newNodeValueOverWriteOldNodeValue.png">

1. 最后将修改次数加一，并检验是否需要扩容

    <img src="../../java-notes/img/hashmap-修改次数加一.png">
    

### 第三种情况-链表转化为红黑树

<img src="../../java-notes/img/hashmap-findNextNodeIndex.png">

在这张图中首先是将新节点附着在链表上，再检查链表长度是否大于等于8(这里从零开始就是7了)，如果大于等于8则将链表转换为红黑树。转换为红黑树的方法就是`treeifyBin(tab,hash)`，我们接下来看他的源码。

<img src="../../java-notes/img/hashmap-treeifybin.png">

根据上面的源码可以看到，将链表的头结点作为根节点生成红黑树，并且将红黑树旋转为平衡的。

### 总结

1. 对key进行hash运算确定桶位置

1. 如果table 数组容量为0则扩容

1. 如果数组位置的值为null，则创建节点加入。

1. 如果数组位置的值是红黑树类型的节点，则创建节点并加入红黑树

1. 如果数组位置的key值与当前key值相同，则使用新值覆盖旧值。并返回


1. 如果数组位置的key与当前key相同，则创建节点并将其加入链表，如果链表长度大于8转换为红黑树。这里需要注意的是jdk1.7采用的头插法而1.8采用的尾插法，这样可以避免死循环


1. 记录修改次数

1. 如果size超过阈值，则进行扩容。



## HashMap的扩容机制

首先我们需要看一下hash的一些属性和常量。

属性
```
//阈值，当size大于等于阈值时触发扩容。它等于容量 * 负载因子
int threshold;             // 所能容纳的key-value对极限 
//默认为0.75 是一个时间和空间上的综合考量
final float loadFactor;    // 负载因子，
int modCount;              // 修改次数
int size;                  // 存储键值对的个数
```
常量
```
// table数组的默认初始容量
static final int DEFAULT_INITIAL_CAPACITY = 1 << 4; // aka 16
// 默认负载因子
static final float DEFAULT_LOAD_FACTOR = 0.75f;
// 链表转换红黑树容量限制(当链表长度大于等于8时转换为红黑树)
static final int TREEIFY_THRESHOLD = 8;
//当树中的键值对小于等于6时，将红黑树转换为链表
static final int UNTREEIFY_THRESHOLD = 6;
//最小的树容量
static final int MIN_TREEIFY_CAPACITY = 64;
```

接下来我们来看源码
```
final Node<K,V>[] resize() {
    Node<K,V>[] oldTab = table;
    int oldCap = (oldTab == null) ? 0 : oldTab.length;
    int oldThr = threshold;
    int newCap, newThr = 0;
    if (oldCap > 0) {
        if (oldCap >= MAXIMUM_CAPACITY) {
            threshold = Integer.MAX_VALUE;
            return oldTab;
        }
        else if ((newCap = oldCap << 1) < MAXIMUM_CAPACITY &&
                    oldCap >= DEFAULT_INITIAL_CAPACITY)
            newThr = oldThr << 1; // double threshold
    }
    else if (oldThr > 0) // initial capacity was placed in threshold
        newCap = oldThr;
    else {               // zero initial threshold signifies using defaults
        newCap = DEFAULT_INITIAL_CAPACITY;
        newThr = (int)(DEFAULT_LOAD_FACTOR * DEFAULT_INITIAL_CAPACITY);
    }
    if (newThr == 0) {
        float ft = (float)newCap * loadFactor;
        newThr = (newCap < MAXIMUM_CAPACITY && ft < (float)MAXIMUM_CAPACITY ?
                    (int)ft : Integer.MAX_VALUE);
    }
    threshold = newThr;
    @SuppressWarnings({"rawtypes","unchecked"})
        Node<K,V>[] newTab = (Node<K,V>[])new Node[newCap];
    table = newTab;
    if (oldTab != null) {
        for (int j = 0; j < oldCap; ++j) {
            Node<K,V> e;
            if ((e = oldTab[j]) != null) {
                oldTab[j] = null;
                if (e.next == null)
                    newTab[e.hash & (newCap - 1)] = e;
                else if (e instanceof TreeNode)
                    ((TreeNode<K,V>)e).split(this, newTab, j, oldCap);
                else { // preserve order
                    Node<K,V> loHead = null, loTail = null;
                    Node<K,V> hiHead = null, hiTail = null;
                    Node<K,V> next;
                    do {
                        next = e.next;
                        if ((e.hash & oldCap) == 0) {
                            if (loTail == null)
                                loHead = e;
                            else
                                loTail.next = e;
                            loTail = e;
                        }
                        else {
                            if (hiTail == null)
                                hiHead = e;
                            else
                                hiTail.next = e;
                            hiTail = e;
                        }
                    } while ((e = next) != null);
                    if (loTail != null) {
                        loTail.next = null;
                        newTab[j] = loHead;
                    }
                    if (hiTail != null) {
                        hiTail.next = null;
                        newTab[j + oldCap] = hiHead;
                    }
                }
            }
        }
    }
    return newTab;
}
```

这段源码非常长我们接下来分段进行分析

### 方法内定义的变量
```
//原数组
Node<K,V>[] oldTab = table;
//旧容量
int oldCap = (oldTab == null) ? 0 : oldTab.length;
//旧阈值
int oldThr = threshold;
//新容量，新阈值
int newCap, newThr = 0;
```

### 计算扩容后的大小
```
// 1. 如果旧容量大于0
if (oldCap > 0) {
    //如果等于或超过最大容量则不进行扩容
    if (oldCap >= MAXIMUM_CAPACITY) {
        threshold = Integer.MAX_VALUE;
        return oldTab;
    }
    // 新容量 = 旧容量 * 2，阈值也
    else if ((newCap = oldCap << 1) < MAXIMUM_CAPACITY &&
                oldCap >= DEFAULT_INITIAL_CAPACITY)
        newThr = oldThr << 1; // double threshold
}
// 2. 旧容量小于等于0，并且旧阈值大于0，则将旧阈值赋值给新容量
else if (oldThr > 0) // initial capacity was placed in threshold
    newCap = oldThr;
// 3. 如果上面条件不成立，则按照初始化容量进行设置
else { // zero initial threshold signifies using defaults
    newCap = DEFAULT_INITIAL_CAPACITY;
    newThr = (int)(DEFAULT_LOAD_FACTOR * DEFAULT_INITIAL_CAPACITY);
}
// 4. 新阈值如果计算后为0，则按照公式重新计算。
if (newThr == 0) {
    float ft = (float)newCap * loadFactor;
    newThr = (newCap < MAXIMUM_CAPACITY && 
        ft < (float)MAXIMUM_CAPACITY ?
        (int)ft : Integer.MAX_VALUE);
}
// 5. 设置新阈值
threshold = newThr;
```
### 扩大容量为原先的二倍
```
//创建新数组
@SuppressWarnings({"rawtypes","unchecked"})
    Node<K,V>[] newTab = (Node<K,V>[])new Node[newCap];
//新数组赋值给table
table = newTab;
```

### 数据复制到新数组

```
 if (oldTab != null) {
    // 如果旧的桶数组不为空，则遍历桶数组，并将键值对映射到新的桶数组中
    for (int j = 0; j < oldCap; ++j) {
        Node<K,V> e;
        if ((e = oldTab[j]) != null) {
            oldTab[j] = null;
            if (e.next == null)
                newTab[e.hash & (newCap - 1)] = e;
            else if (e instanceof TreeNode)
                // 重新映射时，需要对红黑树进行拆分
                ((TreeNode<K,V>)e).split(this, newTab, j, oldCap);
            else { // preserve order
                Node<K,V> loHead = null, loTail = null;
                Node<K,V> hiHead = null, hiTail = null;
                Node<K,V> next;
                // 遍历链表，并将链表节点按原顺序进行分组
                do {
                    next = e.next;
                    if ((e.hash & oldCap) == 0) {
                        if (loTail == null)
                            loHead = e;
                        else
                            loTail.next = e;
                        loTail = e;
                    }
                    else {
                        if (hiTail == null)
                            hiHead = e;
                        else
                            hiTail.next = e;
                        hiTail = e;
                    }
                } while ((e = next) != null);
                // 将分组后的链表映射到新桶中
                if (loTail != null) {
                    loTail.next = null;
                    newTab[j] = loHead;
                }
                if (hiTail != null) {
                    hiTail.next = null;
                    newTab[j + oldCap] = hiHead;
                }
            }
        }
    }
}

```


### 总结

1. 计算新桶数组的容量 newCap 和新阈值 newThr

1. 根据计算出的 newCap 创建新的桶数组，桶数组 table 也是在这里进行初始化的

1. 将键值对节点重新映射到新的桶数组里。如果节点是 TreeNode 类型，则需要拆分红黑树。如果是普通节点，则节点按原顺序进行分组。

## 移除元素
第一步是定位桶位置，

第二步遍历链表并找到键值相等的节点，

第三步删除节点并修复链表或红黑树


# 面试题

## 为什么说hashMap是不安全的？