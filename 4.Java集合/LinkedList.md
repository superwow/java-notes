# 概述

`LinkedList` ，基于节点实现的双向链表的 `List` ，每个节点都指向前一个和后一个节点从而形成链表。相比 `ArrayList` 来说，我们日常开发使用 `LinkedList` 相对比较少。





1. `LinkedList`继承于`AbstractSequentialList`，并且实现了`Dequeue`接口。 
2. `LinkedList` 包含两个重要的成员：`header` 和 `size`

3. `header` 是双向链表的表头，它是双向链表节点所对应的类`Entry`的实例。

4. `Entry` 中包含成员变量： `previous`, `next`, `element。`  
    `previous` 是该节点的上一个节点，  
    `next` 是该节点的下一个节点，  
    `element` 是该节点所包含的值。 

4. `size` 是双向链表中节点的个数。

# 储存方式
基于 `Entry` 进行储存，初始长度为0

# 使用场景

1. 随机查询速度不行，每次查找都需要遍历

2. 顺序访问速度可以

3. 添加元素，删除都很有速度优势


